<?php

namespace Custobar\CustoConnector\Helper;

use Magento\Cron\Model\Schedule;
use Magento\Framework\DB\Select;
use Magento\Framework\Stdlib\DateTime\Timezone;
use \Magento\Store\Model\Store;

/**
 * Class Custobar_CustoConnector_Helper_Data
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const MAX_ERROR_COUNT = 7200;

    /**
     * @var \Magento\Framework\HTTP\ZendClientFactory
     */
    protected $httpClient;
    /**
     * @var int
     */
    public static $INITIAL_PAGE_SIZE = 500;
    /**
     * @var string
     */
    const LOG_CONTEXT = 'Custoconnector: ';
    /**
     * @var string
     */
    public static $CONFIG_ALLOWED_WEBSITES = "custobar/custobar_custoconnector/allowed_websites";
    /**
     * @var string
     */
    private static $CONFIG_TRACKED_MODELS_PATH = "custobar/custobar_custoconnector/models";
    /**
     * @var string
     */
    private static $CONFIG_API_PREFIX_PATH = "custobar/custobar_custoconnector/prefix";
    /**
     * @var string
     */
    private static $CONFIG_API_KEY_PATH = "custobar/custobar_custoconnector/apikey";
    /**
     * @var string
     */
    private static $CONFIG_MODE_PATH = "custobar/custobar_custoconnector/mode";
    /**
     * @var string
     */
    private static $CONFIG_TRACKING_SCRIPT = "custobar/custobar_custoconnector/tracking_script";
    /**
     * @var int
     */
    public static $ITEMS_PER_CRON = 500;

    public static $BACKEND_TRACKED_MODELS = [
        'Magento\Store\Model\Store' => [
            "to" => "shops",
            "fields" => [
                "id" => "external_id",
                "custobar_name" => "name"
            ]
        ]
    ];

    /**
     * @var array
     */
    private $trackedModels = [];

    /**
     * @var \Magento\Framework\App\Config
     */
    protected $scopeConfig;

    /**
     * @var ObjectManager
     */
    protected $objectManager;

    /**
     * @var Timezone
     */
    protected $timeZone;

    /**
     * @var \Magento\Framework\Event\ManagerInterface
     */
    protected $eventManager;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var \Magento\Cron\Model\ScheduleFactory
     */
    protected $cronScheduleFactory;

    /**
     * @var \Magento\Store\Model\StoreFactory
     */
    protected $storeStoreFactory;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $catalogProductFactory;

    /**
     * @var \Magento\Catalog\Model\Config
     */
    protected $catalogConfig;

    /**
     * @var \Magento\Framework\DataObjectFactory
     */
    protected $dataObjectFactory;

    /**
     * @var \Magento\Framework\HTTP\ZendClientFactory
     */
    protected $zendClientFactory;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Cron\Model\ScheduleFactory $cronScheduleFactory,
        \Magento\Store\Model\StoreFactory $storeStoreFactory,
        \Magento\Catalog\Model\ProductFactory $catalogProductFactory,
        \Magento\Catalog\Model\Config $catalogConfig,
        \Magento\Framework\DataObjectFactory $dataObjectFactory,
        \Magento\Framework\HTTP\ZendClientFactory $zendClientFactory
    ) {
        $this->dataObjectFactory = $dataObjectFactory;
        $this->zendClientFactory = $zendClientFactory;
        $this->scopeConfig = $context->getScopeConfig();
        $this->eventManager = $context->getEventManager();
        $this->objectManager = $objectManager;
        $this->logger = $context->getLogger();
        $this->cronScheduleFactory = $cronScheduleFactory;
        $this->storeStoreFactory = $storeStoreFactory;
        $this->catalogProductFactory = $catalogProductFactory;
        $this->catalogConfig = $catalogConfig;
        parent::__construct(
            $context
        );
    }

    /**
     * @param string $format
     *
     * @return false|string
     */
    public function getFormattedScopeTime($format = "Y-m-d H:i:s")
    {
        return date($format, $this->getTimeZone()->scopeTimeStamp());
    }

    /**
     *
     * @param int    $eventEntityId
     * @param int    $store_id
     * @param string $eventEntityType
     * @param int    $errors
     *
     * @return bool|\Custobar\CustoConnector\Model\Schedule
     * @throws \Zend_Db_Statement_Exception
     */
    public function scheduleUpdate(
        $eventEntityId,
        $store_id,
        $eventEntityType,
        $errors = 0
    ) {
        $model = $this->getSchedulesModel();
        $existing = $model->getResource()
            ->getUnique($eventEntityType, $eventEntityId, $store_id);
        if ($existing) {
            return false;
        }

        $model->setEntityId($eventEntityId);
        $model->setEntityType($eventEntityType);
        $model->setCreatedAt($this->getFormattedScopeTime());
        $model->setProcessedAt("0000-00-00 00:00:00");
        $model->setErrors($errors);
        $model->setStoreId($store_id);
        $model->setId(null);

        /* prevents adding data */
        try {
            /** @var $resource \Custobar\CustoConnector\Model\ResourceModel\Schedule\ */
            $resource = $this->objectManager->get(
                \Custobar\CustoConnector\Model\ResourceModel\Schedule::class
            );
            $resource->save($model);
        } catch (\Exception $e) {
            if (
                $e instanceof \Zend_Db_Statement_Exception
                && $e->getCode() === 23000
            ) {
                return false;
            } else {
                // Keep throwing it.
                throw $e;
            }
        }

        if ($model) {
            return $model;
        }
        return false;
    }

    /**
     * @param \Magento\Framework\Model\AbstractModel $object
     *
     * @return bool|string
     */
    public function getEvenObjectTrackedModel($object)
    {
        try {
            $classesToTrack = $this->getConfigTrackedModels();
        } catch (\Exception $e) {
            return false;
        }

        foreach (
            $classesToTrack
            as $className =>
            $data
        ) {
            if (is_a($object, $className)) {
                return $className;
            }
        }

        return false;
    }


    /**
     * @return array
     */
    public function getConfigTrackedModels()
    {
        /** @var array $trackedModels */
        $trackedModels = $this->trackedModels;

        if (count($this->trackedModels) === 0) {
            $values = $this->getCommaSeparatedConfigValue(
                self::$CONFIG_TRACKED_MODELS_PATH
            );

            foreach ($values as $value) {
                $split = $this->explodeAndTrimWith(":", $value);

                $modelMapSplit = array_map('trim', explode(">", $split[0]));

                $target = $modelMapSplit[1];
                $from = $modelMapSplit[0];
                $attributesMap = [];

                foreach (
                    $this->explodeAndTrimWith(";", $split[1]) as $attributes
                ) {
                    // reverse so that magento key is on left
                    $mapPair = array_reverse(
                        array_map('trim', explode(">", $attributes))
                    );
                    $attributesMap[(string)$mapPair[1]] = $mapPair[0];
                }

                $isInternal = false;

                if (strpos($target, "*") !== false) {
                    $isInternal = true;
                    $target = ltrim($target, "*");
                }

                $trackedModels[(string)$from]["to"] =
                    $target;
                $trackedModels[(string)$from]["internal"] =
                    $isInternal;

                $trackedModels[(string)$from]["fields"] =
                    $attributesMap;
            }

            if (count($trackedModels) == 0) {
                $this->log("No tracked models set!");
            }

            $trackedModels =
                array_merge($trackedModels, self::$BACKEND_TRACKED_MODELS);
            $this->trackedModels = $trackedModels;
        }

        return $trackedModels;
    }

    /**
     * Returns comma separated store configs as array
     *
     * @param string                                          $path
     * @param null|string|bool|int|\Magento\Store\Model\Store $store
     *
     * @return array
     */
    public function getCommaSeparatedConfigValue($path, $store = null)
    {
        $trimmableConfigValue = $this->scopeConfig->getValue(
            $path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store
        );
        $configs = trim(
            $trimmableConfigValue
        );
        if ($configs) {
            return $this->explodeAndTrimWith(",", $configs);
        }

        return array();
    }

    /**
     * @return string
     */
    public function getApiPrefix()
    {
        return trim(
            $this->scopeConfig->getValue(
                self::$CONFIG_API_PREFIX_PATH,
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                null
            )
        );
    }

    /**
     * @return string
     */
    public function getApiBaseUri()
    {
        $apiPrefix = $this->getApiPrefix();

        if ($this->getIsDevModeFromConfig()) {
            $apiPrefix = "dev";
        }

        $uri = "https://{$apiPrefix}.custobar.com/api/";

        return $uri;
    }

    /**
     * @param string          $host
     * @param string|resource $rawData
     *
     * @return \Magento\Framework\HTTP\ZendClient
     * @throws \Zend_Http_Client_Exception
     */
    public function getApiClient($host, $rawData)
    {
        $config = array(
            'timeout' => 5
        );

        $auth = "access-token";
        /** @var \Magento\Framework\HTTP\ZendClient $client */
        $client = $this->getClient();
        /** @todo fix so we dont have to call create here
         * @see integration tests
         */
        if (!is_a($client, '\Magento\Framework\HTTP\ZendClient')) {
            $client = $client->create();
        }
        $client->setUri($host)
            ->setConfig($config)
            ->setHeaders("accept-encoding", "application/json")
            ->setRawData($rawData, "application/json")
            ->setMethod(\Zend_Http_Client::POST)
            ->setAuth($auth, $this->getApiKey());
        return $client;
    }

    /**
     * @throws \\Exception
     */
    public function sendUpdateRequests()
    {
        if (!$this->canProcess()) {
            return;
        }
        /** @var \Custobar\CustoConnector\Model\ResourceModel\Schedule\Collection $schedules */
        $schedules = $this->getSchedulesCollection();
        $schedules->addOnlyForSendingFilter()
            ->setOrder("created_at", "ASC")
            ->setOrder("entity_type", "ASC")
            ->setPageSize(self::$ITEMS_PER_CRON)
            ->setCurPage(1);

        /** @var array $entityLists */
        $entityLists = [];
        $types = array_unique($schedules->getColumnValues("entity_type"));

        // intialize empty holder
        foreach ($types as $type) {
            $entityLists[(string)$type] = [];
        }

        foreach ($types as $type) {
            $schedulesByType =
                $schedules->getItemsByColumnValue("entity_type", $type);

            /** @var array $itemsForProcessing */
            $itemsForProcessing = ["schedules" => [], "entities" => []];

            /** @var \Custobar\CustoConnector\Model\Schedule $schedule */
            foreach ($schedulesByType as $schedule) {

                /** @var \Magento\Framework\Model\AbstractModel $entity */
                $entity = $this->getEntityFromSchedule($schedule);
                $itemsForProcessing = $this->returnItemForProcessing(
                    $entity,
                    $schedule,
                    $itemsForProcessing
                );
            }

            if (count($itemsForProcessing["entities"]) > 0) {
                $this->processCustobarResponse($itemsForProcessing);
            }
        }
    }


    /**
     * @return string
     */
    public function getApiKey()
    {
        $apiKey = trim(
            $this->scopeConfig->getValue(
                self::$CONFIG_API_KEY_PATH,
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                null
            )
        );

        if ($apiKey === "") {
            $this->log("Missing api key!");
            return false;
        }

        return $apiKey;
    }

    /**
     * @return bool
     */
    public function getIsDevModeFromConfig()
    {
        return (bool)trim(
            $this->scopeConfig->getValue(
                self::$CONFIG_MODE_PATH,
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                null
            )
        );
    }

    /**
     * @return mixed
     */
    public function getTrackingScript()
    {
        return trim(
            $this->scopeConfig->getValue(
                self::$CONFIG_TRACKING_SCRIPT,
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                null
            )
        );
    }

    /**
     * @param \Magento\Framework\Model\AbstractModel $entity
     *
     * @return \Magento\Framework\DataObject
     */
    public function getMappedEntity($entity)
    {
        $models = $this->getConfigTrackedModels();

        $className = $this->getEvenObjectTrackedModel($entity);

        /** @var array $targetModel */
        $targetModel = null;

        $to = $this->dataObjectFactory->create();

        try {
            $targetModel = $models[$className]["fields"];
        } catch (\Exception $e) {
            $this->log(
                "tried to map entity {$className} that is not tracked"
            );
            return $to;
        }

        $this->eventManager->dispatch(
            strtolower(str_replace('\\', '_', $className))
            . "_custo_map_before",
            [
                "entity" => $entity,
                "map" => $targetModel
            ]
        );

        \Magento\Framework\DataObject\Mapper::accumulateByMap(
            [$entity, 'getDataUsingMethod'],
            $to,
            $targetModel
        );

        return $to;
    }

    /**
     * @param string $delimiter
     * @param string $string
     *
     * @return array
     */
    private function explodeAndTrimWith($delimiter, $string)
    {
        return array_unique(array_map('trim', explode($delimiter, $string)));
    }

    /**
     * @param array $itemsForProcessing
     *
     * @return bool
     */
    private function processCustobarResponse($itemsForProcessing)
    {
        /** @var array $schedules */
        $schedules = $itemsForProcessing["schedules"];
        /** @var array $entities */
        $entities = $itemsForProcessing["entities"];

        $custobarJson = $this->getCustobarJsonForEntity(
            $entities
        );
        /** @var \Magento\Framework\Model\AbstractModel $firstEntity */
        $firstEntity = $entities[0];

        $url = $this->getApiBaseUri() . $this->getUrlSuffix($firstEntity);

        $body = null;
        $client = null;
        $response = null;
        $failed = false;

        try {
            $client = $this->getApiClient($url, $custobarJson);
            $response = $client->request();
            $body = trim($response->getBody());
        } catch (\Exception $ex) {
            $this->logger->alert(self::LOG_CONTEXT . $ex->getMessage());
            $this->logger->critical($ex);

            $failed = true;
        }

        if (empty($body)) {
            $this->logger->error(self::LOG_CONTEXT . "body short");
            $failed = true;
        }
        $json = json_decode($body, true);
        if (!(is_array($json) && isset($json["response"])
            && strtolower($json["response"]) === "ok")
        ) {
            $msg =
                "wrong response content: {$body}. Code {$response->getStatus()}";
            $this->logger->error(self::LOG_CONTEXT . $msg);
            $failed = true;
        }
        array_walk($schedules, array($this, "markScheduleProcessed"));

        if ($failed) {
            array_walk($schedules, array($this, "rescheduleFailedSchedule"));
        }

        return true;
    }

    /**
     * @param \Custobar\CustoConnector\Model\Schedule $item
     * @param                                         $key
     */
    public function markScheduleProcessed($item, $key)
    {
        $item->setProcessedAt($this->getFormattedScopeTime());
        $item->save();
    }

    /**
     * @param \Custobar\CustoConnector\Model\Schedule $item
     * @param                                         $key
     */
    public function rescheduleFailedSchedule($item, $key)
    {
        if ($item->getErrorsCount() < self::MAX_ERROR_COUNT) {
            $this->scheduleUpdate(
                $item->getEntityId(),
                $item->getStoreId(),
                $item->getEntityType(),
                $item->getErrorsCount() + 1
            );
        } else {
            $this->log(
                "Did not reschedule {$item->getEntityId()} of {$item->getEntityType()} due to error limits"
            );
        }
    }

    /**
     * @param \Custobar\CustoConnector\Model\Schedule $schedule
     *
     * @return bool|\Magento\Framework\Model\AbstractModel
     */
    public function getEntityFromSchedule($schedule)
    {
        if ($this->getIsModelInternal($schedule->getEntityType())) {
            return false;
        }

        return $this->getEntityByTypeAndId(
            $schedule->getEntityType(),
            $schedule->getEntityId(),
            $schedule->getStoreId()
        );
    }

    /**
     * @param \Magento\Framework\Model\AbstractModel|array $entities
     *
     * @return string
     */
    public function getCustobarJsonForEntity($entities)
    {
        if (!is_array($entities)) {
            $entities = [$entities];
        }

        $models = $this->getConfigTrackedModels();
        $keyName = null;
        $dataJson = null;

        /** @var array $entityRows */
        $entityRows = [];

        /** @var \Magento\Framework\Model\AbstractModel $entity */
        foreach ($entities as $entity) {
            $mapClass = $this->getEvenObjectTrackedModel($entity);

            /** @var array $entityData */
            $entityData = $this->getMappedEntity($entity)->getData();

            /** @var string $keyName */
            $keyName = $models[$mapClass]["to"];

            if ($mapClass === 'Magento\Sales\Model\Order') {
                /** @var array $itemData */
                $itemData = $entityData["magento__items"];
                unset($entityData["magento__items"]);

                if (count($itemData) > 0) {

                    // merge the first product to the first set
                    $first = array_shift($itemData);
                    $entityData = array_merge($entityData, $first);
                }
                $entityRows =
                    array_merge($entityRows, [$entityData], $itemData);
            } else {
                $entityRows[] = $entityData;
            }
        }
        $dataJson = json_encode([$keyName => $entityRows]);

        return $dataJson;
    }

    /**
     * @param \Magento\Framework\Model\AbstractModel $entity
     *
     * @return string
     */
    public function getUrlSuffix($entity)
    {
        $models = $this->getConfigTrackedModels();
        $mapClass = $this->getEvenObjectTrackedModel($entity);

        $keyName = $models[$mapClass]["to"];
        return "{$keyName}/upload/";
    }

    /**
     * @param string  $message
     * @param integer $level
     *
     * @todo abstract the levels for different logger levels
     */
    public function log($message, $level = 100)
    {
        $this->logger->log($level, "CustoConnector: {$message}");
    }

    /**
     * @param string $whatModel
     */
    public function startInitialScheduling($whatModel = null)
    {
        if (!$this->canProcess()) {
            return;
        }
        /** @var \Magento\Store\Model\ResourceModel\Store\Collection $stores */
        $trackedModels = array_keys($this->getConfigTrackedModels());
        /** @var int $succesfull */
        $successful = 0;
        foreach ($trackedModels as $model) {
            if ($whatModel && ($model !== $whatModel)) {
                continue;
            }

            /** skip internally tracked models as they dont go to the schedules table  */
            if ($this->getIsModelInternal($model)) {
                continue;
            }

            /** @var \Magento\Framework\Model\AbstractModel $modelSingleton */
            $modelSingleton = $this->objectManager->create($model);
            if ($modelSingleton) {
                /** @var \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection $collection */
                $collection = $modelSingleton->getCollection();

                if ($collection->getSize() > 0) {
                    try {
                        $collection->setPageSize(self::$INITIAL_PAGE_SIZE);

                        /** @var \Custobar\CustoConnector\Model\Initial $initial */
                        $initial =
                            $this->objectManager->create(
                                \Custobar\CustoConnector\Model\Initial::class
                            );

                        if ($initial->getResource()->typeExists($model)) {
                            $initial->getResource()->deleteType($model);
                        }
                        $initial->setPage(0);
                        $initial->setPages($collection->getLastPageNumber());
                        $initial->setEntityType($model);
                        $initial->save();
                        $successful++;
                    } catch (\Exception $e) {
                        // dont handle exceptions as we prevent adding same again
                    }
                }
            }
        }
    }

    public function handleNextInitialPage()
    {
        /** @var \Custobar\CustoConnector\Model\Initial $initialModel */
        $initialModel = $this->getInitialModel();
        /** @var \Custobar\CustoConnector\Model\ResourceModel\Initial\Collection $initialSchedulesCollection */
        $initialSchedulesCollection = $initialModel->getCollection();
        /** @var \Custobar\CustoConnector\Model\Initial $initialItem */

        $skipped = 0;

        foreach ($initialSchedulesCollection as $initialItem) {

            // lets skip if whe have done that set
            if ($initialItem->getPage() == $initialItem->getPages()) {
                $skipped++;
                continue;
            }
            if (!$initialItem->isEmpty()) {
                $entityType = $initialItem->getEntityType();

                /** @var \Magento\Framework\Model\AbstractModel $modelSingleton */
                $modelSingleton =
                    $this->objectManager->create($entityType);
                if ($modelSingleton) {
                    $initialItem->setPage((int)$initialItem->getPage() + 1);

                    $entityCollection =
                        $modelSingleton->getResourceCollection();
                    $entityCollection->setPageSize(self::$INITIAL_PAGE_SIZE);
                    $entityCollection->setCurPage($initialItem->getPage());

                    if (is_a(
                        $modelSingleton,
                        'Magento\Catalog\Model\Product'
                    )) {
                        /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $productCollection */
                        $productCollection = $entityCollection;
                        $productCollection->addWebsiteNamesToResult();

                        /** @var \Magento\Catalog\Model\Product $entity */
                        foreach ($entityCollection as $entity) {
                            $storeIds = $this->getStoreIdsByWebsites(
                                $entity->getWebsites()
                            );

                            foreach ($storeIds as $storeId) {
                                $this->scheduleUpdate(
                                    $entity->getId(),
                                    $storeId,
                                    $initialItem->getEntityType()
                                );
                            }
                        }
                    } else {
                        foreach ($entityCollection as $entity) {
                            $this->scheduleUpdate(
                                $entity->getId(),
                                $entity->getStoreId(),
                                $initialItem->getEntityType()
                            );
                        }
                    }


                    $initialItem->save();
                } else {
                    /** remove an unknown model schedule */
                    $initialItem->delete();
                }
                /** break the for loop so we don't handle next set */
                break;
            }
        }
        // If we have skipped all done sets lest clear the collection
        if ($skipped >= $initialSchedulesCollection->getSize()) {
            $initialModel->cleanInitials();
        }
    }

    /**
     * @param string $job_code
     *
     * @return \Magento\Cron\Model\ResourceModel\Schedule\Collection
     */
    public function getCronSchedulesByCode($job_code)
    {
        $schedules = $this->getCronCollection();
        $schedules->addFilter('job_code', $job_code);
        return $schedules;
    }

    /**
     * @return \Magento\Cron\Model\Schedule
     */
    private function getCronModel()
    {
        return $this->cronScheduleFactory->create();
    }

    /**
     * @return \Magento\Cron\Model\ResourceModel\Schedule\Collection
     */
    private function getCronCollection()
    {
        return $this->getCronModel()->getCollection();
    }

    public function canProcess()
    {
        return ($this->getApiKey() && $this->getApiPrefix());
    }

    public function getIsModelInternal($className)
    {
        $set = $this->getModelMapSet($className);

        if (isset($set["internal"]) && $set["internal"] == true) {
            return true;
        }

        return false;
    }

    /**
     * @param string $className
     *
     * @return bool|mixed
     */
    public function getModelMapSet($className)
    {
        $models = $this->getConfigTrackedModels();

        if (isset($models[$className])) {
            return $models[$className];
        }
        return false;
    }

    /**
     * @return \Custobar\CustoConnector\Model\ResourceModel\Schedule\Collection
     */
    public function getSchedulesCollection()
    {
        /** @var \Custobar\CustoConnector\Model\ResourceModel\Schedule\Collection $collection */
        $collection = $this->getSchedulesModel()->getCollection();
        return $collection;
    }

    /**
     * @return \Custobar\CustoConnector\Model\Schedule'
     */
    public function getSchedulesModel()
    {
        return $this->objectManager->get(
            '\Custobar\CustoConnector\Model\ScheduleFactory'
        )->create();
    }

    /**
     * @param $websiteId
     *
     * @return bool
     */
    public function getIsWebsiteAllowed($websiteId)
    {
        $values = $this->getCommaSeparatedConfigValue(
            self::$CONFIG_ALLOWED_WEBSITES
        );

        $values[] = 0; // add the admin store id

        if (count($values) > 0 && in_array($websiteId, $values)) {
            return true;
        }

        return false;
    }

    /**
     * @param \Magento\Framework\Model\AbstractModel $ob
     *
     * @return bool
     */
    public function getIsInAllowedWebsites($ob)
    {
        if ($ob->hasData("website_id")) {
            return $this->getIsWebsiteAllowed((int)$ob->getData("website_id"));
        }

        if (method_exists($ob, "getWebsiteId")) {
            return $this->getIsWebsiteAllowed($ob->getWebsiteId());
        }

        if (method_exists($ob, "getWebsiteIds")) {
            $webSiteIds = $ob->getWebsiteIds();
            foreach ($webSiteIds as $id) {
                if ($this->getIsWebsiteAllowed($id)) {
                    return true;
                }
            }

            return false; // need to break out here the store id we might get is 0 on products in the next
        }

        if ($ob->getStoreId() !== null) {
            if ($this->getIsWebsiteAllowed(
                $this->storeStoreFactory->create()
                    ->load($ob->getStoreId())
                    ->getWebsiteId()
            )
            ) {
                return true;
            }
        }
        return false;
    }

    public function getCanTrackObject($ob)
    {
        if ($ob && !$ob->isEmpty()) {
            $can = $this->getIsInAllowedWebsites($ob);
            // TODO: add a dispatch here so we have a nice hook point to do other kinds of rejections
            return $can;
        } else {
            return false;
        }
    }

    /**
     * @param string $entityType
     * @param int    $entityId
     * @param int    $storeId
     *
     * @return bool|\Magento\Framework\Model\AbstractModel
     */
    public function getEntityByTypeAndId($entityType, $entityId, $storeId)
    {
        if (!class_exists($entityType)) {
            return false;
        }

        /** @var \Magento\Framework\Model\AbstractModel $entity */
        $entity = $this->objectManager->create($entityType);

        if (!$this->getEvenObjectTrackedModel($entity)) {
            return false;
        }
        if ($entity) {
            if (is_a($entity, 'Magento\Catalog\Model\Product')) {
                $entity = $this->getProductEntity($entityId, $storeId);
            } else {
                $entity->load(
                    $entityId
                );
            }
        }
        return $entity;
    }

    /**
     * @param \Magento\Framework\Model\AbstractModel  $entity
     * @param \Custobar\CustoConnector\Model\Schedule $schedule
     * @param array                                   $itemsForProcessing
     *
     * @return array
     */
    public function returnItemForProcessing(
        $entity,
        $schedule,
        &$itemsForProcessing
    ) {
        /** skip internally tracked models as they dont go to the schedules table  */

        if ($entity && !$entity->isEmpty()
            && $this->getIsInAllowedWebsites(
                $entity
            )
        ) {
            $itemsForProcessing["schedules"][] = $schedule;
            $itemsForProcessing["entities"][] = $entity;
            return $itemsForProcessing;
        } else {
            // mark items not tracked anymore processed

            $schedule->setProcessedAt($this->getFormattedScopeTime());
            $schedule->save();
            $this->log(
                "Did not process {$schedule->getEntityId()} of {$schedule->getEntityType()}. Not tracked or not present"
            );
            return $itemsForProcessing;
        }
    }

    public function getClient()
    {
        if (!$this->httpClient) {
            $this->setClient();
        }
        return $this->httpClient;
    }

    public function setClient($client = null)
    {
        $this->httpClient =
            ($client) ? $client : $this->zendClientFactory->create();
    }

    private function getStoreIdsByWebsites($websites)
    {
        /** @var \Magento\Store\Model\Store $storesCollection */
        $storesCollection =
            $this->objectManager->create(Store::class)
                ->getCollection();
        return $storesCollection->addWebsiteFilter($websites)->getAllIds();
    }

    public function isInitialRunning()
    {
        /** @var \Custobar\CustoConnector\Model\ResourceModel\Initial\Collection $rC */
        return ($this->getInitialModel()->getCollection()->getSize() > 0);
    }

    /**
     * @return false|\Custobar\CustoConnector\Model\Initial
     */
    public function getInitialModel()
    {
        return $this->objectManager->get(
            \Custobar\CustoConnector\Model\Initial::class
        );
    }

    private function getProductEntity($entityId, $storeId)
    {
        /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $collection */
        $collection = $this->catalogProductFactory->create()
            ->setStoreId($storeId)
            ->getCollection();

        /** @var \Magento\Catalog\Model\Config $config */
        $config = $this->catalogConfig;
        $productAttributes =
            $config->getProductAttributes();

        $productAttributes[] = "description";
        $productAttributes[] = "image";
        $productAttributes[] = "visibility";

        $collection->setStoreId($storeId);
        $collection
            ->addAttributeToSelect($productAttributes)
            ->addAttributeToFilter("entity_id", $entityId);

        $collection->addPriceData();
        // change the price inner join to a left join so we get products that are out of stock
        $select = $collection->getSelect();
        $fromAndJoins = $select->getPart(Select::FROM);
        foreach ($fromAndJoins as $item => $value) {
            if ($item === "price_index") {
                $value["joinType"] = "left join";
                $fromAndJoins[$item] = $value;
            }
        }
        $select->setPart(Select::FROM, $fromAndJoins);

        $collection->addWebsiteNamesToResult();
        $collection->load();
        $collection->addCategoryIds();
        $collection->addUrlRewrite();

        /** @var \Magento\Catalog\Model\Product $firstItem */
        $firstItem = $collection->getFirstItem();
        $firstItem->setStoreId($storeId);
        return $firstItem;
    }

    /**
     * @return Timezone
     */
    public function getTimeZone()
    {
        return $this->objectManager->get(Timezone::class);
    }
}
