<?php

namespace Custobar\CustoConnector\Cron;

use Custobar\CustoConnector\Helper\Data;

class Send
{
    protected $helper;

    public function __construct(Data $helper)
    {
        $this->helper = $helper;
    }

    public function execute()
    {
        $this->callCustobarApi();
    }

    public function callCustobarApi()
    {
        if (!$this->helper->isInitialRunning()) {
            $this->helper->sendUpdateRequests();
        } else {
            $this->helper->log(
                "Initial scheduling running. Will not purge schedules"
            );
        }
    }
}
