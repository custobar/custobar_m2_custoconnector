<?php

namespace Custobar\CustoConnector\Log\Handler;

use Magento\Framework\Logger\Handler\Base;
use Monolog\Logger;

class Exception extends Base
{
    /**
     * Logging level
     *
     * @var int
     */
    protected $loggerType = Logger::ALERT;

    /**
     * File name
     *
     * @var string
     */
    protected $fileName = '/var/log/custoconnector-exception.log';
}
