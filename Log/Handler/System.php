<?php

namespace Custobar\CustoConnector\Log\Handler;

use Magento\Framework\Logger\Handler\Base;
use Monolog\Logger;

class System extends Base
{
    /**
     * Logging level
     *
     * @var int
     */
    protected $loggerType = Logger::INFO;

    /**
     * File name
     *
     * @var string
     */
    protected $fileName = '/var/log/custoconnector-system.log';
}
