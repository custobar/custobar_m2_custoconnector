<?php

namespace Custobar\CustoConnector\Log;


class Logger extends \Monolog\Logger
{

    public function __construct(
        $name,
        $handlers = array(),
        $processors = array()
    ) {
        parent::__construct($name, $handlers, $processors);
    }
}
