<?php
/**
 * Created by PhpStorm.
 * User: olli.tyynela
 * Date: 10.4.2017
 * Time: 15.22
 */

namespace Custobar\CustoConnector\Test\Unit\cron;


use Custobar\CustoConnector\Cron\Send;
use Custobar\CustoConnector\Helper\Data;
use Custobar\CustoConnector\Model\Schedule;

use Custobar\CustoConnector\Test\Integration\CustoTestCase;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit_Framework_MockObject_MockObject;

class CronTest extends CustoTestCase
{
    const VAR_PATH = "tmp";

    /** @var  Data|PHPUnit_Framework_MockObject_MockObject */
    public $helperMock;
    /** @var  Send */
    public $custobarSendCron;

    /** @var  Schedule */
    public $schedule;

    /** @var  DirectoryList|PHPUnit_Framework_MockObject_MockObject */
    public $directoryListMock;

    protected function setUp()
    {
        $this->helperMock = $this
            ->getMockBuilder(Data::class)
            ->setMethods(
                [
                    'isInitialRunning',
                    'sendUpdateRequests',
                    'handleNextInitialPage',
                    'log'
                ]
            )
            ->disableOriginalConstructor()
            ->getMock();

        $objectManager =
            new ObjectManager(
                $this
            );

        $this->custobarSendCron = $objectManager->getObject(
            Send::class,
            [
                "helper" => $this->helperMock
            ]
        );


        $this->directoryListMock = $this->getMockBuilder(DirectoryList::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->schedule = $objectManager->getObject(
            Schedule::class,
            [
                "helper" => $this->helperMock,
                "directory_list" => $this->directoryListMock
            ]
        );
    }

    /**
     * @test
     * @group m2
     */
    public function testCron()
    {
        $this->helperMock->expects($this->atLeastOnce())
            ->method('isInitialRunning')
            ->willReturn(true);

        $this->helperMock->expects($this->never())
            ->method('sendUpdateRequests')
            ->willReturn(true);

        $this->helperMock->expects($this->atLeastOnce())
            ->method('log')
            ->willReturn(true);

        $this->custobarSendCron->execute();
    }

    public function testCronSending()
    {

        $this->helperMock->expects($this->atLeastOnce())
            ->method('isInitialRunning')
            ->willReturn(false);

        $this->helperMock->expects($this->atLeastOnce())
            ->method('sendUpdateRequests')
            ->willReturn(true);

        $this->helperMock->expects($this->never())
            ->method('log')
            ->willReturn(true);

        $this->custobarSendCron->execute();
    }

    public function testInitialScheduling()
    {

        $this->directoryListMock->expects($this->any())
            ->method('getPath')
            ->with('var')
            ->willReturn(self::VAR_PATH);

        $this->assertEquals("tmp/custconnector-initial.lock", $this->schedule->lockFilePath());

    }

    public function testInitialStarting()
    {
        $this->directoryListMock->expects($this->any())
            ->method('getPath')
            ->with('var')
            ->willReturn(self::VAR_PATH);

        $this->helperMock->expects($this->atLeastOnce())
            ->method('handleNextInitialPage')
            ->willReturn(true);

        $this->schedule->initialPopulation();

    }
}
