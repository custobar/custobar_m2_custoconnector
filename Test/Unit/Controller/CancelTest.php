<?php
use Custobar\CustoConnector\Controller\Adminhtml\CustoConnector\Cancel;
use Custobar\CustoConnector\Helper\Data;
use Custobar\CustoConnector\Model\Initial;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;

/**
 * Created by PhpStorm.
 * User: olli.tyynela
 * Date: 12.4.2017
 * Time: 20.52
 */
class CancelTest extends \Custobar\CustoConnector\Test\Integration\CustoTestCase
{

    /** @var  Data|\PHPUnit_Framework_MockObject_MockObject */
    public $helperMock;

    /** @var  Cancel */
    public $controller;
    public $initialsMock;

    protected function setUp()
    {
        $this->initialsMock = $this
            ->getMockBuilder(Initial::class)
            ->setMethods(
                [
                    'cleanInitials'
                ]
            )
            ->disableOriginalConstructor()
            ->getMock();


        $this->helperMock = $this
            ->getMockBuilder(Data::class)
            ->setMethods(
                [
                    'isInitialRunning',
                    'getInitialModel',
                    'cleanInitials'
                ]
            )
            ->disableOriginalConstructor()
            ->getMock();

        $objectManager =
            new ObjectManager(
                $this
            );

        $this->controller = $objectManager->getObject(
            Cancel::class,
            [
                "helper" => $this->helperMock
            ]
        );
    }

    public function testCancel()
    {

        $this->helperMock->expects($this->atLeastOnce())
            ->method('isInitialRunning')
            ->willReturn(true);

        $this->initialsMock->expects($this->atLeastOnce())
            ->method('cleanInitials')
            ->willReturn(true);

        $this->helperMock->expects($this->atLeastOnce())
            ->method('getInitialModel')
            ->will($this->returnValue($this->initialsMock));

        $this->controller->execute();
    }

    public function testWontCancel()
    {

        $this->helperMock->expects($this->atLeastOnce())
            ->method('isInitialRunning')
            ->willReturn(false);

        $this->initialsMock->expects($this->never())
            ->method('cleanInitials')
            ->willReturn(true);

        $this->helperMock->expects($this->never())
            ->method('getInitialModel')
            ->will($this->returnValue($this->initialsMock));

        $this->controller->execute();
    }

    public function cleanInitials()
    {
        return "test";
    }

}
