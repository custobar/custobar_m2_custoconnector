<?php

/**
 * Created by PhpStorm.
 * User: olli.tyynela
 * Date: 5.4.2017
 * Time: 16.41
 */

namespace Custobar\CustoConnector\Test\Unit\Model;

use Custobar\CustoConnector\Helper\Data;
use Custobar\CustoConnector\Observer\Main;
use Magento\Catalog\Model\Product;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use PHPUnit_Framework_MockObject_MockObject;

class ObserverTest extends \Custobar\CustoConnector\Test\Integration\CustoTestCase
{
    /**
     * @var Data|PHPUnit_Framework_MockObject_MockObject
     */
    public $helperMock;
    /**
     * @var Main
     */
    protected $custobarObserver;

    /**
     * @var \Magento\Framework\Event\Observer|PHPUnit_Framework_MockObject_MockObject
     */
    protected $observerMock;

    /**
     * @var \Magento\Framework\Event|PHPUnit_Framework_MockObject_MockObject
     */
    protected $eventMock;

    /**
     * @var \Magento\Framework\DataObject|PHPUnit_Framework_MockObject_MockObject
     */
    protected $objectMock;

    protected function setUp()
    {

        $this->observerMock = $this
            ->getMockBuilder('Magento\Framework\Event\Observer')
            ->disableOriginalConstructor()
            ->getMock();
        $this->eventMock = $this
            ->getMockBuilder('Magento\Framework\Event')
            ->setMethods(
                [
                    'getStatus',
                    'getRedirect',
                ]
            )
            ->disableOriginalConstructor()
            ->getMock();
        $this->objectMock = $this
            ->getMockBuilder('Magento\Framework\DataObject')
            ->setMethods(
                [
                    'setLoaded',
                    'setForwardModule',
                    'setForwardController',
                    'setForwardAction',
                    'setRedirectUrl',
                    'setRedirect',
                    'setPath',
                    'setArguments',
                ]
            )
            ->disableOriginalConstructor()
            ->getMock();

        $this->helperMock = $this
            ->getMockBuilder(Data::class)
            ->setMethods(
                [
                    'canProcess',
                    'getEvenObjectTrackedModel'
                ]
            )
            ->disableOriginalConstructor()
            ->getMock();

        $objectManager =
            new ObjectManager(
                $this
            );

        $this->custobarObserver = $objectManager->getObject(
            Main::class,
            [
                "_helper" => $this->helperMock

            ]
        );

    }

    /**
     * @test
     *
     * fails with a errror: Call to undefined method Mock_Data_96d6b53f::getIsModelInternal()
     */
    public function testMeme()
    {
        $this->markTestSkipped("this doesnt work but its a nice stub.");

        $this->observerMock
            ->expects($this->atLeastOnce())
            ->method('getEvent')
            ->willReturn($this->eventMock);
        $this->eventMock
            ->expects($this->atLeastOnce())
            ->method('getRedirect')
            ->willReturn($this->objectMock);
        $this->helperMock->method('canProcess')->willReturn(true);
        $this->helperMock->method('getEvenObjectTrackedModel')->willReturn(
            Product::class
        );


        $this->assertEquals(
            $this->custobarObserver,
            $this->custobarObserver->execute($this->observerMock)
        );
    }
}
