<?php

namespace Custobar\CustoConnector\Test\Unit\Log;

use Custobar\CustoConnector\Log\Logger;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
use Psr\Log\LogLevel;

class LoggerTest extends \Custobar\CustoConnector\Test\Integration\CustoTestCase

{

    /** @var Logger */
    public $custobarLogger;

    protected function setUp()
    {
        $this->helperMock = $this
            ->getMockBuilder(Data::class)
            ->setMethods(
                [
                    'isInitialRunning',
                    'sendUpdateRequests',
                    'log'
                ]
            )
            ->disableOriginalConstructor()
            ->getMock();

        $objectManager =
            new ObjectManager(
                $this
            );

        $this->custobarLogger = $objectManager->getObject(
            Logger::class
        );
    }


    /**
     * @test
     */
    public function testLogger()
    {
        $this->markTestIncomplete("Not implemented yet");
        $this->assertTrue(is_a($this->custobarLogger, Logger::class));
    }
}
