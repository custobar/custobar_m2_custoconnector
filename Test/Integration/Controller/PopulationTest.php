<?php

namespace Custobar\CustoConnector\Test\Integration\Controller;

use Custobar\CustoConnector\Helper\Data;
use Custobar\CustoConnector\Model\Initial;
use Custobar\CustoConnector\Model\ResourceModel\Initial\Collection;
use Custobar\CustoConnector\Test\Integration\TestTrait;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\TestFramework\Helper\Bootstrap;
use Magento\TestFramework\TestCase\AbstractBackendController;

use Magento\Framework\App\Route\ConfigInterface as RouteConfigInterface;

/**
 * Class Custobar_CustoConnector_Test_Controller_PopulationController
 *
 * @group Custobar_Controller
 */
class PopulationTest extends AbstractBackendController
{
    use TestTrait;

    /**
     * @var Data
     */
    private $helper;

    protected function setUp()
    {
        parent::setUp();

        $this->_objectManager = Bootstrap::getObjectManager();
        /** @var Data $helper */
        $this->helper = $this->_objectManager->get(
            Data::class
        );
    }

    /**
     * @test
     * @magentoAppArea frontend
     */
    public function testRouteIsNotConfiguredForFrontend()
    {
        /** @var RouteConfigInterface $routeConfig */
        $routeConfig =
            $this->_objectManager->create(RouteConfigInterface::class);
        $modules = $routeConfig->getModulesByFrontName('custobar');
        $this->assertNotContains(
            'Custobar_CustoConnector',
            $modules
        );
    }

    /**
     * @test
     * @magentoAppArea adminhtml
     * @group          m2
     */
    public function testRouteIsConfiguredForAdmin()
    {
        /** @var RouteConfigInterface $routeConfig */
        $routeConfig =
            $this->_objectManager->create(RouteConfigInterface::class);
        $modules = $routeConfig->getModulesByFrontName('custobar');
        $this->assertContains(
            'Custobar_CustoConnector',
            $modules
        );
    }

    /**
     * @test
     * @magentoAppArea frontend
     */
    public function testPermission()
    {
        $this->markTestSkipped("no special permissions requirements");
    }

    /**
     * @test
     * @magentoAppArea adminhtml
     * @group          m2
     */
    public function testStatusCode()
    {
        Data::$INITIAL_PAGE_SIZE = 2;

        $this->cleanInitials();

        $this->getRequest()->setParam('ajax', true)->setParam('isAjax', true);
        $this->dispatch('backend/custobar/custoconnector/helper');

        $html = $this->getResponse()->getBody();

        $this->assertContains(
            '{"errors":0,"queued":0}',
            $html,
            "Can get the unkown status for admin"
        );
    }

    /**
     * @test
     * @magentoAppArea       adminhtml
     * @magentoDataFixture   loadFixtureDefault
     * @group                m2
     */
    public function testInitialScheduling()
    {
        $this->helper->startInitialScheduling();

        $trackedModels = array_keys($this->helper->getConfigTrackedModels());
        $pages = 0;

        foreach ($trackedModels as $trackedModel) {
            if (!$this->helper->getIsModelInternal($trackedModel)) {
                /** @var AbstractCollection $expectedCollection */
                $expectedCollection =
                    $this->_objectManager->get($trackedModel)->getCollection();
                /** @var \Magento\Framework\Model\AbstractModel $expectedEntity */
                $pages += (int)ceil(
                    $expectedCollection->count()
                    / Data::$INITIAL_PAGE_SIZE
                );
            }
        }

        // had another dispatch here but cant use it as the second would cause response error
        $this->helper->handleNextInitialPage();

        $this->getRequest()->setParam('ajax', true)->setParam('isAjax', true);
        $this->dispatch('backend/custobar/custoconnector/population');

        $html = $this->getResponse()->getBody();

        $percentage = round((1 / $pages) * 100, 2);

        $this->assertContains(
            "{\"status\":\"Processing\",\"message\":\"Processed pages {$percentage}%\",\"processing\":\"1\"}",
            $html,
            "Can get the processing status"
        );
    }

    /**
     * @test
     * @magentoAppArea adminhtml
     * @magentoDataFixture  loadFixtureStatisticsAction
     * @group               m2
     */
    public function testScheduleStatus()
    {
        $this->getRequest()->setParam('ajax', true)->setParam('isAjax', true);
        $this->dispatch('backend/custobar/custoconnector/helper');

        $html = $this->getResponse()->getBody();

        $this->assertContains(
            '{"errors":2,"queued":3}',
            $html,
            "Can get the unkown status for admin"
        );
    }

    /**
     * @test
     * @magentoAppArea adminhtml
     */
    public function testStartActionNoParams()
    {
        $this->getRequest()->setParam('ajax', true)->setParam('isAjax', true);
        $this->getRequest()->setPostValue(
            'model',
            "all"
        );
        $this->dispatch('backend/custobar/custoconnector/population');

        $html = $this->getResponse()->getBody();
        $this->assertContains(
            '{"status":"Started","processing":"0","message":"Initials created and enqueued for all"}',
            $html,
            "Did start for all"
        );
    }

    /**
     * @test
     * @magentoDataFixture   loadFixtureDefault
     * @magentoAppArea       adminhtml
     * @group                m2
     */
    public function testStartActionWithParams()
    {
        $this->getRequest()->setParam('ajax', true)->setParam('isAjax', true);
        $this->getRequest()->setPostValue(
            'model',
            'Magento\Customer\Model\Customer'
        );

        $this->dispatch('backend/custobar/custoconnector/population');

        $html = $this->getResponse()->getBody();
        $this->assertContains(
            '"status":"Started","processing":"0","message',
            $html,
            'Did start for Magento\Customer\Model\Customer'
        );

        $this->assertContains(
            'Customer',
            $html,
            'Did start for Magento\Customer\Model\Customer'
        );

        /** @var Initial $model */
        $model = $this->getInitial();

        /** @var Collection $collection */
        $collection = $model->getCollection();

        $collectionData = $collection->getData();
        self::assertCount(1, $collectionData);

        self::assertEquals(1, $collectionData[0]["pages"], "Customer pages");
        self::assertEquals(0, $collectionData[0]["page"]);
        self::assertEquals(
            'Magento\Customer\Model\Customer',
            $collectionData[0]["entity_type"]
        );
    }

    public static function loadFixtureStatisticsAction()
    {
        /** @noinspection PhpUnusedLocalVariableInspection */
        $file = __DIR__ . '/_files/statisticsaction.yaml';
        /** @noinspection PhpUnusedLocalVariableInspection */
        $tables = ['custobar_schedule'];
        include __DIR__ . '/../_files/tables.php';
    }
}
