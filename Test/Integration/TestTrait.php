<?php
/**
 * Created by PhpStorm.
 * User: olli.tyynela
 * Date: 5.4.2017
 * Time: 14.04
 */

namespace Custobar\CustoConnector\Test\Integration;

use Custobar\CustoConnector\Model\Initial;
use Custobar\CustoConnector\Model\ResourceModel\Schedule\Collection;
use Custobar\CustoConnector\Model\Schedule;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\Website;
use Magento\TestFramework\Helper\Bootstrap;
use Magento\TestFramework\Store\StoreManager;

trait TestTrait
{
    /** @var \Magento\Framework\App\MutableScopeConfig */
    public $config;

    public static function loadFixtureDefault()
    {
        include __DIR__ . '/_files/default.php';
    }

    public static function enableDefaultWebsite()
    {
        $objectManager = Bootstrap::getObjectManager();

        /** @var \Magento\Framework\App\Config\Storage\WriterInterface $configWriter */
        $configWriter = $objectManager->get(
            \Magento\Framework\App\Config\Storage\WriterInterface::class
        );

        $configWriter->save(
            'custobar/custobar_custoconnector/allowed_websites',
            '1',
            'default'
        );

        $configWriter->save(
            'custobar/custobar_custoconnector/allowed_websites',
            '1',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORES,
            '1'
        );

        $configWriter->save(
            'custobar/custobar_custoconnector/apikey',
            'apikey',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORES,
            1
        );

        $configWriter->save(
            'custobar/custobar_custoconnector/prefix',
            'prefix',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORES,
            1
        );

        $objectManager->get(\Magento\Store\Model\StoreManagerInterface::class)->reinitStores();
    }

    public static function loadFixtureDefaultRollback()
    {
        include __DIR__ . '/_files/default_rollback.php';
    }

    public static function loadFixtureImageTable()
    {
        /** @noinspection PhpUnusedLocalVariableInspection */
        $file = __DIR__ . '/Helper/_files/image.yaml';
        /** @noinspection PhpUnusedLocalVariableInspection */
        $tables = [
            'catalog_product_entity_media_gallery',
            'catalog_product_entity_media_gallery_value'
        ];
        include __DIR__ . '/_files/tables.php';
    }

    public static function loadFixtureNewsLetterTable()
    {
        /** @noinspection PhpUnusedLocalVariableInspection */
        $file = __DIR__ . '/Helper/_files/newsletter.yaml';
        /** @noinspection PhpUnusedLocalVariableInspection */
        $tables = ['newsletter_subscriber'];
        include __DIR__ . '/_files/tables.php';
    }

    public static function loadFixtureInitialTable()
    {
        /** @noinspection PhpUnusedLocalVariableInspection */
        $file = __DIR__ . '/Helper/_files/initial.yaml';
        /** @noinspection PhpUnusedLocalVariableInspection */
        $tables = ['custobar_initial'];
        include __DIR__ . '/_files/tables.php';
    }

    /**
     * @return Initial
     */
    public function getInitial()
    {

        $om = Bootstrap::getObjectManager();
        return $om->get(
            Initial::class
        );
    }

    /**
     * @param $sku
     *
     * @return ProductInterface|mixed
     */
    public function getProductBySku($sku)
    {

        /** @var ProductRepository $repository */
        $repository =
            $this->_objectManager->create(
                'Magento\Catalog\Model\ProductRepository'
            );

        $product = $repository->get($sku);
        return $product;
    }

    /**
     * @return Schedule
     */
    public function getSchedule()
    {
        $om = Bootstrap::getObjectManager();
        return $om->get(
            Schedule::class
        );
    }

    /**
     * @return Collection
     */
    public function getAllSchedules()
    {
        /** @var Collection $schedules */
        $schedules = $this->getSchedule()
            ->getCollection();
        return $schedules;
    }

    public function alterDefaults()
    {
        $om = Bootstrap::getObjectManager();
        /** @var Website $website */
        $website = $om->create('Magento\Store\Model\Website');
        $website->load(1);
        $website->setIsDefault(0);
        $website->save();
        $website->load(2);
        $website->setIsDefault(1);
        $website->save();
        $this->getStoreManager()->reinitStores();
    }

    public function restoreDefaults()
    {
        $om = Bootstrap::getObjectManager();
        /** @var Website $website */
        $website = $om->create('Magento\Store\Model\Website');
        $website->load(2);
        $website->setIsDefault(0);
        $website->save();
        $website->load(1);
        $website->setIsDefault(1);
        $website->save();

        $om->get('Magento\Store\Model\StoreManagerInterface')->reinitStores();
    }

    /**
     * @return \Magento\Framework\ObjectManagerInterface
     */
    public function getObjectManager()
    {
        $objectManager = Bootstrap::getObjectManager();
        return $objectManager;
    }

    protected function clearSchedules()
    {
        /** @var \Custobar\CustoConnector\Model\ResourceModel\Schedule\Collection $schedules */
        $schedules = $this->getAllSchedules();

        /** @var Schedule $schedule */
        foreach ($schedules as $schedule) {
            $schedule->delete();
        }
    }

    /**
     * @return \Magento\Framework\App\MutableScopeConfig
     */
    protected function Config()
    {
        $objectManager = $this->getObjectManager();
        $context = $objectManager->get(Context::class);
        return $context->getScopeConfig();
    }

    /**
     * @param $path
     * @param $value
     */
    protected function setConfigValue($path, $value, $scope = \Magento\Framework\App\ScopeInterface::SCOPE_DEFAULT, $scopeCode = null)
    {
        $this->Config()->setValue($path, $value, $scope, $scopeCode);
    }

    /**
     * @return StoreManager
     */
    protected function getStoreManager()
    {
        $objectManager = $this->getObjectManager();
        return $objectManager->get(StoreManager::class);
    }

    /**
     * @param null $id
     *
     * @return \Magento\Store\Model\Store\|null|string
     */
    protected function getStore($id = null)
    {
        return $this->getStoreManager()->getStore($id);
    }

    public function cleanInitials()
    {
        /** @var Initial $model */
        $model = $this->getInitial();
        $model->cleanInitials();
    }
}
