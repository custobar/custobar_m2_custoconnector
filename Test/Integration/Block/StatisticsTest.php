<?php

namespace Custobar\CustoConnector\Test\Integration\Block;

use Custobar\CustoConnector\Helper\Data;
use Custobar\CustoConnector\Test\Integration\TestTrait;
use Magento\TestFramework\TestCase\AbstractController;

class StatisticsTest extends AbstractController
{
    use TestTrait;

    /** @var Data */
    public $helper;

    protected function setUp()
    {
        parent::setUp();

        /** @var Data $helper */
        $this->helper = $this->_objectManager->get(
            Data::class
        );
    }

    /**
     * @test
     * @magentoConfigFixture default_store custobar/custobar_custoconnector/allowed_websites 1
     * @magentoConfigFixture default_store custobar/custobar_custoconnector/prefix prefixthatdoesntexists
     * @magentoConfigFixture default_store custobar/custobar_custoconnector/apikey prefixthatdoesntexists
     * @magentoConfigFixture default_store custobar/custobar_custoconnector/tracking_script
     * @group                m2
     */
    public function testNoTrackingCode()
    {
        $this->assertEquals("", $this->helper->getTrackingScript());

        $this->dispatch("/");
        $html = $this->getResponse()->getBody();
        $this->assertNotContains(
            "'https://www.custobar.com/js/v1/custobar.js', 'cstbr', 'cstbrConfig'",
            $html,
            "Assert that code is not present as its set to empty"
        );
    }

    /**
     * @test
     * @magentoDataFixture   Magento/Customer/_files/customer.php
     * @magentoDataFixture   Magento/Catalog/controllers/_files/products.php
     * @magentoConfigFixture default_store custobar/custobar_custoconnector/allowed_websites 1
     * @magentoConfigFixture default_store custobar/custobar_custoconnector/prefix prefixthatdoesntexists
     * @magentoConfigFixture default_store custobar/custobar_custoconnector/apikey prefixthatdoesntexists
     * @magentoConfigFixture default_store custobar/custobar_custoconnector/tracking_script (function(c,u,s,t,o,b,a,r) {var e;c[o]=[];c[b] = a;e = u.createElement(s);r=u.getElementsByTagName(s)[0];e.async=1;e.src = t;r.parentNode.insertBefore(e, r);})(window, document, 'script', 'https://www.custobar.com/js/v1/custobar.js', 'cstbr', 'cstbrConfig', {'_companyToken':'ABCD1234'});
     * @group                m2
     */
    public function testNotLoggedIn()
    {
        $this->dispatch("/");
        $html = $this->getResponse()->getBody();

        $this->assertContains(
            "'https://www.custobar.com/js/v1/custobar.js', 'cstbr', 'cstbrConfig'",
            $html,
            "Assert that we get the code when its set"
        );

        $this->assertNotContains(
            "cstbrConfig.customerId",
            $html,
            "assert that no customer details is present"
        );
        $this->assertNotContains(
            "cstbrConfig.productId",
            $html,
            "assert that product line is not outputted"
        );
    }

    /**
     * Login the user
     *
     * @param string $customerId Customer to mark as logged in for the session
     *
     * @return void
     */
    protected function login($customerId)
    {
        /** @var \Magento\Customer\Model\Session $session */
        $session = $this->_objectManager
            ->get('Magento\Customer\Model\Session');
        $session->loginById($customerId);
    }

    /**
     * @test
     * @magentoDataFixture   Magento/Customer/_files/customer.php
     * @magentoDataFixture   Magento/Catalog/controllers/_files/products.php
     * @magentoConfigFixture default_store custobar/custobar_custoconnector/allowed_websites 1,0
     * @magentoConfigFixture default_store custobar/custobar_custoconnector/prefix prefixthatdoesntexists
     * @magentoConfigFixture default_store custobar/custobar_custoconnector/apikey prefixthatdoesntexists
     * @magentoConfigFixture default_store custobar/custobar_custoconnector/tracking_script (function(c,u,s,t,o,b,a,r) {var e;c[o]=[];c[b] = a;e = u.createElement(s);r=u.getElementsByTagName(s)[0];e.async=1;e.src = t;r.parentNode.insertBefore(e, r);})(window, document, 'script', 'https://www.custobar.com/js/v1/custobar.js', 'cstbr', 'cstbrConfig', {'_companyToken':'ABCD1234'});
     * @group                m2
     */
    public function testLoggedIn()
    {
        $this->login(1);

        $product = $this->getProductBySku('simple_product_1');

        $this->dispatch(
            sprintf('catalog/product/view/id/%s', $product->getEntityId())
        );

        $html = $this->getResponse()->getBody();

        $this->assertContains(
            "cstbrConfig.customerId = \"1\";",
            $html,
            "Is customer id present"
        );
        $this->assertContains(
            "cstbrConfig.productId = \"simple_product_1\";",
            $html,
            "Is cb.track_browse_product present"
        );
    }

    /**
     * @test
     * @magentoDataFixture   Magento/Customer/_files/customer.php
     * @magentoDataFixture   Magento/Catalog/controllers/_files/products.php
     * @magentoConfigFixture default_store custobar/custobar_custoconnector/allowed_websites 100
     * @magentoConfigFixture default_store custobar/custobar_custoconnector/prefix prefixthatdoesntexists
     * @magentoConfigFixture default_store custobar/custobar_custoconnector/apikey prefixthatdoesntexists
     * @magentoConfigFixture default_store custobar/custobar_custoconnector/tracking_script (function(c,u,s,t,o,b,a,r) {var e;c[o]=[];c[b] = a;e = u.createElement(s);r=u.getElementsByTagName(s)[0];e.async=1;e.src = t;r.parentNode.insertBefore(e, r);})(window, document, 'script', 'https://www.custobar.com/js/v1/custobar.js', 'cstbr', 'cstbrConfig', {'_companyToken':'ABCD1234'});
     * @group                m2
     */
    public function testWebsiteNotAllowed()
    {
        $this->login(1);

        $product = $this->getProductBySku('simple_product_1');

        $this->dispatch(
            sprintf('catalog/product/view/id/%s', $product->getEntityId())
        );

        $html = $this->getResponse()->getBody();

        $this->assertNotContains(
            "'https://www.custobar.com/js/v1/custobar.js', 'cstbr', 'cstbrConfig'",
            $html,
            "Assert that custobar code is not present"
        );
    }
}
