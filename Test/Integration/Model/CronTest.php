<?php

namespace Custobar\CustoConnector\Test\Integration\Model;

use Custobar\CustoConnector\Test\Integration\CustoTestCase;
use Magento\Cron\Model\ScheduleFactory;
use Magento\TestFramework\Helper\Bootstrap;
use Magento\TestFramework\ObjectManager;

/**
 * Class Custobar_CustoConnector_Test_Model_Cron
 *
 * @group Custobar_Cron
 * @group Custobar_Model
 */
class CronTest extends CustoTestCase
{

    /** @var  ObjectManager */
    public $objectManager;
    /** @var  ScheduleFactory */
    public $scheduleFactory;

    protected function setUp()
    {
        $this->objectManager = Bootstrap::getObjectManager();

        $this->scheduleFactory =
            $this->objectManager->get(
                ScheduleFactory::class
            );
    }

    /**
     * @test
     * @group m2
     */
    public function testCron()
    {
        $this->markTestIncomplete(
            "Can we test that the cron schedules are present?"
        );
    }
}
