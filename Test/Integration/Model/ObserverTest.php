<?php

namespace Custobar\CustoConnector\Test\Integration\Model;

use Custobar\CustoConnector\Helper\Data;
use Custobar\CustoConnector\Test\Integration\CustoTestCase;
use Custobar\CustoConnector\Test\Integration\TestTrait;
use Magento\Catalog\Model\ProductFactory;
use Magento\Newsletter\Model\Subscriber;
use Magento\Newsletter\Model\SubscriberFactory;
use \Magento\TestFramework\Helper\Bootstrap;
use Magento\TestFramework\Store\StoreManager;
use Magento\TestFramework\TestCase\AbstractBackendController;

/**
 * Class Custobar_CustoConnector_Test_Model_Observer
 *
 * @group Custobar_Observer
 * @group Custobar_Model
 */
class ObserverTest extends CustoTestCase
{
    use TestTrait;

    /** @var  \Magento\TestFramework\ObjectManager */
    public $objectManager;

    /** @var \Custobar\CustoConnector\Helper\Data $helper */
    public $helper;

    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    protected $customerCustomerFactory;

    /**
     * @var \Magento\Customer\Model\AddressFactory
     */
    protected $customerAddressFactory;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $catalogProductFactory;

    /**
     * @var \Magento\Newsletter\Model\SubscriberFactory
     */
    protected $newsletterSubscriberFactory;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * @var \Magento\Quote\Model\QuoteFactory
     */
    protected $quoteQuoteFactory;

    /**
     * @var \Magento\Framework\DB\TransactionFactory
     */
    protected $transactionFactory;

    protected function setUp()
    {
        $this->objectManager = Bootstrap::getObjectManager();

        $this->customerCustomerFactory =
            $this->objectManager->get('Magento\Customer\Model\CustomerFactory');
        $this->customerAddressFactory =
            $this->objectManager->get('Magento\Customer\Model\AddressFactory');
        $this->storeManager = $this->objectManager->get(StoreManager::class);
        $this->catalogProductFactory =
            $this->objectManager->get(ProductFactory::class);
        $this->newsletterSubscriberFactory =
            $this->objectManager->get(SubscriberFactory::class);


        /** @var \Custobar\CustoConnector\Helper\Data $helper */
        $this->helper = $this->objectManager->get(
            '\Custobar\CustoConnector\Helper\Data'
        );
    }

    /**
     * @test
     * @magentoDataFixture   loadFixtureDefault
     * @magentoDbIsolation disabled
     * @group                m2
     * @magentoConfigFixture current_store custobar/custobar_custoconnector/allowed_websites 10
     * @magentoConfigFixture current_store custobar/custobar_custoconnector/prefix prefixthatdoesntexists
     * @magentoConfigFixture current_store custobar/custobar_custoconnector/apikey prefixthatdoesntexists
     */
    public function testWebsiteCustomerEvents()
    {
        $this->alterDefaults();
        $this->clearSchedules();

        $this->assertEquals(
            "prefixthatdoesntexists",
            $this->helper->getApiPrefix()
        );

        $this->assertEquals(
            "prefixthatdoesntexists",
            $this->helper->getApiKey()
        );

        $this->assertEquals(
            ["10"],
            $this->helper->getCommaSeparatedConfigValue(
                Data::$CONFIG_ALLOWED_WEBSITES
            )
        );

        /** @var \Magento\Customer\Model\Customer $jane */
        $jane = $this->customerCustomerFactory->create()->load(2);
        $this->assertEquals("Jane Doe", $jane->getName());
        $jane->setFirstname("Jane Updated");
        $jane->setHasDataChanges(true);
        $jane->save();

        $data = $this->getAllSchedules()->getData();
        $this->assertCount(
            0,
            $data,
            "assert that we have no events as the website was not allowed"
        );
    }

    /**
     * @test
     * @magentoDataFixture loadFixtureDefault
     * @magentoDbIsolation disabled
     * @group              m2
     */
    public function testCustomerEvents()
    {
        $this->alterDefaults();
        $this->clearSchedules();

        $this->assertCount(
            0,
            $this->getAllSchedules()->getData(),
            "No schedules should be present as we just cleared the them"
        );

        /** @var \Magento\Customer\Model\Customer $john */
        $john = $this->customerCustomerFactory->create()->load(1);
        $this->assertEquals(
            "John Doe",
            $john->getName(),
            "Assert that our fixture customer is present"
        );
        $john->setData("some_data", "yay");
        $john->save();

        /** @var \Magento\Customer\Model\Customer $jane */
        $jane = $this->customerCustomerFactory->create()->load(2);
        $this->assertEquals("Jane Doe", $jane->getName());

        $jane->setFirstname("Jane Updated");
        $jane->setHasDataChanges(true);
        $jane->save();

        $schedules = $this->getAllSchedules();

        $data = $schedules->getData();

        $this->assertCount(2, $data);

        $this->assertEquals(1, $data[0]["entity_id"]);
        $this->assertContains(
            'Magento\Customer\Model\Customer',
            $data[0]["entity_type"]
        );

        $this->assertEquals(2, $data[1]["entity_id"]);
        $this->assertContains(
            'Magento\Customer\Model\Customer',
            $data[1]["entity_type"]
        );

        /** @var \Magento\Customer\Model\Customer $whois1 */
        $whois1 = $this->customerCustomerFactory->create()
            ->load($data[1]["entity_id"]);
        $this->assertEquals("Jane Updated Doe", $whois1->getName());

        /** @var \Magento\Customer\Model\Address $address1 */
        $address1 = $this->customerAddressFactory->create()
            ->load($data[0]["entity_id"]);

        $this->assertEquals(
            "Address 123",
            implode(",", $address1->getStreet())
        );

        $this->assertEquals(
            $data[0]["entity_id"],
            $address1->getCustomer()->getId()
        );
    }

    /**
     * @test
     * @magentoDataFixture loadFixtureDefault
     * @magentoDbIsolation disabled
     * @group              m2
     */
    public function testAddressEvents()
    {
        $this->alterDefaults();
        $this->clearSchedules();

        $this->setConfigValue(
            "custobar/custobar_custoconnector/prefix",
            "prefixthatdoesntexists"
        );
        $this->setConfigValue(
            "custobar/custobar_custoconnector/apikey",
            "prefixthatdoesntexists"
        );
        $this->setConfigValue(
            "custobar/custobar_custoconnector/allowed_websites",
            "2,3"
        );

        $this->clearSchedules();

        /** @var \Magento\Customer\Model\Address $johnAddress */
        $johnAddress = $this->customerAddressFactory->create()->load(1);
        $this->assertEquals("John Doe", $johnAddress->getName());

        $johnAddress->setStreet("new street from the model");
        $johnAddress->setHasDataChanges(true);
        $johnAddress->save();

        $schedules = $this->getAllSchedules();
        $data = $schedules->getData();

        $this->assertCount(1, $data);
        $this->assertEquals(1, $data[0]["entity_id"]);
        $this->assertEquals(
            'Magento\Customer\Model\Customer',
            $data[0]["entity_type"]
        );

        /** @var \Magento\Customer\Model\Address $address1 */
        $address1 = $this->customerAddressFactory->create()
            ->load($data[0]["entity_id"]);

        $this->assertEquals(
            "new street from the model",
            implode(",", $address1->getStreet())
        );

        $this->assertEquals(
            $data[0]["entity_id"],
            $address1->getCustomer()->getId()
        );
    }

    /**
     * @test
     * @magentoDataFixture enableDefaultWebsite
     * @magentoDataFixture Magento/Sales/_files/order.php
     * @group              m2
     */
    public function testOrderEvents()
    {
        $schedules =
            $this->getAllSchedules()->addFieldToFilter(
                "entity_type",
                'Magento\Sales\Model\Order'
            );

        $this->assertCount(
            1,
            $schedules->getData(),
            "Test we have one schedule for only one order per the fixture file"
        );
    }

    /**
     * @test
     * @magentoDataFixture   loadFixtureDefault
     * @magentoDataFixture   loadFixtureImageTable
     * @magentoConfigFixture current_store custobar/custobar_custoconnector/allowed_websites 2
     * @group                m2
     */
    public function testProductEvents()
    {
        $this->clearSchedules();

        /** @var \Magento\Catalog\Model\Product $product */
        $product = $this->catalogProductFactory->create()->load(22201);
        $product->setData("test_data", 1);
        $product->save();

        $storeIds = $product->getStoreIds();

        $product = $this->catalogProductFactory->create()->load(31001);
        $product->setData("test_data", 1);
        $product->save();

        /** @var \Magento\Catalog\Model\Product $product */
        $product =
            $this->catalogProductFactory->create()->setStoreId(2)->load(22111);
        $product->setData("test_data", 1);
        $product->save();

        $schedules = $this->getAllSchedules();

        $this->assertCount(
            6,
            $schedules->getData(),
            "Have we got only 6 schedules present: 3 storeviews * products. All translations for the products 21001 and 22111, but none for the 31001 as its in a website that is not allowed."

        );

        $schedules->resetData();
        $schedules->clear();

        $schedules->addFieldToFilter(
            "entity_id",
            31001
        );

        $this->assertCount(
            0,
            $schedules->getData(),
            "We dont have any events for 31001"
        );
    }

    /**
     * @test
     * @magentoDataFixture   loadFixtureDefault
     * @magentoDataFixture   loadFixtureNewsLetterTable
     * @magentoConfigFixture current_store custobar/custobar_custoconnector/allowed_websites 2
     * @group                m2
     */
    public function testNewsLetterEvents()
    {
        $this->clearSchedules();

        /** @var Subscriber $newsletterSubscriberJojo */
        $newsletterSubscriberJojo =
            $this->newsletterSubscriberFactory->create()->load(12);

        $newsletterSubscriberJojo->setStatus(
            Subscriber::STATUS_UNSUBSCRIBED
        );
        $newsletterSubscriberJojo->save();

        /** @var Subscriber $newsletterSubscriberJohn */
        $newsletterSubscriberJohn =
            $this->newsletterSubscriberFactory->create()->load(13);
        $newsletterSubscriberJohn->setStatus(
            Subscriber::STATUS_SUBSCRIBED
        );
        $newsletterSubscriberJohn->save();

        $schedules = $this->getAllSchedules();
        $schedules->addFilter(
            "entity_type",
            'Magento\Newsletter\Model\Subscriber'
        );

        $schedulesData = $schedules->getData();
        $this->assertCount(
            1,
            $schedulesData,
            "Assert that we have only 1 newsletter event present"
        );

        self::assertEquals("12", $schedulesData[0]["entity_id"]);
    }


    protected function tearDown()
    {
        $name = $this->getName();

        // testOrderEvents fails with LocalizedException without this
        if ($name !== "testOrderEvents") {
            $this->restoreDefaults();
        }
    }
}
