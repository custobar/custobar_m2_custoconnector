<?php

if (in_array('catalog_product_entity_media_gallery', $tables)) {
    require __DIR__ . '/product_image.php';
}

use Magento\Sales\Model\Order\Payment;

// Magentos Zend_Config_Yaml wont do, so Spyc.
// \Zend\Config\Reader\Yaml might work also
$defaults = Spyc::YAMLLoad($file);
$objectManager = \Magento\TestFramework\Helper\Bootstrap::getObjectManager();
$mapping = [
    'custobar_schedule' => \Custobar\CustoConnector\Model\Schedule::class,
    'custobar_initial' => \Custobar\CustoConnector\Model\Initial::class,
    'sales_order' => \Magento\Sales\Model\Order::class,
    'sales_order_address' => \Magento\Sales\Model\Order\Address::class,
    'sales_order_item' => \Magento\Sales\Model\Order\Item::class,
    'sales_order_payment' => \Magento\Sales\Model\Order\Payment::class,
    'newsletter_subscriber' => \Magento\Newsletter\Model\Subscriber::class,
];

$resource = $objectManager->create('\Magento\Store\Model\ResourceModel\Store');
$select = $resource->getConnection()->select()->from($resource->getMainTable());
$stockItems = $resource->getConnection()->fetchAll($select);

// column names from sales_order and sales_order_item eg below:
// select CONCAT("'", column_name, "',")
// from INFORMATION_SCHEMA.COLUMNS
// where TABLE_NAME='sales_order_item'
$sales_order_columns = [
    'entity_id',
    'state',
    'status',
    'coupon_code',
    'protect_code',
    'shipping_description',
    'is_virtual',
    'store_id',
    'customer_id',
    'base_discount_amount',
    'base_discount_canceled',
    'base_discount_invoiced',
    'base_discount_refunded',
    'base_grand_total',
    'base_shipping_amount',
    'base_shipping_canceled',
    'base_shipping_invoiced',
    'base_shipping_refunded',
    'base_shipping_tax_amount',
    'base_shipping_tax_refunded',
    'base_subtotal',
    'base_subtotal_canceled',
    'base_subtotal_invoiced',
    'base_subtotal_refunded',
    'base_tax_amount',
    'base_tax_canceled',
    'base_tax_invoiced',
    'base_tax_refunded',
    'base_to_global_rate',
    'base_to_order_rate',
    'base_total_canceled',
    'base_total_invoiced',
    'base_total_invoiced_cost',
    'base_total_offline_refunded',
    'base_total_online_refunded',
    'base_total_paid',
    'base_total_qty_ordered',
    'base_total_refunded',
    'discount_amount',
    'discount_canceled',
    'discount_invoiced',
    'discount_refunded',
    'grand_total',
    'shipping_amount',
    'shipping_canceled',
    'shipping_invoiced',
    'shipping_refunded',
    'shipping_tax_amount',
    'shipping_tax_refunded',
    'store_to_base_rate',
    'store_to_order_rate',
    'subtotal',
    'subtotal_canceled',
    'subtotal_invoiced',
    'subtotal_refunded',
    'tax_amount',
    'tax_canceled',
    'tax_invoiced',
    'tax_refunded',
    'total_canceled',
    'total_invoiced',
    'total_offline_refunded',
    'total_online_refunded',
    'total_paid',
    'total_qty_ordered',
    'total_refunded',
    'can_ship_partially',
    'can_ship_partially_item',
    'customer_is_guest',
    'customer_note_notify',
    'billing_address_id',
    'customer_group_id',
    'edit_increment',
    'email_sent',
    'send_email',
    'forced_shipment_with_invoice',
    'payment_auth_expiration',
    'quote_address_id',
    'quote_id',
    'shipping_address_id',
    'adjustment_negative',
    'adjustment_positive',
    'base_adjustment_negative',
    'base_adjustment_positive',
    'base_shipping_discount_amount',
    'base_subtotal_incl_tax',
    'base_total_due',
    'payment_authorization_amount',
    'shipping_discount_amount',
    'subtotal_incl_tax',
    'total_due',
    'weight',
    'customer_dob',
    'increment_id',
    'applied_rule_ids',
    'base_currency_code',
    'customer_email',
    'customer_firstname',
    'customer_lastname',
    'customer_middlename',
    'customer_prefix',
    'customer_suffix',
    'customer_taxvat',
    'discount_description',
    'ext_customer_id',
    'ext_order_id',
    'global_currency_code',
    'hold_before_state',
    'hold_before_status',
    'order_currency_code',
    'original_increment_id',
    'relation_child_id',
    'relation_child_real_id',
    'relation_parent_id',
    'relation_parent_real_id',
    'remote_ip',
    'shipping_method',
    'store_currency_code',
    'store_name',
    'x_forwarded_for',
    'customer_note',
    'created_at',
    'updated_at',
    'total_item_count',
    'customer_gender',
    'discount_tax_compensation_amount',
    'base_discount_tax_compensation_amount',
    'shipping_discount_tax_compensation_amount',
    'base_shipping_discount_tax_compensation_amnt',
    'discount_tax_compensation_invoiced',
    'base_discount_tax_compensation_invoiced',
    'discount_tax_compensation_refunded',
    'base_discount_tax_compensation_refunded',
    'shipping_incl_tax',
    'base_shipping_incl_tax',
    'coupon_rule_name',
    'paypal_ipn_customer_notified',
    'gift_message_id',
    // items
    'item_id',
    'order_id',
    'parent_item_id',
    'quote_item_id',
    'store_id',
    'created_at',
    'updated_at',
    'product_id',
    'product_type',
    'product_options',
    'weight',
    'is_virtual',
    'sku',
    'name',
    'description',
    'applied_rule_ids',
    'additional_data',
    'is_qty_decimal',
    'no_discount',
    'qty_backordered',
    'qty_canceled',
    'qty_invoiced',
    'qty_ordered',
    'qty_refunded',
    'qty_shipped',
    'base_cost',
    'price',
    'base_price',
    'original_price',
    'base_original_price',
    'tax_percent',
    'tax_amount',
    'base_tax_amount',
    'tax_invoiced',
    'base_tax_invoiced',
    'discount_percent',
    'discount_amount',
    'base_discount_amount',
    'discount_invoiced',
    'base_discount_invoiced',
    'amount_refunded',
    'base_amount_refunded',
    'row_total',
    'base_row_total',
    'row_invoiced',
    'base_row_invoiced',
    'row_weight',
    'base_tax_before_discount',
    'tax_before_discount',
    'ext_order_item_id',
    'locked_do_invoice',
    'locked_do_ship',
    'price_incl_tax',
    'base_price_incl_tax',
    'row_total_incl_tax',
    'base_row_total_incl_tax',
    'discount_tax_compensation_amount',
    'base_discount_tax_compensation_amount',
    'discount_tax_compensation_invoiced',
    'base_discount_tax_compensation_invoiced',
    'discount_tax_compensation_refunded',
    'base_discount_tax_compensation_refunded',
    'tax_canceled',
    'discount_tax_compensation_canceled',
    'tax_refunded',
    'base_tax_refunded',
    'discount_refunded',
    'base_discount_refunded',
    'free_shipping',
    'gift_message_id',
    'gift_message_available',
    'weee_tax_applied',
    'weee_tax_applied_amount',
    'weee_tax_applied_row_amount',
    'weee_tax_disposition',
    'weee_tax_row_disposition',
    'base_weee_tax_applied_amount',
    'base_weee_tax_applied_row_amnt',
    'base_weee_tax_disposition',
    'base_weee_tax_row_disposition',
    // schedules
    'entity_type',
    'processed_at',
    'id',
    'entity_id',
    'errors',
    // newsletter
    'subscriber_id',
    'store_id',
    'change_status_at',
    'customer_id',
    'subscriber_email',
    'subscriber_status',
    'subscriber_confirm_code',
    // initial
    'id',
    'page',
    'pages',
    'entity_type',
    'created_at',
];

foreach ($tables as $table) {
    if (!empty($mapping[$table])) {
        $model = $objectManager->create($mapping[$table] . 'Factory');
        $resource = $model->create()->getResource();
        $connection = $resource->getConnection();
        foreach ($defaults['tables'][$table] as $data) {

            // ditch unknown(legacy) attributes
            $filtered = array_filter(
                $data,
                function ($key) use ($sales_order_columns) {
                    return in_array($key, $sales_order_columns);
                },
                ARRAY_FILTER_USE_KEY
            );

            $connection->insert($resource->getMainTable(), $filtered);
        }
    } elseif ($table == 'catalog_product_entity_media_gallery') {
        foreach ($defaults['tables'][$table] as $data) {
            /** @var Magento\Catalog\Model\ProductRepository $productRepository */
            $productRepository = $objectManager->create(
                'Magento\Catalog\Api\ProductRepositoryInterface'
            );
            $product = $productRepository->getById($data['entity_id']);

            /** @var $product \Magento\Catalog\Model\Product */
            $product->setStoreId(0)
                ->setImage($data['value'])
                ->setSmallImage($data['value'])
                ->setThumbnail($data['value'])
                ->setData(
                    'media_gallery',
                    [
                        'images' => [
                            [
                                'file' => $data['value'],
                                'position' => 1,
                                'label' => 'Image Alt Text',
                                'disabled' => 0,
                                'media_type' => 'image'
                            ],
                        ]
                    ]
                )
                ->setCanSaveCustomOptions(true)
                ->save();
        }
    }
}
