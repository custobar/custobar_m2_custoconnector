<?php

use Magento\Framework\App\Config\ScopeConfigInterface;

$objectManager = \Magento\TestFramework\Helper\Bootstrap::getObjectManager();
$defaults = Spyc::YAMLLoad(__DIR__ . '/default.yaml');

/** @var \Magento\Framework\Registry $registry */
$registry = \Magento\TestFramework\Helper\Bootstrap::getObjectManager()->get(\Magento\Framework\Registry::class);

$registry->unregister('isSecureArea');
$registry->register('isSecureArea', true);

/********** Rollback Schedule */
$scheduleFactory =
    $objectManager->create('\Custobar\CustoConnector\Model\ScheduleFactory');
/** @var $item \Custobar\CustoConnector\Model\Schedule */
foreach ($scheduleFactory->create()->getCollection() as $item) {
    $item->delete();
}

/********** Rollback configs */
/** @var Magento\Store\Model\StoreManager $storeManager */
$storeManager =
    $objectManager->get(\Magento\Store\Model\StoreManagerInterface::class);
/** @var \Magento\Framework\App\Config\Storage\Writer $configWriter */
$configWriter = $objectManager->get(
    '\Magento\Framework\App\Config\Storage\WriterInterface'
);


foreach ($defaults['config'] as $key => $value) {
    /** @var TYPE_NAME $elems */
    $elements = explode("/", $key);
    $scopeType = '';
    $storeCode = $storeManager->getStore("default")->getId();
    switch ($elements[0]) {
        case "global":
            unset($elements[0]);
            $scopeType = ScopeConfigInterface::SCOPE_TYPE_DEFAULT;
            break;
        case "stores":
            $scopeType = "stores";
            try {
                $storeCode = $storeManager->getStore($elements[1])->getId();
            } catch (Exception $exception) {
                $storeCode = 1;
            }
            unset($elements[1]);
            unset($elements[0]);
            break;
        default:
            unset($elements[0]);
            $scopeType = ScopeConfigInterface::SCOPE_TYPE_DEFAULT;
    }
    $configWriter->delete(
        implode("/", $elements),
        $scopeType,
        $storeCode
    );
}

/********** Rollback Stores and websites */
/** @var \Magento\Store\Model\Website $website */
$website = $objectManager->create(
    'Magento\Store\Model\Website'
);

$website->load(1);
$website->setIsDefault(1);
$website->save();

foreach ($defaults['scope']['website'] as $websiteData) {
    $website = $objectManager->create(
        'Magento\Store\Model\Website'
    );

    $id = $website->load($websiteData['code'], 'code')->getId();
    if ($id) {
        $website->delete();
    }
}

$website = $objectManager->create(
    'Magento\Store\Model\Website'
);
$resource = $website->getResource();
$resource->getConnection()->query('ALTER TABLE ' . $resource->getMainTable() . ' AUTO_INCREMENT=1');


foreach ($defaults['scope']['store'] as $storeData) {
    /** @var \Magento\Store\Model\Store $store */
    $store = $objectManager->create(
        'Magento\Store\Model\Store'
    );

    $id = $store->load($storeData['code'], 'code')->getId();
    if ($id) {
        $store->delete();
    }
}

$store = $objectManager->create(
    'Magento\Store\Model\Store'
);
$resource = $store->getResource();
$resource->getConnection()->query('ALTER TABLE ' . $resource->getMainTable() . ' AUTO_INCREMENT=1');
$resource->getConnection()->query('ALTER TABLE store_group AUTO_INCREMENT=1');


/********** Rollback products */
$productCollection = $objectManager->create(\Magento\Catalog\Model\ResourceModel\Product\Collection::class);
foreach ($productCollection as $product) {
    $product->delete();
}

/********** Rollback categories */
/** @var \Magento\Catalog\Model\ResourceModel\Category\Collection $categories */
$categories = $objectManager->create(\Magento\Catalog\Model\ResourceModel\Category\Collection::class);
$categories->addIdFilter([21, 22, 221, 2211, 222, 3, 30, 31, 32]);
foreach ($categories as $category) {
    $category->delete();
}

/********** Rollback customers */
$customers = $objectManager->create(\Magento\Customer\Model\ResourceModel\Customer\Collection::class);
foreach ($customers as $customer) {
    $customer->delete();
}
$connection = $customers->getResource()->getConnection();
$connection->query('ALTER TABLE customer_address_entity AUTO_INCREMENT=1;');
$connection->query('ALTER TABLE customer_address_entity_datetime AUTO_INCREMENT=1;');
$connection->query('ALTER TABLE customer_address_entity_decimal AUTO_INCREMENT=1;');
$connection->query('ALTER TABLE customer_address_entity_int AUTO_INCREMENT=1;');
$connection->query('ALTER TABLE customer_address_entity_text AUTO_INCREMENT=1;');
$connection->query('ALTER TABLE customer_address_entity_varchar AUTO_INCREMENT=1;');

$registry->unregister('isSecureArea');
$registry->register('isSecureArea', false);
