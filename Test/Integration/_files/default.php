<?php
// Magentos Zend_Config_Yaml wont do, so Spyc.
// \Zend\Config\Reader\Yaml might work also
use Custobar\CustoConnector\Model\ScheduleFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;

$defaults = Spyc::YAMLLoad(__DIR__ . '/default.yaml');
$objectManager = \Magento\TestFramework\Helper\Bootstrap::getObjectManager();
$errorMessage = [];
$defaultAttributeSet = $objectManager->get(Magento\Eav\Model\Config::class)
    ->getEntityType('catalog_product')
    ->getDefaultAttributeSetId();
/** @var \Magento\Catalog\Api\ProductRepositoryInterface $productRepository */
$productRepository = $objectManager->create(
    \Magento\Catalog\Api\ProductRepositoryInterface::class
);

$categoryLinkRepository = $objectManager->create(
    \Magento\Catalog\Api\CategoryLinkRepositoryInterface::class,
    [
        'productRepository' => $productRepository
    ]
);

/** @var Magento\Catalog\Api\CategoryLinkManagementInterface $linkManagement */
$categoryLinkManagement = $objectManager->create(\Magento\Catalog\Api\CategoryLinkManagementInterface::class);
$reflectionClass = new \ReflectionClass(get_class($categoryLinkManagement));
$properties = [
    'productRepository' => $productRepository,
    'categoryLinkRepository' => $categoryLinkRepository
];
foreach ($properties as $key => $value) {
    if ($reflectionClass->hasProperty($key)) {
        $reflectionProperty = $reflectionClass->getProperty($key);
        $reflectionProperty->setAccessible(true);
        $reflectionProperty->setValue($categoryLinkManagement, $value);
    }
}
/** @var ScheduleFactory $scheduleFactory */
$scheduleFactory =
    $objectManager->create('\Custobar\CustoConnector\Model\ScheduleFactory');
$mapping = [
    'website' => \Magento\Store\Model\Website::class,
    'group' => \Magento\Store\Model\Group::class,
    'store' => \Magento\Store\Model\Store::class,
];
/*
 *  ----------------- SET WEBSITE ----------------------
 */
/** @var $website \Magento\Store\Model\Website */
$website = \Magento\TestFramework\Helper\Bootstrap::getObjectManager()->create(
    'Magento\Store\Model\Website'
);

$website->load(1);
$website->setIsDefault(0);
$website->save();
/** @var Magento\Store\Model\StoreManager $storeManager */
$storeManager =
    $objectManager->get(\Magento\Store\Model\StoreManagerInterface::class);
foreach ($defaults['scope'] as $key => $table) {
    foreach ($table as $row) {
        $model = $objectManager->create($mapping[$key]);
        $model->isObjectNew(true);
        $model->addData($row);
        $model->save($model);
    }
}
$storeManager->reinitStores();


/** @var \Magento\Framework\App\Helper\Context $context */
$context = $objectManager->get(\Magento\Framework\App\Helper\Context::class);
$config = $context->getScopeConfig();
/** @var \Magento\Framework\App\Config\Storage\Writer $configWriter */
$configWriter = $objectManager->get(
    '\Magento\Framework\App\Config\Storage\WriterInterface'
);
/*
 * ------------- PROCESS CONFIGS -----------------
 *
 * "default/custobar/custobar_custoconnector/models"
 * stores / books_eng / custobar / custobar_custoconnector / tracking_script
 * global/models / catalog / rewrite / product: Custobar_CustoConnector_Test_Model_TestProductModel
 *    default/custobar / custobar_custoconnector / models: |
 */
foreach ($defaults['config'] as $key => $value) {
    /** @var TYPE_NAME $elems */
    $elements = explode("/", $key);
    $scopeType = '';
    $storeCode = $storeManager->getStore("default")->getId();
    switch ($elements[0]) {
        case "global":
            unset($elements[0]);
            $scopeType = ScopeConfigInterface::SCOPE_TYPE_DEFAULT;
            break;
        case "stores":
            $scopeType = "stores";
            try {
                $storeCode = $storeManager->getStore($elements[1])->getId();
            } catch (Exception $exception) {
                $storeCode = 1;
            }
            unset($elements[1]);
            unset($elements[0]);
            break;
        default:
            unset($elements[0]);
            $scopeType = ScopeConfigInterface::SCOPE_TYPE_DEFAULT;
    }
    $configWriter->save(
        implode("/", $elements),
        $value,
        $scopeType,
        $storeCode
    );
}

/*
 *  ----------- PROCESS MODELS ----------------
 */
$eav_mapping = [
    'customer' => \Magento\Customer\Model\Customer::class,
    'customer_address' => \Magento\Customer\Model\Address::class,
    'catalog_category' => \Magento\Catalog\Model\Category::class,
    'catalog_product' => \Magento\Catalog\Model\Product::class
];
foreach (array_keys($eav_mapping) as $eav) {
    foreach ($defaults['eav'][$eav] as $row) {
        $model = $objectManager->create($eav_mapping[$eav]);
        if ($eav == 'customer') {
            $model->isObjectNew(true);
            $model->setId($row['entity_id'])
                ->setWebsiteId($row['website_id'])
                ->setStoreId($row['store_id'])
                ->setCreatedIn($row['created_in'])
                ->setGroupId($row['group_id'])
                ->setFirstname($row['firstname'])
                ->setLastname($row['lastname'])
                ->setEmail($row['email'])
                ->setPasswordHash($row['password_hash'])
                ->setCreatedAt($row['created_at'])
                ->setUpdatedAt($row['updated_at']);
            if (isset($row['default_billing'])) {
                $model->setDefaultBilling($row['default_billing'])
                    ->setDefaultShipping($row['default_shipping']);
            }
            $model->save();
        } elseif ($eav == 'customer_address') {
            $model->setParentId($row['parent_id'])
                ->setFirstname($row['firstname'])
                ->setLastname($row['lastname'])
                ->setCountryId($row['country_id'])
                ->setRegionId($row['region_id'])
                ->setRegion($row['region'])
                ->setPostcode($row['postcode'])
                ->setCity($row['city'])
                ->setTelephone($row['telephone'])
                ->setStreet($row['street'])
                ->setCreatedAt($row['created_at'])
                ->setUpdatedAt($row['updated_at'])
                ->save();
        } elseif ($eav == 'catalog_category') {
            /** @var $category \Magento\Catalog\Model\Category */
            $category =
                $objectManager->create('Magento\Catalog\Model\Category');
            $category->isObjectNew(true);
            $category->setId($row['entity_id'])
                ->setName($row['name'])
                ->setParentId($row['parent_id'])
                ->setPath($row['path'])
                ->setLevel($row['level'])
                ->setAvailableSortBy('name')
                ->setDefaultSortBy('name')
                ->setIsActive(true)
                ->setIsAnchor(true)
                ->setPosition(1)
                ->save();
            if (!empty($row['stores'])) {
                foreach ($row['stores'] as $store_version) {
                    $storeManager->setCurrentStore($store_version['store']);
                    $category = $objectManager->create(
                        'Magento\Catalog\Model\Category'
                    );
                    $category->isObjectNew(true);
                    $category->setId($row['entity_id'])
                        ->setName($store_version['name'])
                        ->setParentId($row['parent_id'])
                        ->setPath($row['path'])
                        ->setLevel($row['level'])
                        ->setAvailableSortBy('name')
                        ->setDefaultSortBy('name')
                        ->setIsActive(true)
                        ->setIsAnchor(true)
                        ->setPosition(1)
                        ->save();
                }
                $storeManager->setCurrentStore(2);
            }
        } elseif ($eav == 'catalog_product') {
            $store_id = !empty($row['store_id']) ? $row['store_id'] : 2;
            $storeManager->setCurrentStore($store_id);
            /** @var $product \Magento\Catalog\Model\Product */
            $product =
                $objectManager->create('Magento\Catalog\Model\Product');
            $product->isObjectNew(true);

            $product->setTypeId(\Magento\Catalog\Model\Product\Type::TYPE_SIMPLE)
                ->setId($row['entity_id'])
                ->setWebsiteIds($row['website_ids'])
                ->setStoreId($store_id)
                ->setName($row['name'])
                ->setSku($row['sku'])
                ->setAttributeSetId($defaultAttributeSet)
                ->setStockData([
                    'use_config_manage_stock' => 0,
                    'manage_stock' => 0,
                    'is_in_stock' => $row['stock']['is_in_stock'],
                    'qty' => $row['stock']['qty']
                ])
                ->setPrice($row['price'])
                ->setWeight(18)
                ->setShortDescription("Short description" . $row['entity_id'])
                ->setMetaTitle('meta title' . $row['entity_id'])
                ->setMetaKeyword('meta keyword' . $row['entity_id'])
                ->setMetaDescription('meta description' . $row['entity_id'])
                ->setVisibility(\Magento\Catalog\Model\Product\Visibility::VISIBILITY_BOTH)
                ->setStatus(\Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED)
                ->setTaxClassId(0)
                ->setDescription($row['description']);

            if (count($row['category_ids'])) {
                $product->setCategoryIds($row['category_ids']);
            }

            $productRepository->save($product);

            $sku = $product->getSku();
            $productId = $product->getId();
            if (!empty($row['store_copies'])) {
                foreach ($row['store_copies'] as $store_to) {
                    /** @var Magento\Catalog\Model\Product $copy */
                    $copy =
                        $objectManager->create(
                            'Magento\Catalog\Model\Product'
                        )
                            ->setStoreId($store_id)
                            ->load($productId);
                    $storeManager->setCurrentStore($store_to['store_id']);
                    $copy->setStoreId($store_to['store_id'])
                        ->setName($store_to['name'])
                        ->setUrlKey(null)
                        ->setDescription($store_to['description'])
                        ->setShortDescription(
                            $store_to['short_description']
                        )
                        ->setCopyFromView(true);
                    $productRepository->save($copy);
                    $storeManager->setCurrentStore($store_id);
                }
            }
        }
    }
}

/* Refresh stores memory cache */
$storeManager->reinitStores();

/** @var $item \Custobar\CustoConnector\Model\Schedule */
foreach ($scheduleFactory->create()->getCollection() as $item) {
    $item->delete();
}
foreach ($defaults['tables']['custobar_schedule'] as $data) {
    unset($data['id']);
    $item = $scheduleFactory->create(['data' => $data]);
    $item->isObjectNew(true);
    $item->setDataChanges(true)
        ->save();
}
