<?php
// @codingStandardsIgnoreFile

namespace Custobar\CustoConnector\Test\Integration\Helper;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\TestFramework\Helper\Bootstrap;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;

/**
 * Class ConfigurableTest
 *
 * @magentoAppIsolation enabled
 * @magentoDataFixture Magento/ConfigurableProduct/_files/product_configurable.php
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class ConfigurableTest extends \Custobar\CustoConnector\Test\Integration\CustoTestCase
{

    /** @var \Magento\Framework\App\ObjectManager */
    protected $_objectManager;

    /**
     * Object under test
     *
     * @var Configurable
     */
    private $model;

    /**
     * @var Product
     */
    private $product;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /** @var \Custobar\CustoConnector\Helper\Data */
    private $helper;

    protected function setUp()
    {
        $this->_objectManager = Bootstrap::getObjectManager();
        $this->productRepository = $this->_objectManager
            ->create(ProductRepositoryInterface::class);

        $this->product = $this->productRepository->get('configurable');

        $this->model = $this->_objectManager
            ->create(Configurable::class);

        // prevent fatal errors by assigning proper "singleton" of type instance to the product
        $this->product->setTypeInstance($this->model);

        /** @var \Custobar\CustoConnector\Helper\Data $helper */
        $this->helper = $this->_objectManager->get(
            '\Custobar\CustoConnector\Helper\Data'
        );
    }

    /**
     * @test
     * @group m2
     */
    public function testCanGetSku2ExternalIdOnConfigurableProduct()
    {
        $objectManager = Bootstrap::getObjectManager();
        $this->helper = $objectManager->get(\Custobar\CustoConnector\Helper\Data::class);
        $productRepository = $objectManager->create(ProductRepositoryInterface::class);
        $product = $productRepository->get('configurable', true);

        $mappedProduct = $this->helper->getMappedEntity($product);

        $this->assertEquals(
            "configurable",
            $mappedProduct->getData("external_id"),
            "Product external_id not mapped on configurable product"
        );
    }
}
