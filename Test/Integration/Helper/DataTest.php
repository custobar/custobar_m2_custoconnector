<?php
/**
 * Class Custobar\CustoConnector\Test\Integration\Helper\DataTest
 *
 * @group Custobar_Controller
 */

namespace Custobar\CustoConnector\Test\Integration\Helper;

use \Custobar\CustoConnector\Helper\Data as Helper;
use Custobar\CustoConnector\Helper\Data;
use \Custobar\CustoConnector\Model\Initial;
use Custobar\CustoConnector\Model\ResourceModel\Schedule\Collection;
use Custobar\CustoConnector\Model\Schedule;
use Custobar\CustoConnector\Test\Integration\TestTrait;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product\Visibility;
use \Magento\Customer\Model\CustomerFactory;
use \Magento\Framework\App\MutableScopeConfig;
use \Magento\Framework\App\Helper\Context;
use \Magento\Framework\HTTP\ZendClientFactory;
use Magento\Store\Model\ScopeInterface;
use \Magento\TestFramework\Store\StoreManager;
use \Magento\TestFramework\TestCase\AbstractBackendController;
use \Magento\TestFramework\Helper\Bootstrap;
use \Magento\Catalog\Model\ProductFactory;
use \Magento\Newsletter\Model\SubscriberFactory;
use \Magento\Newsletter\Model\Subscriber;
use \Magento\Sales\Model\Order;
use \Magento\Framework\Stdlib\DateTime\Timezone;
use Psr\Log\LogLevel;


/**
 * @group Custobar_Helper
 */
class DataTest extends AbstractBackendController
{
    use TestTrait;

    protected $realHttpClient;

    /** @var \Custobar\CustoConnector\Helper\Data */
    private $helper;
    /** @var \Magento\Framework\App\ObjectManager */
    protected $objectManager;

    /**
     * @param null $content
     * @param null $body
     * @param int  $status
     *
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    private function getMockZendClient(
        $content = null,
        $body = null,
        $status = 200
    ) {
        $mockResponse = $this->getMockBuilder(
            \Zend_Http_Response::class
        )->disableOriginalConstructor()->getMock();
        $mockResponse->expects($this->any())
            ->method('getBody')
            ->will($this->returnValue($body));
        $mockResponse->expects($this->any())
            ->method('getStatus')
            ->will($this->returnValue($status));

        $mockClient = $this->getMockBuilder(
            \Magento\Framework\HTTP\ZendClient::class
        )->getMock();
        $mockClient->expects($this->any())
            ->method('request')
            ->will($this->returnValue($mockResponse));
        $mockClient->expects($this->any())
            ->method('setUri')
            ->will($this->returnSelf());
        $mockClient->expects($this->any())
            ->method('setConfig')
            ->will($this->returnSelf());
        $mockClient->expects($this->any())
            ->method('setHeaders')
            ->will($this->returnSelf());
        $mockClient->expects($this->any())
            ->method('setRawData')
            ->will($this->returnSelf());
        $mockClient->expects($this->any())
            ->method('setMethod')
            ->will($this->returnSelf());
        $mockClient->expects($this->any())
            ->method('setAuth')
            ->will($this->returnSelf());

        return $mockClient;
    }

    protected function setUp()
    {
        parent::setUp();

        $this->objectManager = $this->_objectManager;

        /** @var \Custobar\CustoConnector\Helper\Data $helper */
        $this->helper = $this->_objectManager->get(
            '\Custobar\CustoConnector\Helper\Data'
        );

        $body = "{\"response\": \"ok\", \"id\": \"201611111148539022881736\"}";
        $this->helper->setClient($this->getMockZendClient($body, $body));
    }

    public static function loadFixtureDefault()
    {
        include __DIR__ . '/../_files/default.php';
    }

    public static function loadFixtureSalesTable()
    {
        /** @noinspection PhpUnusedLocalVariableInspection */
        $file = __DIR__ . '/_files/orders.yaml';
        /** @noinspection PhpUnusedLocalVariableInspection */
        $tables = ['sales_order', 'sales_order_item'];
        include __DIR__ . '/../_files/tables.php';
    }

    public static function loadFixtureCleaningTable()
    {
        /** @noinspection PhpUnusedLocalVariableInspection */
        $file = __DIR__ . '/_files/testCleaning.yaml';
        /** @noinspection PhpUnusedLocalVariableInspection */
        $tables = ['custobar_schedule'];
        include __DIR__ . '/../_files/tables.php';
    }

    public static function loadFixtureErrorsTable()
    {
        /** @noinspection PhpUnusedLocalVariableInspection */
        $file = __DIR__ . '/_files/errors.yaml';
        /** @noinspection PhpUnusedLocalVariableInspection */
        $tables = ['custobar_schedule'];
        include __DIR__ . '/../_files/tables.php';
    }

    /**
     * @test
     * @magentoDbIsolation  enabled
     * @magentoAppIsolation enabled
     * @magentoDataFixture  loadFixtureDefault
     * @group               m2
     */
    public function testScheduleFilters()
    {
        $this->alterDefaults();

        /** @var Collection $schedules */
        $schedules = $this->getAllSchedules();
        $schedules->setOrder("entity_id", "ASC")
            ->addOnlyForSendingFilter();

        /** @noinspection PhpUnnecessaryFullyQualifiedNameInspection */
        $this->assertEquals(
            Collection::class,
            get_class($schedules)
        );

        /** @var Schedule $first */
        $first = $schedules->getFirstItem();

        $this->assertEquals(1, $first->getEntityId(), "is id right");
        $this->assertEquals(
            'Magento\Customer\Model\Customer',
            $first->getEntityType(),
            "is type right"
        );

        $this->assertEquals(
            5,
            $first->getStoreId(),
            "is the store right"
        );

        $this->restoreDefaults();
    }

    /**
     * @test
     * @magentoDataFixture  loadFixtureDefault
     * @magentoDataFixture  loadFixtureSalesTable
     * @group               m2
     */
    public function testSchemaDuplicateKeyRestriction()
    {
        $this->alterDefaults();

        $this->assertFalse(
            $this->helper->scheduleUpdate(
                1,
                5,
                'Magento\Customer\Model\Customer'
            ),
            "Testing that cant add the same again that has been added from the fixture already"
        );
    }

    /**
     * @test
     * @magentoDataFixture  loadFixtureDefault
     * @magentoDataFixture  loadFixtureSalesTable
     * @group               m2
     */
    public function testAddSchedules()
    {
        $this->alterDefaults();

        $this->assertNotNull($this->helper);

        $productSchedule = $this->helper->scheduleUpdate(
            1,
            2,
            'Magento\Catalog\Model\Product'
        );

        $this->assertFalse(
            $productSchedule->isEmpty(),
            "Testing did add a product schedule"
        );

        /** @var Collection $schedules */
        $schedules = $this->getSchedule()->getCollection();
        $schedules->addOnlyForSendingFilter();

        $data = $schedules->getData();

        $this->assertEquals(1, $data[0]["entity_id"]);
        $this->assertEquals(
            'Magento\Customer\Model\Customer',
            $data[0]["entity_type"],
            "Test that customer model is the first in the schedules"
        );

        $this->assertEquals(
            2,
            $data[3]["store_id"],
            "Test that there is a store id present in the data"
        );

        // stored state count
        $this->assertEquals(
            4,
            $schedules->count(),
            "Test that there are only 4 scheduled items"
        );

        $this->restoreDefaults();
    }

    /**
     * @test
     * @magentoDataFixture  loadFixtureDefault
     * @group               m2
     */
    public function testApiClient()
    {
        $this->alterDefaults();

        /** @var Collection $schedules */
        $schedules = $this->getSchedule()->getCollection();
        $schedules->setOrder("entity_id", "ASC")
            ->addOnlyForSendingFilter();

        /** @var Schedule $schedule */
        $schedule = $schedules->getFirstItem();
        $entity = $this->helper->getEntityFromSchedule($schedule);
        $this->assertInstanceOf(
            \Magento\Customer\Model\Customer::class,
            $entity,
            'test that the entity is Magento\Customer\Model\Customer'
        );

        $custobarJson1 = $this->helper->getCustobarJsonForEntity(
            $entity
        );

        $custobarJson2 = $this->helper->getCustobarJsonForEntity(
            [$entity]
        );

        $this->assertEquals($custobarJson1, $custobarJson2);

        $this->Config()->setValue(
            "custobar/custobar_custoconnector/mode",
            1,
            "stores",
            "default"
        );

        $url = $this->helper->getApiBaseUri() . $this->helper->getUrlSuffix(
                $entity
            );

        $this->assertEquals(
            "https://dev.custobar.com/api/customers/upload/",
            $url,
            "test that the dev url matches"
        );

        $this->Config()->setValue(
            "custobar/custobar_custoconnector/prefix",
            "dev",
            "stores",
            "default"
        );
        // Mock JSON
        $json = <<< JSON
{
  "timestamp": 1385150462,
  "stuff": {
    "this": 2,
    "that": 4,
    "other": 1
    }
}
JSON;
        $this->helper->setClient($this->getMockZendClient($json));
        /** @var \Magento\Framework\HTTP\ZendClient $client */
        $client = $this->helper->getApiClient($url, $custobarJson1);
        // Smoke test from variable getters using mock
        // less hazle than local stub controller
        // we should unit test more like above
        $this->assertNotEmpty($client->request());

        $this->restoreDefaults();
    }

    /**
     * @test
     * @magentoDbIsolation  enabled
     * @magentoAppIsolation enabled
     * @magentoDataFixture  Magento/Catalog/_files/product_simple.php
     * @magentoDataFixture  loadFixtureDefault
     * @group               m2
     */
    public function testSettings()
    {
        $objectManager = Bootstrap::getObjectManager();

        /** @var \Magento\Catalog\Model\Product $product */
        $product =
            $objectManager->create(ProductFactory::class)
                ->create()
                ->load(1);

        $can_track = $this->helper->getEvenObjectTrackedModel(
            $product
        );
        $this->assertNotFalse(
            $can_track,
            "Test can we track product models"
        );

        $this->assertNotFalse($this->helper->getApiKey());

        // test the api connection settings
        $this->assertEquals(
            "https://developer.custobar.com/api/",
            $this->helper->getApiBaseUri(),
            "Test do we get the live api base uri"
        );


        $this->Config()->setValue(
            "custobar/custobar_custoconnector/mode",
            1,
            "stores",
            "default"
        );

        $this->assertEquals(
            "https://dev.custobar.com/api/",
            $this->helper->getApiBaseUri(),
            "Test the dev api base uri"
        );

        $this->setConfigValue(
            "custobar/custobar_custoconnector/tracking_script",
            <<<text
        (function(c,u,s,t,o,b,a,r) {var e;c[o]=[];c[b] = a;e =
u.createElement(s);r=u.getElementsByTagName(s)[0];e.async=1;e.src =
t;r.parentNode.insertBefore(e, r);})(window, document, 'script',
'https://www.custobar.com/js/v1/custobar.js', 'cstbr', 'cstbrConfig',
{'_companyToken':'ABCD1234','_pages':[['Login','^login/'],['Register','^register/']],'_banners':[['Sidebanner','#sidebar']]});
text
        );

        $this->Config()->setValue(
            "custobar/custobar_custoconnector/tracking_script",
            <<<text
        (function(c,u,s,t,o,b,a,r) {var e;c[o]=[];c[b] = a;e =
u.createElement(s);r=u.getElementsByTagName(s)[0];e.async=1;e.src =
t;r.parentNode.insertBefore(e, r);})(window, document, 'script',
'https://www.custobar.com/js/v1/custobar.js', 'cstbr', 'cstbrConfig',
{'_companyToken':'ABCD1234','_pages':[['Login','^login/'],['Register','^register/']],'_banners':[['Sidebanner','#sidebar']]});
text
            ,
            "stores",
            "default"
        );

        $this->assertContains(
            "'https://www.custobar.com/js/v1/custobar.js', 'cstbr', 'cstbrConfig'",
            $this->helper->getTrackingScript(),
            'can get the tracking data'
        );

        $this->assertTrue(
            $this->helper->getIsWebsiteAllowed(3),
            'Should be true'
        );
        $this->assertFalse(
            $this->helper->getIsWebsiteAllowed(8),
            'Should be false'
        );

        $this->setConfigValue(
            "custobar/custobar_custoconnector/allowed_websites",
            '1,2,4,7,9'
        );


        $this->Config()->setValue(
            "custobar/custobar_custoconnector/allowed_websites",
            '1,2,4,7,9',
            "stores",
            "default"
        );

        $this->assertFalse($this->helper->getIsWebsiteAllowed(3));
    }

    /**
     * @test
     * @group m2
     */
    public function testUrlGetterApiClient()
    {
        $this->helper->setClient(null);

        $this->Config()->setValue(
            "custobar/custobar_custoconnector/mode",
            0,
            "stores",
            "default"
        );

        $this->Config()->setValue(
            "custobar/custobar_custoconnector/mode",
            "dev",
            "stores",
            "default"
        );

        /** @var \Magento\Framework\HTTP\ZendClient $client */
        $client = $this->helper->getApiClient(
            $this->helper->getApiBaseUri(),
            []
        );

        $this->assertEquals(
            "https://dev.custobar.com:443/api/",
            $client->getUri(true)
        );
    }

    /**
     * @test
     * @magentoDataFixture  loadFixtureDefault
     * @magentoDataFixture  loadFixtureSalesTable
     * @group               m2
     */
    public function testFailingApiUrl()
    {
        $this->alterDefaults();

        $this->helper->setClient($this->getMockZendClient());
        $this->setConfigValue(
            "custobar/custobar_custoconnector/prefix",
            "prefixthatdoesntexists"
        );
        $this->setConfigValue(
            "custobar/custobar_custoconnector/apikey",
            "prefixthatdoesntexists"
        );
        $this->setConfigValue(
            "custobar/custobar_custoconnector/allowed_websites",
            "2,3"
        );

        /** @var Collection $collection */
        $collection = $this->getAllSchedules();
        $this->assertCount(3, $collection->getData());

        /** @var Schedule $item */
        $item = $collection->getFirstItem();
        $entity = $this->helper->getEntityFromSchedule($item);
        /** @noinspection PhpUndefinedMethodInspection */
        $this->setConfigValue(
            Helper::$CONFIG_ALLOWED_WEBSITES,
            (string)$entity->getWebsiteId()
        );

        $this->helper->sendUpdateRequests();
        sleep(1);
        $collection->resetData()->clear();
        $data = $collection->getData();
        $this->assertCount(
            6,
            $data,
            "Do we have new rows for same items"
        );

        // we need to sleep to simulate that the cron run per "minute"
        // so we don`t get a constrain on adding the same entity with the same processed time
        sleep(1);

        $this->helper->sendUpdateRequests();
        sleep(1);
        $this->helper->sendUpdateRequests();
        sleep(1);
        $this->helper->sendUpdateRequests();

        $collection->resetData()->clear();

        $this->assertCount(
            15,
            $collection->getData(),
            "Do we have new rows for same items"
        );

        $collection->resetData()->clear();
        $collection->addOnlyErroneousFilter();

        $this->assertCount(3, $collection->getData());

        $errorIncrement = 0;

        /** @var Schedule $item */
        foreach ($collection as $item) {
            $errorIncrement += $item->getErrorsCount();
        }

        $this->assertEquals(12, $errorIncrement);

        $this->restoreDefaults();
    }

    /**
     * @test
     * @magentoDataFixture loadFixtureCleaningTable
     * @group              m2
     */
    public function testCleaning()
    {
        /** @var Collection $schedules */
        $schedules = $this->getAllSchedules()
            ->setPageSize(Helper::$ITEMS_PER_CRON)
            ->setCurPage(1);

        $data = $schedules->getData();

        $this->assertCount(4, $data);

        /** @var Schedule $model */
        $model = $this->getSchedule();
        $model->cleanSchedules();

        $schedules->resetData()->clear();

        $data = $schedules->getData();

        $this->assertCount(2, $data);
    }

    /**
     * @test
     * @magentoDataFixture  loadFixtureDefault
     * @group               m2
     */
    public function testMappingStore()
    {
        $this->alterDefaults();

        /** @var \Magento\Framework\Model\AbstractModel $storeViewBooksEng */
        $storeViewBooksEng = $this->getStore(2);
        $mappedStoreBooksEng =
            $this->helper->getMappedEntity($storeViewBooksEng);
        $this->assertEquals(2, $mappedStoreBooksEng->getData("external_id"));

        $this->assertEquals(
            "Books Website, Books Store, English",
            $mappedStoreBooksEng->getData("name")
        );

        /** @var \Magento\Framework\Model\AbstractModel $storeViewBooksGerman */
        $storeViewBooksGerman = $this->getStore(3);
        $mappedStoreBooksGerman =
            $this->helper->getMappedEntity($storeViewBooksGerman);
        $this->assertEquals(
            3,
            $mappedStoreBooksGerman->getData("external_id")
        );
        $this->assertEquals(
            "Books Website, Books Store, German",
            $mappedStoreBooksGerman->getData("name")
        );

        /** @var \Magento\Framework\Model\AbstractModel $storeViewExtensionsEng */
        $storeViewExtensionsEng = $this->getStore(5);
        $mappedStoreExtensionsEng =
            $this->helper->getMappedEntity($storeViewExtensionsEng);
        $this->assertEquals(
            5,
            $mappedStoreExtensionsEng->getData("external_id")
        );

        $this->restoreDefaults();
    }

    /**
     * @test
     * @magentoDataFixture loadFixtureDefault
     * @group              m2
     */
    public function testSendingStoreData()
    {
        $this->alterDefaults();

        $this->clearSchedules();
        $this->helper->scheduleUpdate(
            2,
            null,
            'Magento\Store\Model\Store'
        );

        $schedulesBefore = $this->getAllSchedules();

        $schedulesBefore->addFieldToFilter(
            "entity_type",
            'Magento\Store\Model\Store'
        );

        $schedulesBeforeData = $schedulesBefore->getData();

        $this->assertEquals(
            'Magento\Store\Model\Store',
            $schedulesBeforeData[0]["entity_type"]
        );

        /** @noinspection PhpUndefinedMethodInspection OBSOLETE */
        $this->getStore()
            ->setConfig("custobar/custobar_custoconnector/mode", 1);

        $this->setConfigValue(
            "custobar/custobar_custoconnector/prefix",
            "abc"
        );
        $this->setConfigValue(
            "custobar/custobar_custoconnector/apikey",
            "123"
        );

        $body = "{\"response\": \"ok\", \"id\": \"201611111148539022881736\"}";

        $this->helper->setClient($this->getMockZendClient($body, $body));
        $this->helper->sendUpdateRequests();

        $schedulesAfter = $this->getAllUnProcessedSchedules()->getData();

        $this->assertCount(
            0,
            $schedulesAfter,
            "All schedules have been processed"
        );

        $this->restoreDefaults();
    }

    /**
     * @test
     * @magentoDataFixture loadFixtureDefault
     * @magentoDataFixture loadFixtureNewsLetterTable
     * @group              m2
     */
    public function testMappingNewsLetter()
    {
        $this->alterDefaults();
        $this->clearSchedules();

        $factory = $this->objectManager->get(SubscriberFactory::class);
        /** @var \Magento\Newsletter\Model\Subscriber $newsletterSubscriberJohnDoe */
        $newsletterSubscriberJohnDoe = $factory->create()->load(13);

        $mappedSubscriberJohnDoe =
            $this->helper->getMappedEntity($newsletterSubscriberJohnDoe);

        self::assertEquals("john@doe.com", $mappedSubscriberJohnDoe["email"]);
        self::assertEquals("1", $mappedSubscriberJohnDoe["customer_id"]);
        self::assertEquals("MAIL_SUBSCRIBE", $mappedSubscriberJohnDoe["type"]);
        self::assertEquals("5", $mappedSubscriberJohnDoe["store_id"]);

        $newsletterSubscriberJohnDoe->setStatus(
            Subscriber::STATUS_UNSUBSCRIBED
        );

        $mappedSubscriberJohnDoe =
            $this->helper->getMappedEntity($newsletterSubscriberJohnDoe);

        self::assertEquals(
            "MAIL_UNSUBSCRIBE",
            $mappedSubscriberJohnDoe["type"]
        );

        /** @var \Magento\Newsletter\Model\Subscriber $newsletterSubscriberJoJo */
        $newsletterSubscriberJojo =
            $factory->create()->load(14);

        $mappedSubscriberJoJo =
            $this->helper->getMappedEntity($newsletterSubscriberJojo);

        self::assertEquals("MAIL_UNSUBSCRIBE", $mappedSubscriberJoJo["type"]);
        $this->restoreDefaults();
    }

    /**
     * @test
     * @magentoDataFixture loadFixtureDefault
     * @magentoDataFixture loadFixtureNewsLetterTable
     * @group              m2
     */
    public function testSendingNewsLetterData()
    {
        $this->alterDefaults();

        $this->helper->scheduleUpdate(
            12,
            2,
            'Magento\Newsletter\Model\Subscriber'
        );

        $schedulesBefore = $this->getAllSchedules();

        $schedulesBefore->addFieldToFilter(
            "entity_type",
            'Magento\Newsletter\Model\Subscriber'
        );

        $schedulesBeforeData = $schedulesBefore->getData();

        $this->assertEquals(
            'Magento\Newsletter\Model\Subscriber',
            $schedulesBeforeData[0]["entity_type"]
        );

        /** @noinspection PhpUndefinedMethodInspection OBSOLETE */
        $this->getStore()
            ->setConfig("custobar/custobar_custoconnector/mode", 1);
        $this->setConfigValue(
            "custobar/custobar_custoconnector/prefix",
            "abc"
        );
        $this->setConfigValue(
            "custobar/custobar_custoconnector/apikey",
            "123"
        );

        $this->helper->sendUpdateRequests();

        $schedulesAfter = $this->getAllUnProcessedSchedules()->getData();

        $this->assertCount(
            0,
            $schedulesAfter,
            "All schedules have been processed"
        );

        $this->restoreDefaults();
    }

    /**
     * Get object created at date affected current active store timezone
     * OBSOLETE
     *
     * @param string $createdAt
     *
     * @return \\Zend_Date
     */
    public function getCreatedAtDateAsIsoISO8601($createdAt)
    {
        /** @noinspection PhpUndefinedClassInspection */
        return Mage::app()->getLocale()->date(
            Varien_Date::toTimestamp($createdAt),
            null,
            null,
            true
        )->toString(\Zend_Date::ISO_8601);
    }

    /**
     * @param string $format
     *
     * @return false|string
     */
    public function getFormattedScopeTime($format = "Y-m-d H:i:s")
    {
        $objectManager = Bootstrap::getObjectManager();
        $timeZone = $objectManager->get(Timezone::class);
        return date($format, $timeZone->scopeTimeStamp());
    }

    /**
     * @test
     * @magentoDataFixture loadFixtureDefault
     * @group              m2
     */
    public function testMappingCustomer()
    {
        $this->alterDefaults();

        $this->clearSchedules();

        /** @noinspection PhpUndefinedMethodInspection OBSOLETE */
        $this->getStore()
            ->setConfig("general/locale/timezone", "America/Los_Angeles");

        $factory = $this->objectManager->get(CustomerFactory::class);
        /** @var \Magento\Customer\Model\Customer $john */
        $john = $factory->create()->load(1);
        $customerJohn = $this->helper->getMappedEntity($john);

        $this->assertEquals(1, $customerJohn->getData("external_id"));
        $this->assertEquals("John", $customerJohn->getData("first_name"));
        $this->assertEquals("Doe", $customerJohn->getData("last_name"));
        $this->assertEquals(
            "Address 123",
            $customerJohn->getData("street_address"),
            "is address present"
        );

        $this->assertEquals(
            "US",
            $customerJohn->getData("country"),
            "Is the country id present"
        );

        $this->assertEquals(
            "5",
            $customerJohn->getData("store_id"),
            "Is the store id present"
        );

        $this->assertEquals(
            "89001",
            $customerJohn->getData("zip_code"),
            "Is the zip code present"
        );

        $this->assertEquals(
            "Alamo",
            $customerJohn->getData("city"),
            "Is the city present"
        );

        $this->assertEquals(
            "555-55-55",
            $customerJohn->getData("phone_number"),
            "Is the phone present"
        );

        $this->assertNotNull(
            $customerJohn->getData("date_joined"),
            "Is the date joined present"
        );

        /** @var \Magento\Customer\Model\Customer $jane */
        $jane = $factory->create()->load(2);
        $customerJane = $this->helper->getMappedEntity($jane);

        $this->assertEquals(
            "2",
            $customerJane->getData("store_id"),
            "Is the store id present"
        );

        $this->restoreDefaults();
    }

    /**
     * @test
     * @magentoDataFixture loadFixtureDefault
     * @group              m2
     */
    public function testSendingCustomerData()
    {
        $this->alterDefaults();

        $this->clearSchedules();

        $this->helper->scheduleUpdate(
            1,
            5,
            'Magento\Customer\Model\Customer'
        );

        $schedulesBefore = $this->getAllSchedules();

        $schedulesBefore->addFieldToFilter(
            "entity_type",
            'Magento\Customer\Model\Customer'
        );

        $schedulesBeforeData = $schedulesBefore->getData();

        $this->assertEquals(
            'Magento\Customer\Model\Customer',
            $schedulesBeforeData[0]["entity_type"]
        );

        /** @noinspection PhpUndefinedMethodInspection */
        $this->getStore()
            ->setConfig("custobar/custobar_custoconnector/mode", 1);
        $this->setConfigValue(
            "custobar/custobar_custoconnector/prefix",
            "abc"
        );
        $this->setConfigValue(
            "custobar/custobar_custoconnector/apikey",
            "123"
        );

        $this->helper->sendUpdateRequests();

        $schedulesAfter = $this->getAllUnProcessedSchedules()->getData();

        $this->assertCount(
            0,
            $schedulesAfter,
            "All schedules have been processed"
        );

        $this->restoreDefaults();
    }

    /**
     * @test
     * @magentoDataFixture loadFixtureDefault
     * @magentoDataFixture loadFixtureImageTable
     * @group              m2
     */
    public function testMappingProduct()
    {
        $this->alterDefaults();

        $this->Config()->setValue(
            \Magento\Directory\Helper\Data::XML_PATH_DEFAULT_LOCALE,
            'en_EN',
            ScopeInterface::SCOPE_STORE,
            'books_eng'
        );
        $this->Config()->setValue(
            \Magento\Directory\Helper\Data::XML_PATH_DEFAULT_LOCALE,
            'de_DE',
            ScopeInterface::SCOPE_STORE,
            'books_deu'
        );
        $this->Config()->setValue(
            \Magento\Directory\Helper\Data::XML_PATH_DEFAULT_LOCALE,
            'uk_UA',
            ScopeInterface::SCOPE_STORE,
            'books_ukr'
        );

        $this->clearSchedules();
        $scheduleDefault = $this->helper->scheduleUpdate(
            22201,
            3,
            'Magento\Catalog\Model\Product'
        );

        $scheduleUkr = $this->helper->scheduleUpdate(
            22201,
            4,
            'Magento\Catalog\Model\Product'
        );

        $schedules = $this->getAllSchedules();

        $this->assertCount(
            2,
            $schedules->getData(),
            "Assert that we can have two of the same types int the schedule if the store differs"
        );

        $entity = $this->helper->getEntityFromSchedule($scheduleDefault);

        $mappedProduct = $this->helper->getMappedEntity($entity);

        $this->assertEquals(
            "wells-0002",
            $mappedProduct->getData("external_id"),
            "Product external_id present"
        );

        $this->assertEquals(
            99,
            $mappedProduct->getData("price"),
            "Product price present"
        );

        $this->assertEquals(
            99,
            $mappedProduct->getData("minimal_price"),
            "Product minimal_price present"
        );

        $this->assertEquals(
            "simple",
            $mappedProduct->getData("mage_type"),
            "Product mage_type present"
        );

        $this->assertEquals(
            "Default",
            $mappedProduct->getData("type"),
            "Product type present"
        );

        $this->assertEquals(
            "Zeitreisen",
            $mappedProduct->getData("category"),
            "Product category name from store present."
        );
        $this->assertEquals(
            "222",
            $mappedProduct->getData("category_id"),
            "Product category_id present"
        );
        $this->assertEquals(
            "Wells, H.G. 1898. The Time Machine",
            $mappedProduct->getData("title"),
            "Product title present"
        );
        $this->assertContains(
            "http://localhost/pub/media/catalog/product/m/a/pms005a_4",
            $mappedProduct->getData("image"),
            "Product base image was not found"
        );
        $this->assertContains(
            "wells-h-g-1898-the-time-machine",
            $mappedProduct->getData("url"),
            "Product url not exported as seo friendly product page url"
        );
        $this->assertEquals(
            "0",
            $mappedProduct->getData("sale_price"),
            "Product sale_price present"
        );
        $this->assertEquals(
            "Wells, H.G. 1898. The Time Machine",
            $mappedProduct->getData("description"),
            "Product description present"
        );

        $this->assertEquals(
            3,
            $mappedProduct->getData("store_id"),
            "Product german store id present"
        );

        $this->assertEquals(
            "de",
            $mappedProduct->getData("language"),
            "Product german language present"
        );

        $entityUkr = $this->helper->getEntityFromSchedule($scheduleUkr);
        $mappedProductUkr = $this->helper->getMappedEntity($entityUkr);

        $this->assertEquals(
            "Герберт Джордж Уеллс: Машина часу",
            $mappedProductUkr->getData("title"),
            "Product ukr title present"
        );

        $this->assertEquals(
            "Герберт Джордж Уеллс: Машина часу desc",
            $mappedProductUkr->getData("description"),
            "Product ukr description present"
        );

        $this->assertEquals(
            4,
            $mappedProductUkr->getData("store_id")
        );

        $this->assertEquals(
            "uk",
            $mappedProductUkr->getData("language")
        );

        $this->assertContains(
            "gerbert-dzhordzh-uells-mashina-chasu",
            $mappedProductUkr->getData("url"),
            "Ukr Product url not exported as seo friendly product page url"
        );

        $this->assertEquals(
            "Подорожі у часі",
            $mappedProductUkr->getData("category"),
            "Product ukr category present"
        );

        $this->restoreDefaults();
    }

    /**
     * @test
     * @magentoDbIsolation disabled
     * @magentoDataFixture Magento/ConfigurableProduct/_files/product_configurable.php
     * @group              m2
     */
    public function testMappingConfigurableProduct()
    {
        $this->assertTrue(true);

        $scheduleDefault = $this->helper->scheduleUpdate(
            1,
            1,
            'Magento\Catalog\Model\Product'
        );

        $entity = $this->helper->getEntityFromSchedule($scheduleDefault);

        $mappedProduct = $this->helper->getMappedEntity($entity);

        $this->assertEquals(
            "configurable",
            $mappedProduct->getData("external_id"),
            "Product external_id present"
        );

        $this->assertEquals(
            "configurable",
            $mappedProduct->getData("mage_type"),
            "Product mage_type present"
        );

        $this->assertEquals(
            "Default",
            $mappedProduct->getData("type"),
            "Product type present"
        );

        $this->assertEquals(
            0,
            $mappedProduct->getData("price"),
            "Configurable product price is 0"
        );

        $this->assertEquals(
            "1000",
            $mappedProduct->getData("minimal_price"),
            "Lowest configurable price fail in mapping"
        );

        $this->assertEquals(
            "simple_10,simple_20",
            $mappedProduct->getData("mage_child_ids"),
            "Product child items not present"
        );

        $childProductEvent = $this->helper->scheduleUpdate(
            10,
            1,
            'Magento\Catalog\Model\Product'
        );

        $mappedChildProduct = $this->helper->getMappedEntity(
            $this->helper->getEntityFromSchedule($childProductEvent)
        );

        $this->assertEquals(
            "configurable",
            $mappedChildProduct->getData("mage_parent_ids"),
            "Product parent items not present"
        );

        $this->clearSchedules();
        $this->cleanInitials();
    }

    /**
     * @test
     * @magentoDataFixture Magento/Bundle/_files/product.php
     * @group              m2
     */
    public function testMappingBundleProduct()
    {

        $bundleProductSchedule = $this->helper->scheduleUpdate(
            3,
            1,
            'Magento\Catalog\Model\Product'
        );

        $bundleProduct =
            $this->helper->getEntityFromSchedule($bundleProductSchedule);

        $mappedBundleProduct = $this->helper->getMappedEntity(
            $bundleProduct
        );

        $this->assertEquals(
            "simple",
            $mappedBundleProduct->getData("mage_child_ids"),
            "Bundle child ids not present"
        );

        $this->assertArrayNotHasKey(
            "mage_parent_ids",
            $mappedBundleProduct->getData(),
            "child ids for bundle cannot be present as its it self"
        );
        $this->assertEquals(
            "simple",
            $mappedBundleProduct->getData("mage_child_ids"),
            "Bundle child ids not present"
        );

//          CANNOT TEST CHILDS PARENT IDES AS THE TESTS IS ALWAYS NULL.
//          Integration test probably has some issues
//
//        $childProductEvent = $this->helper->scheduleUpdate(
//            1,
//            1,
//            'Magento\Catalog\Model\Product'
//        );
//
//        $mappedChildProduct = $this->helper->getMappedEntity(
//            $this->helper->getEntityFromSchedule($childProductEvent)
//        );
//
//        $this->assertEquals(
//            "bundle-product",
//            $mappedChildProduct->getData("mage_parent_ids"),
//            "Product parent items not present"
//        );

        $this->clearSchedules();
        $this->cleanInitials();
    }

    /**
     * @test
     * @magentoAppIsolation enabled
     * @magentoDataFixture Magento/GroupedProduct/_files/product_grouped.php
     * @group              m2
     */
    public function testMappingGroupedProduct()
    {

        $objectManager =
            \Magento\TestFramework\Helper\Bootstrap::getObjectManager();
        /** @var ProductRepositoryInterface $productRepository */
        $productRepository = $objectManager->get(
            'Magento\Catalog\Api\ProductRepositoryInterface'
        );
        $groupedProduct = $productRepository->get("grouped-product");
        $groupedProductSchedule = $this->helper->scheduleUpdate(
            $groupedProduct->getId(),
            1,
            'Magento\Catalog\Model\Product'
        );

        $groupedProduct =
            $this->helper->getEntityFromSchedule($groupedProductSchedule);

        $mappedGroupedProduct = $this->helper->getMappedEntity(
            $groupedProduct
        );

        $this->assertEquals(
            "simple,virtual-product",
            $mappedGroupedProduct->getData("mage_child_ids"),
            "Grouped child ids not present"
        );

        $childProductEvent = $this->helper->scheduleUpdate(
            1,
            1,
            'Magento\Catalog\Model\Product'
        );

        $mappedChildProduct = $this->helper->getMappedEntity(
            $this->helper->getEntityFromSchedule($childProductEvent)
        );

        $this->assertEquals(
            "grouped-product",
            $mappedChildProduct->getData("mage_parent_ids"),
            "Product parent items not present"
        );

        $this->clearSchedules();
        $this->cleanInitials();
    }

    /**
     * @test
     * @magentoConfigFixture default_store custobar/custobar_custoconnector/models Magento\Catalog\Model\Product>products:name>title;sku>external_id;custobar_minimal_price>minimal_price;custobar_price>price;type_id>mage_type;configurable_min_price>my_configurable_min_price;custobar_attribute_set_name>type;custobar_category>category;custobar_category_id>category_id;custobar_image>image;custobar_product_url>url;custobar_special_price>sale_price;description>description;custobar_language>language;custobar_store_id>store_id;custobar_child_ids>mage_child_ids;custobar_parent_ids>mage_parent_ids;multiselect_attribute>brand
     * @magentoDataFixture Magento/Catalog/_files/products_with_multiselect_attribute.php
     * @group              m2
     */
    public function testMappingMultiselectAttributeProduct()
    {
        $objectManager = Bootstrap::getObjectManager();

        $trackedModels = array_keys($this->helper->getConfigTrackedModels());
        $this->assertCount(2, $trackedModels, "Tracking only two models");

        /** @var \Magento\Catalog\Model\Product $product */
        $product =
            $objectManager->create(ProductFactory::class)
                ->create()
                ->loadByAttribute('sku', 'simple_ms_2');

        $dropdownProductSchedule = $this->helper->scheduleUpdate(
            $product->getId(),
            1,
            'Magento\Catalog\Model\Product'
        );

        $productWithDropdownData =
            $this->helper->getEntityFromSchedule($dropdownProductSchedule);


        $mappedProduct = $this->helper->getMappedEntity(
            $productWithDropdownData
        );

        $this->assertEquals(
            "simple_ms_2",
            $mappedProduct->getData("external_id"),
            "Product external_id present"
        );

        $this->assertEquals(
            "Option 2, Option 3, Option 4 \"!@#$%^&*",
            $mappedProduct->getData("brand"),
            "Product attribute present"
        );

        $this->clearSchedules();
        $this->cleanInitials();
    }


    /**
     * @test
     * @magentoConfigFixture default_store custobar/custobar_custoconnector/models Magento\Catalog\Model\Product>products:name>title;sku>external_id;custobar_minimal_price>minimal_price;custobar_price>price;type_id>mage_type;configurable_min_price>my_configurable_min_price;custobar_attribute_set_name>type;custobar_category>category;custobar_category_id>category_id;custobar_image>image;custobar_product_url>url;custobar_special_price>sale_price;description>description;custobar_language>language;custobar_store_id>store_id;custobar_child_ids>mage_child_ids;custobar_parent_ids>mage_parent_ids;select_attribute>brand
     * @magentoDataFixture loadSelectFixture
     * @group              m2
     */
    public function testMappingDropdownAttributeProduct()
    {
        $objectManager = Bootstrap::getObjectManager();

        $trackedModels = array_keys($this->helper->getConfigTrackedModels());
        $this->assertCount(2, $trackedModels, "Tracking only two models");

        /** @var \Magento\Catalog\Model\Product $product */
        $product =
            $objectManager->create(ProductFactory::class)
                ->create()
                ->loadByAttribute('sku', 'simple_s_1');

        $dropdownProductSchedule = $this->helper->scheduleUpdate(
            $product->getId(),
            1,
            'Magento\Catalog\Model\Product'
        );

        $productWithDropdownData =
            $this->helper->getEntityFromSchedule($dropdownProductSchedule);


        $mappedProduct = $this->helper->getMappedEntity(
            $productWithDropdownData
        );

        $this->assertEquals(
            "simple_s_1",
            $mappedProduct->getData("external_id"),
            "Product external_id present"
        );

        $this->assertEquals(
            "Fixture Option",
            $mappedProduct->getData("brand"),
            "Product attribute present"
        );

        $this->clearSchedules();
        $this->cleanInitials();
    }

    /**
     * @test
     * @magentoAppIsolation enabled
     * @magentoDataFixture  Magento/Catalog/_files/product_simple.php
     * @group               m2
     */
    public function testMappingNoLinkProduct()
    {
        $objectManager = Bootstrap::getObjectManager();

        /** @var \Magento\Catalog\Model\Product $product */
        $product =
            $objectManager->create(ProductFactory::class)
                ->create()
                ->loadByAttribute('sku', 'simple');


        $schedule = $this->helper->scheduleUpdate(
            $product->getId(),
            1,
            'Magento\Catalog\Model\Product'
        );

        $entity = $this->helper->getEntityFromSchedule(
            $schedule
        );

        $mappedProductWithLink = $this->helper->getMappedEntity($entity);

        $this->assertContains(
            "simple-product",
            $mappedProductWithLink->getData("url"),
            "Product url present"
        );

        $product->setStoreId(0)->setVisibility(
            Visibility::VISIBILITY_NOT_VISIBLE
        );
        $product->save();

        $entity = $this->helper->getEntityFromSchedule(
            $schedule
        );

        $this->assertEquals(1, $product->getVisibility());
        $this->assertEquals(1, $entity->getVisibility());

        $mappedProductWithNoLink = $this->helper->getMappedEntity($entity);

        $this->assertEquals(
            "simple",
            $mappedProductWithNoLink->getData("external_id"),
            "Sku present in the external_id"
        );
        $this->assertArrayNotHasKey(
            "url",
            $mappedProductWithNoLink->getData(),
            "Product not preset present"
        );

        $this->clearSchedules();
        $this->cleanInitials();
    }


    /**
     * @test
     * @magentoDataFixture loadFixtureDefault
     * @group              m2
     */
    public function testSendingProductData()
    {
        $this->alterDefaults();
        $this->clearSchedules();

        /** @noinspection PhpUndefinedMethodInspection */
        $this->getStore()
            ->setConfig("custobar/custobar_custoconnector/mode", 1);
        $this->setConfigValue(
            "custobar/custobar_custoconnector/prefix",
            "abc"
        );
        $this->setConfigValue(
            "custobar/custobar_custoconnector/apikey",
            "123"
        );

        $this->helper->scheduleUpdate(
            22201,
            3,
            'Magento\Catalog\Model\Product'
        );

        $this->helper->scheduleUpdate(
            22201,
            4,
            'Magento\Catalog\Model\Product'
        );
        $schedulesBefore = $this->getAllSchedules()->getData();

        $this->assertCount(
            2,
            $schedulesBefore,
            "Schedule fixtures clear and add 2 result count mismatch"
        );

        $this->helper->sendUpdateRequests();

        $schedulesAfter = $this->getAllUnProcessedSchedules()->getData();

        $this->assertCount(
            0,
            $schedulesAfter,
            "All schedules have been processed"
        );
        $this->restoreDefaults();
    }

    /**
     * @magentoDataFixture Magento/Sales/_files/order.php
     * @group              m2
     */
    public function testMappingOrderDefault()
    {
        /** @var $objectManager \Magento\TestFramework\ObjectManager */
        $objectManager = Bootstrap::getObjectManager();

        /** @var $order \Magento\Sales\Model\Order */
        $order = $objectManager->create(Order::class);
        $order->loadByIncrementId('100000001');

        $mappedOrder = $this->helper->getMappedEntity($order);
        $item = $mappedOrder->getData('magento__items')[0];

        $this->assertEquals(
            100000001,
            $item['sale_external_id']
        );
        $this->assertEquals(
            null,
            $item['product_id']
        );
        $this->assertEquals(
            0,
            $item['unit_price']
        );
        $this->assertEquals(
            "2.0000",
            $item['quantity']
        );
        $this->assertEquals(
            0,
            $item['discount']
        );
        $this->assertEquals(
            0,
            $item['total']
        );
    }

    public static function loadSelectFixture()
    {
        include __DIR__
            . '/_files/product_with_select_attribute.php';
    }


    public static function loadOrderFixture()
    {
        include __DIR__
            . '/_files/two_orders_for_two_diff_customers_custobar.php';
    }

    /**
     * @test
     * @magentoDataFixture loadOrderFixture
     * @group              m2
     */
    public function testOrderCustobarJson()
    {
        /** @var $objectManager \Magento\TestFramework\ObjectManager */
        $objectManager = Bootstrap::getObjectManager();

        /** @var $order1 \Magento\Sales\Model\Order */
        $order1 = $objectManager->create(Order::class);
        $order1->loadByIncrementId('100000001');
        /** @var $order2 \Magento\Sales\Model\Order */
        $order2 = $objectManager->create(Order::class);
        $order2->loadByIncrementId('100000002');

        $json = $this->helper->getCustobarJsonForEntity(
            [
                $order1,
                $order2
            ]
        );

        $data = json_decode($json, true);
        $this->assertCount(
            3,
            $data["sales"],
            "Mapped fixture data encode decode matches 2 order lines containing a product and one line for a additional product"
        );

        $this->assertEquals(100000001, $data["sales"][0]["sale_external_id"]);
        $this->assertEquals(
            "customer@null.com",
            $data["sales"][0]["sale_email"]
        );
        $this->assertEquals(100000002, $data["sales"][1]["sale_external_id"]);
        $this->assertEquals(
            "customer2@null.com",
            $data["sales"][1]["sale_email"]
        );
        $this->assertEquals(100000002, $data["sales"][2]["sale_external_id"]);
        $this->assertEquals(
            "2.000",
            $data["sales"][2]["quantity"]
        );
    }

    /**
     * @test
     * @magentoDataFixture loadFixtureDefault
     * @group              m2
     */
    public function testProductCustobarJson()
    {
        /** @var $objectManager \Magento\TestFramework\ObjectManager */
        $objectManager = Bootstrap::getObjectManager();

        $product1 =
            $objectManager->create(\Magento\Catalog\Model\Product::class)->load(
                22201
            );
        $product2 =
            $objectManager->create(\Magento\Catalog\Model\Product::class)->load(
                31002
            );

        $json = $this->helper->getCustobarJsonForEntity(
            [
                $product1,
                $product2
            ]
        );

        $data = json_decode($json, true);

        $this->assertCount(
            2,
            $data["products"],
            "are there correct amount of rows"
        );

        $this->assertEquals("wells-0002", $data["products"][0]["external_id"]);
        $this->assertEquals(
            "extension-0002",
            $data["products"][1]["external_id"]
        );
    }

    /**
     * @test
     * @magentoDataFixture Magento/Sales/_files/order_with_customer.php
     * @magentoAppIsolation enabled
     * @group              m2
     */
    public function testMappingOrderWithSimpleProducts()
    {
        /** @var $objectManager \Magento\TestFramework\ObjectManager */
        $objectManager = Bootstrap::getObjectManager();

        /** @var $order1 \Magento\Sales\Model\Order */
        $order = $objectManager->create(Order::class);
        $order->loadByIncrementId('100000001');

        // mapped_from_core_order_fixture
        $fixture = \Spyc::YAMLLoad(
            <<<YAML
            sale_state: PROCESSING
sale_external_id: "100000001"
sale_customer_id: "1"
# dynamic
# sale_date: 2017-04-02T11:23:52-07:00
sale_email: customer@null.com
sale_shop_id: "1"
sale_discount: 0
sale_total: 10000
sale_payment_method: checkmo
magento__items:
  -
    sale_external_id: "100000001"
    external_id: "1"
    product_id: null
    unit_price: 0
    quantity: "2.0000"
    discount: 0
    total: 0
YAML
        );

        $mappedOrder = $this->helper->getMappedEntity($order);

        foreach ($fixture as $key => $value) {
            if (!is_array($value)) {
                $this->assertEquals(
                    $value,
                    $mappedOrder->getData($key),
                    "Failed asserting order mapping $key"
                );
            }
        }

        $orderItems = $mappedOrder->getData("magento__items");

        foreach ($fixture['magento__items'][0] as $key => $value) {
            $this->assertEquals(
                $value,
                $orderItems[0][$key],
                "Failed asserting order item mapping $key"
            );
        }
    }

    /**
     * @test
     * @magentoDataFixture Magento/Sales/_files/order_with_customer.php
     * @group              m2
     */
    public function testSendingOrderWithSimpleProductsData()
    {
        $this->alterDefaults();
        $this->clearSchedules();

        /** @noinspection PhpUndefinedMethodInspection */
        $this->getStore()
            ->setConfig("custobar/custobar_custoconnector/mode", 1);
        $this->setConfigValue(
            "custobar/custobar_custoconnector/prefix",
            "abc",
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        $this->setConfigValue(
            "custobar/custobar_custoconnector/apikey",
            "123",
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        $this->helper->scheduleUpdate(
            100000001,
            1,
            'Magento\Sales\Model\Order'
        );

        $schedulesBefore = $this->getAllSchedules();

        $schedulesBefore->addFieldToFilter(
            "entity_type",
            'Magento\Sales\Model\Order'
        );

        $schedulesBeforeData = $schedulesBefore->getData();
        $this->assertCount(1, $schedulesBefore);
        $this->assertEquals(
            'Magento\Sales\Model\Order',
            $schedulesBeforeData[0]["entity_type"]
        );

        $this->helper->sendUpdateRequests();

        $schedulesAfter = $this->getAllUnProcessedSchedules()->getData();

        $this->assertCount(
            0,
            $schedulesAfter,
            "All schedules have been processed"
        );
        //$this->restoreDefaults();
    }


    /**
     * @test
     * @magentoDataFixture Magento/Sales/_files/order_with_customer.php
     * @group              m2
     */
    public function testInitialTableFunction()
    {
        /** @var $objectManager \Magento\TestFramework\ObjectManager */
        $objectManager = Bootstrap::getObjectManager();

        $this->setConfigValue(
            "custobar/custobar_custoconnector/prefix",
            "abc",
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        $this->setConfigValue(
            "custobar/custobar_custoconnector/apikey",
            "123",
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        $this->clearSchedules();

        Data::$INITIAL_PAGE_SIZE = 2;

        // we can call the method two time without an exception
        $this->helper->startInitialScheduling();
        $this->helper->startInitialScheduling();

        /** @var Initial $model */
        $model = $objectManager->create(Initial::class);

        /** @var \Custobar\CustoConnector\Model\ResourceModel\Initial\Collection $collection */
        $collection = $model->getCollection();

        $collectionData = $collection->getData();
        self::assertCount(
            4,
            $collectionData,
            "Assert that we have the right amount initial schedules present"
        );

        self::assertEquals(1, $collectionData[0]["pages"], "Product pages");
        self::assertEquals(0, $collectionData[0]["page"]);
        self::assertEquals(
            'Magento\Catalog\Model\Product',
            $collectionData[0]["entity_type"]
        );

        self::assertEquals(1, $collectionData[1]["pages"], "Customer pages");
        self::assertEquals(0, $collectionData[1]["page"]);
        self::assertEquals(
            'Magento\Customer\Model\Customer',
            $collectionData[1]["entity_type"]
        );

        self::assertEquals(1, $collectionData[2]["pages"]);
        self::assertEquals(0, $collectionData[2]["page"]);
        self::assertEquals(
            'Magento\Sales\Model\Order',
            $collectionData[2]["entity_type"]
        );

        self::assertEquals(1, $collectionData[3]["pages"], "Store pages");
        self::assertEquals(0, $collectionData[3]["page"]);
        self::assertEquals(
            'Magento\Store\Model\Store',
            $collectionData[3]["entity_type"]
        );

        /** @var array $types */
        $types = $collection->getColumnValues("entity_type");

        self::assertFalse(
            in_array('Magento\Newsletter\Model\Subscriber', $types),
            'Magento\Newsletter\Model\Subscriber is not present as no data in the table'
        );

        $trackedModels = array_keys($this->helper->getConfigTrackedModels());
        $pages = 0;
        foreach ($trackedModels as $trackedModel) {
            // skip internal models as they dont appear in the schedule table
            if ($this->helper->getIsModelInternal($trackedModel)) {
                continue;
            }
            $expectedCollection =
                $objectManager->create($trackedModel)->getCollection();
            /** @var \Magento\Framework\Model\AbstractModel $expectedEntity */
            $pages += (int)ceil($expectedCollection->count() / 2);
        }

        for ($i = 0; $i < $pages; $i++) {
            $this->helper->handleNextInitialPage();
        }

        $schedules = $this->getAllSchedules();
        $schedulesData = $schedules->getData();
        $adminStoreCount = 1;
        $storeCount = 0;
        $customerCount = 1;
        $orderCount = 1;
        $productCount = 1;
        self::assertCount(
            $adminStoreCount + $storeCount + $customerCount + $orderCount
            + $productCount,
            $schedulesData,
            "does the combined amount of items match the scheduled ones"
        );

        $collection->clear();
        $collection->resetData();

        $initialCollectionDataAfter = $collection->getData();

        self::assertCount(
            4,
            $initialCollectionDataAfter,
            "Does the initial table have rows"
        );

        $this->helper->handleNextInitialPage();

        $collection->clear();
        $collection->resetData();

        $initialCollectionDataAfter = $collection->getData();

        self::assertCount(
            0,
            $initialCollectionDataAfter,
            "Is the initial table empty after we call handle once more"
        );

        $model->cleanInitials();

        /** @var \Custobar\CustoConnector\Model\ResourceModel\Initial\Collection $collection */
        $collection = $model->getCollection();

        self::assertCount(0, $collection->getData());
    }

    /**
     * @test
     * @magentoDataFixture loadFixtureDefault
     * @group              m2
     */
    public function testScheduleItemsWebsitePrevention()
    {
        $this->Config()->setValue(
            "custobar/custobar_custoconnector/allowed_websites",
            "2",
            "stores",
            "default"
        );


        // this is in the allowed websites
        $this->assertNotFalse(
            $this->helper->scheduleUpdate(
                22111,
                2,
                'Magento\Catalog\Model\Product'
            )
        );

        // this isnt in the allowed website
        $this->assertNotFalse(
            $this->helper->scheduleUpdate(
                31002,
                5,
                'Magento\Catalog\Model\Product'
            )
        );

        $schedules = $this->getAllUnProcessedSchedules();

        $itemsForProcessing = [];

        /** @var Schedule $schedule */
        foreach ($schedules as $schedule) {
            $entity = $this->helper->getEntityFromSchedule($schedule);
            $this->helper->returnItemForProcessing(
                $entity,
                $schedule,
                $itemsForProcessing
            );
        }

        // we have 1 customer that is allowed from the default fixture
        // and only 1 product that comes form here
        $this->assertCount(2, $itemsForProcessing["entities"]);
        $this->assertCount(2, $itemsForProcessing["schedules"]);

        $this->assertTrue(
            is_a(
                $itemsForProcessing["entities"][0],
                'Magento\Customer\Model\Customer'
            )
        );
        $this->assertTrue(
            is_a(
                $itemsForProcessing["entities"][1],
                'Magento\Catalog\Model\Product'
            )
        );

        $this->assertEquals(22111, $itemsForProcessing["entities"][1]->getId());
    }

    /**
     * @test
     * @magentoDataFixture  loadFixtureDefault
     * @magentoDataFixture  loadFixtureErrorsTable
     * @group               m2
     */
    public function testAddingScheduleErrorMaximum()
    {

        /** @var Schedule $first */
        $schedules = $this->getAllUnProcessedSchedules();
        $first = $schedules->getFirstItem();
        $first->setProcessedAt($this->getFormattedScopeTime());
        $first->setErrors(
            Data::MAX_ERROR_COUNT
        );
        $first->save();
        //$this->getCreatedAtDateAsIsoISO8601()

        /** @var Schedule $last */
        $last = $schedules->getLastItem();
        $last->setProcessedAt($this->getFormattedScopeTime());
        $last->save();

        $this->helper->rescheduleFailedSchedule($first, "key");
        $this->helper->rescheduleFailedSchedule($last, "key");

        $schedules = $this->getAllUnProcessedSchedules();

        $this->assertCount(8, $schedules->getData());

        $correctErrorCount = $this->getAllSchedules();
        $correctErrorCount->getSelect()->where("errors = 1001");

        /** @var Schedule $errorCountSchedule */
        $errorCountSchedule = $correctErrorCount->getFirstItem();

        self::assertEquals(1001, $errorCountSchedule->getErrorsCount());


        $maxErrorCount = $this->getAllSchedules();
        $maxErrorCount->getSelect()->where(
            "errors > " . (Data::MAX_ERROR_COUNT
                - 1)
        );

        self::assertCount(1, $maxErrorCount->getData());
    }

    /**
     * @test
     * @magentoDataFixture Magento/Sales/_files/order_with_customer.php
     * @group              m2
     */
    public function testInitialWithSpecificModel()
    {
        $this->clearSchedules();

        $this->setConfigValue(
            "custobar/custobar_custoconnector/prefix",
            "abc",
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        $this->setConfigValue(
            "custobar/custobar_custoconnector/apikey",
            "123",
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        Data::$INITIAL_PAGE_SIZE = 1;

        // we can call the method two time without an exception
        $this->helper->startInitialScheduling(
            'Magento\Customer\Model\Customer'
        );

        /** @var Initial $model */
        $model = $this->getInitial();

        /** @var \Custobar\CustoConnector\Model\ResourceModel\Initial\Collection $collection */
        $collection = $model->getCollection();

        $collectionData = $collection->getData();
        self::assertCount(1, $collectionData);

        self::assertEquals(1, $collectionData[0]["pages"], "Customer pages");
        self::assertEquals(0, $collectionData[0]["page"]);
        self::assertEquals(
            'Magento\Customer\Model\Customer',
            $collectionData[0]["entity_type"]
        );
    }

    /**
     * @test
     * @magentoDataFixture  loadFixtureDefault
     * @magentoDataFixture  loadFixtureErrorsTable
     * @group               m2
     */
    public function testScheduleErrors()
    {
        /** @var Collection $schedulesCollection */
        $schedulesCollection = $this->getAllSchedules();

        $schedulesCollection->addOnlyErroneousFilter();
        $this->assertCount(
            2,
            $schedulesCollection->getData(),
            "Are there correct amount of Erroneous items"
        );

        $body = "{\"response\": \"ok\", \"id\": \"201611111148539022881736\"}";
        $this->helper->setClient($this->getMockZendClient($body, $body));
        $this->helper->sendUpdateRequests();

        $schedulesCollection->resetData()->clear();
        $schedulesCollection->getSelect()->reset('where');
        $schedulesCollection->getSelect()->where(
            'entity_type = ?',
            "MODEL_THAT_DOESNT_EXISTS"
        );

        /** @var Schedule $nonExistent */
        $nonExistent = $schedulesCollection->getFirstItem();

        self::assertFalse($nonExistent->isEmpty());

        $date = $this->getFormattedScopeTime("Y-m-d H");

        // match the date and hour only. Any more accurate than that well fail unnecessarily.
        self::assertContains(
            $date,
            $nonExistent->getProcessedAt(),
            "Does the date and hour match"
        );

        $schedulesCollection->resetData()->clear();
        $schedulesCollection->getSelect()->reset('where');
        $schedulesCollection->getSelect()->where(
            'entity_type = ?',
            'Magento\Customer\Model\Address'
        );

        /** @var Schedule $address */
        $address = $schedulesCollection->getFirstItem();

        self::assertFalse($address->isEmpty(), "We got an address event");
        self::assertContains(
            $date,
            $address->getProcessedAt()
        );

        $schedulesCollection->resetData()->clear();
        $schedulesCollection->getSelect()->reset('where');
        $schedulesCollection->addOnlyErroneousFilter();
        $this->assertCount(0, $schedulesCollection->getData());

        $schedulesCollection->resetData()->clear();
        $schedulesCollection->getSelect()->reset('where');

        /** @var Schedule $item */
        $errors = 0;
        foreach ($schedulesCollection as $item) {
            $errors += $item->getErrorsCount();
        }

        self::assertEquals(9000, $errors, "Is the error count still the same");
    }

    /**
     * @test
     * @magentoDataFixture  loadFixtureInitialTable
     * @group               m2
     * @group               active
     */
    public function testIsInitialRunning()
    {
        $this->assertTrue($this->helper->isInitialRunning());
        $this->cleanInitials();
        $this->assertFalse($this->helper->isInitialRunning());
    }

    /**
     * @test
     * @group               m2
     */
    public function testLogger()
    {

        $this->helper->log("test", LogLevel::INFO);
    }

    protected function tearDown()
    {
        parent::tearDown();
        // we shouldnt need these in isolation
        //$this->clearSchedules();
        //$this->cleanInitials();
    }

    /**
     * @return Collection
     */
    private function getAllUnProcessedSchedules()
    {
        /** @var Collection $schedules */
        $schedules = $this->getSchedule()
            ->getCollection()
            ->setOrder("entity_id", "ASC")
            ->addOnlyForSendingFilter();

        return $schedules;
    }

    /**
     * @test
     * @magentoDataFixture  Magento/Catalog/_files/enable_price_index_schedule.php
     * @magentoDataFixture  Magento/Catalog/_files/product_simple.php
     * @magentoDbIsolation  disabled
     */
    public function testProductNotInStock()
    {

        $productNotInStockSchedule = $this->helper->scheduleUpdate(
            1,
            1,
            'Magento\Catalog\Model\Product'
        );

        /** @var \Magento\Catalog\Model\Product $productNotInStock */
        $productNotInStock =
            $this->helper->getEntityFromSchedule($productNotInStockSchedule);

        $mappedProductNotInStock = $this->helper->getMappedEntity(
            $productNotInStock
        );


        $this->assertEquals(
            "Simple Product",
            $mappedProductNotInStock->getData("title"),
            "Product price present"
        );

        $this->assertEquals(
            0,
            $mappedProductNotInStock->getData("price"),
            "Product price should not be present since magento does not add the product to price indices"
        );

        $this->clearSchedules();
        $this->cleanInitials();
    }
}
