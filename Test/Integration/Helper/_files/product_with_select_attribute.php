<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Create multiselect attribute
 */
require __DIR__
    . '/../../../../../../../dev/tests/integration/testsuite/Magento/Catalog/Model/Product/Attribute/_files/select_attribute.php';

/** Create product with options and select attribute */

/** @var $installer \Magento\Catalog\Setup\CategorySetup */
$installer =
    \Magento\TestFramework\Helper\Bootstrap::getObjectManager()->create(
        'Magento\Catalog\Setup\CategorySetup'
    );

/** @var $options \Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\Collection */
$options = \Magento\TestFramework\Helper\Bootstrap::getObjectManager()->create(
    'Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\Collection'
);
$options->setAttributeFilter($attribute->getId());
$optionIds = $options->getAllIds();

$product = \Magento\TestFramework\Helper\Bootstrap::getObjectManager()->create(
    'Magento\Catalog\Model\Product'
);
$product->setTypeId(
    \Magento\Catalog\Model\Product\Type::TYPE_SIMPLE
)->setId(
    $optionIds[0] * 8888
)->setAttributeSetId(
    $installer->getAttributeSetId('catalog_product', 'Default')
)->setWebsiteIds(
    [1]
)->setName(
    'With select 1'
)->setSku(
    'simple_s_1'
)->setPrice(
    10
)->setVisibility(
    \Magento\Catalog\Model\Product\Visibility::VISIBILITY_BOTH
)->setSelectAttribute(
    $optionIds[0]
)->setStatus(
    \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED
)->setStockData(
    [
        'use_config_manage_stock' => 1,
        'qty' => 100,
        'is_qty_decimal' => 0,
        'is_in_stock' => 1
    ]
)->save();
