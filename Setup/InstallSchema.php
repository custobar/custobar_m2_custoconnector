<?php

namespace Custobar\CustoConnector\Setup;

use Magento\Framework\DB\Adapter\AdapterInterface;

class InstallSchema implements \Magento\Framework\Setup\InstallSchemaInterface
{
    public function install(
        \Magento\Framework\Setup\SchemaSetupInterface $setup,
        \Magento\Framework\Setup\ModuleContextInterface $context
    ) {
        $installer = $setup;
        $installer->startSetup();

        //START table setup
        $scheduleTableName = $installer->getTable('custoconnector_schedule');

        $scheduleTable = $installer->getConnection()
            ->newTable($scheduleTableName)
            ->addColumn(
                'id',
                \Magento\Framework\Db\Ddl\Table::TYPE_INTEGER,
                null,
                array(
                    'identity' => true,
                    'unsigned' => true,
                    'nullable' => false,
                    'primary' => true
                ),
                'inventory ID'
            )
            ->addColumn(
                'entity_id',
                \Magento\Framework\Db\Ddl\Table::TYPE_INTEGER,
                10,
                array(),
                'Id of the entity stored'
            )
            ->addColumn(
                'entity_type',
                \Magento\Framework\Db\Ddl\Table::TYPE_TEXT,
                255,
                array(),
                'Type of the entity stored'
            )
            ->addColumn(
                'store_id',
                \Magento\Framework\Db\Ddl\Table::TYPE_INTEGER,
                0,
                array('nullable' => false),
                'Store view id when the event happened'
            )
            ->addColumn(
                'created_at',
                \Magento\Framework\Db\Ddl\Table::TYPE_TIMESTAMP,
                null,
                array('nullable' => false),
                'Created Time'
            )
            ->addColumn(
                'processed_at',
                \Magento\Framework\Db\Ddl\Table::TYPE_TIMESTAMP,
                null,
                array('nullable' => false, 'default' => "0000-00-00 00:00:00"),
                'Finish Time'
            )
            ->addColumn(
                'errors',
                \Magento\Framework\Db\Ddl\Table::TYPE_INTEGER,
                0,
                array('nullable' => false),
                'Cumulative error count'
            )
            ->addIndex(
                $installer->getIdxName(
                    'custoconnector_schedule',
                    array(
                        'entity_type',
                        'entity_id',
                        'store_id',
                        'processed_at'
                    ),
                    AdapterInterface::INDEX_TYPE_UNIQUE
                ),
                array('entity_type', 'entity_id', 'store_id', 'processed_at'),
                array('type' => AdapterInterface::INDEX_TYPE_UNIQUE)
            );
        $installer->getConnection()->createTable($scheduleTable);


        $initialTableName = $installer->getTable('custoconnector_initial');
        $initialTable = $installer->getConnection()
            ->newTable($initialTableName)
            ->addColumn(
                'id',
                \Magento\Framework\Db\Ddl\Table::TYPE_INTEGER,
                null,
                array(
                    'identity' => true,
                    'unsigned' => true,
                    'nullable' => false,
                    'primary' => true
                ),
                'initial id'
            )
            ->addColumn(
                'page',
                \Magento\Framework\Db\Ddl\Table::TYPE_INTEGER,
                null,
                array(),
                'current page'
            )
            ->addColumn(
                'pages',
                \Magento\Framework\Db\Ddl\Table::TYPE_INTEGER,
                null,
                array(),
                'total pages'
            )
            ->addColumn(
                'entity_type',
                \Magento\Framework\Db\Ddl\Table::TYPE_TEXT,
                255,
                array(),
                'Type of the entity stored'
            )
            ->addColumn(
                'created_at',
                \Magento\Framework\Db\Ddl\Table::TYPE_TIMESTAMP,
                null,
                array('nullable' => false),
                'Created Time'
            )
            ->addIndex(
                $installer->getIdxName(
                    'custoconnector/initial',
                    array('entity_type'),
                    AdapterInterface::INDEX_TYPE_UNIQUE
                ),
                array('entity_type'),
                array('type' => AdapterInterface::INDEX_TYPE_UNIQUE)
            );
        $installer->getConnection()->createTable($initialTable);

        //END   table setup
        $installer->endSetup();
    }
}
