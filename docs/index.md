Custobar_CustoConnector
=========


## Settings

Follow the installation guides from [README.md](../README.md)

## Initial scheduling

After installing the extension it is necessary to transfer the original store data to Custobar at least once. 

Go to `SYSTEM > Other Settings: Custobar Status` to start the scheduling by clicking the `Initialize scheduling queue for all model types` button. 

This will start the process of collecting data to the queue. 

It is important to note that the queue purging is halted during the initial scheduling and after that normal operation will be continued.  

Also the queue and initial scheduling uses Magento's cron cycles so updates will happen by every minute.

## Data mapping

Under `STORES > Settings:Configuration > CUSTOBAR:Custoconnector > Configuration > Model classes to listen` you can control how data is mapped to Custobar or add custom mappings.

```
Magento\Catalog\Model\Product>products:
  name>title;
  sku>external_id;
  custobar_minimal_price>minimal_price;
  custobar_price>price;
```

Previous snipped tells how a model / value from Magento is mapped to Custobar. Right of the > is Custobar side and on the left is what the model or attribute name is in Magento.

### Mapping new data to Custobar

When the data is a standard Magento attribute/table column you can just add `attributeatmagento>attributeatcustobar` under the correct model line.
 
If the data comes from a complex relation (method) then you need to add an observer to feature the data as a mappable entity.

See how `custobar_minimal_price` is mapped with `public function extendProductData($observer)` on `Observer/Main.php`

## FAQ

### No data appears to Custobar views

1. Wrong details in settings
2. Magento's cron is not running correctly
3. Initial data scheduling is running

    Please check the status at `SYSTEM > Other Settings: Custobar Status`

4. Initial data scheduling has failed
    
    Depending on the resources of the server there might be a need use a lower value on the `public static $INITIAL_PAGE_SIZE = 500;` 
    found in `Helper/Data.php` and use the `Cancel initial` button and restart initial scheduling. 
    
5. Allowed websites to send data from prevents sending data
