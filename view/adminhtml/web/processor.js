define([
    'ko',
    'uiComponent',
    'jquery'
], function (ko, Component, $) {
    'use strict';

    var delay = 10000;
    var updateBlocked = false;
    return Component.extend({
        initialize: function () {
            this._super();
            this.url = this.jsParams.AjaxUrl;
            this.cancelUrl = this.jsParams.AjaxCancelUrl;
            this.fetchIndicator = ko.observable('');
            this.callIndicator = ko.observable('');
            this.notProcessing = ko.observable(false);
            this.isProcessing = ko.observable(false);
            this.error = ko.observable('');
            this.message = ko.observable('');
            this.queueUpdate();
        },
        queueUpdate: function () {
            if (!updateBlocked) {
                this.fetchIndicator('.');
                updateBlocked = true;
                this.fetchUpdate();
            }
        },
        process: function (data, e) {
            if (e.preventDefault) e.preventDefault();
            var self = this;
            var elem = $(e.target).closest("li, button");
            var model = elem.hasClass('action-default') ? 'all' : elem.data('model');
            self.fetchIndicator('.');
            self.callIndicator('executing');
            var param = 'ajax=1&model=' + model;
            $.ajax({
                url: this.url,
                data: param,
                type: "POST",
                dataType: 'json'
            }).done(function (data) {
                self.fetchIndicator('');
                self.callIndicator('');
            });
        },
        cancelprocess: function (data, e) {
            if (e.preventDefault) e.preventDefault()
            var self = this;
            self.fetchIndicator('.');
            self.callIndicator('executing');
            $.ajax({
                url: this.cancelUrl
            }).done(function (data) {
                self.fetchIndicator('');
                self.callIndicator('');
            });
        },
        fetchUpdate: function () {
            var self = this;
            var param = 'ajax=1';
            $.ajax({
                url: this.url,
                data: param,
                type: "POST",
                dataType: 'json'
            }).done(function (data) {
                self.fetchIndicator('');
                self.notProcessing(data.processing == '0');
                self.isProcessing(data.processing == '1');
                self.error(data.error ? data.error : 'none');
                self.message(data.message);
                setTimeout(function () {
                    updateBlocked = false;
                    self.queueUpdate();
                }, delay);
            });
        }

    });
});
