define([
    'ko',
    'uiComponent',
    'jquery'
], function (ko, Component, $) {
    'use strict';

    var delay = 10000;
    var updateBlocked = false;
    return Component.extend({
        initialize: function () {
            this._super();
            this.url = this.jsParams.AjaxUrl;
            this.errors = ko.observable('-');
            this.queued = ko.observable('-');
            this.fetching = ko.observable('.');
            this.queueUpdate();
        },
        queueUpdate: function () {
            if(!updateBlocked){
                this.fetching('.');
                updateBlocked = true;
                this.fetchUpdate();
            }
        },
        fetchUpdate: function () {
            var self = this;
            var param = 'ajax=1';
            $.ajax({
                url: this.url,
                data: param,
                type: "POST",
                dataType: 'json'
            }).done(function (data) {
                self.fetching('');
                self.queued(data.queued);
                self.errors(data.errors);
                setTimeout(function () {
                    updateBlocked = false;
                    self.queueUpdate();
                },delay);
            });
        }
    });
});
