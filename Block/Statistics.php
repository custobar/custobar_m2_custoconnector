<?php

namespace Custobar\CustoConnector\Block;

use Custobar\CustoConnector\Helper\Data;
use Magento\Catalog\Model\Product;
use Magento\Customer\Model\Customer;
use Magento\Customer\Model\Session;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Model\StoreManagerInterface;

class Statistics extends \Magento\Framework\View\Element\Template
{
    /**
     * @var Data
     */
    private $helper;

    /**
     * @var Session
     */
    protected $customerSession;

    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    public function __construct(
        Context $context,
        Session $customerSession,
        Registry $registry,
        Data $helper,
        array $data = []
    ) {
        $this->customerSession = $customerSession;
        $this->registry = $registry;
        $this->storeManager = $context->getStoreManager();
        $this->helper = $helper;

        parent::__construct(
            $context,
            $data
        );
    }


    /**
     * @return Customer|null
     */
    public function getCustomer()
    {
        /** @var Session $session */
        $session = $this->customerSession;

        if ($session->isLoggedIn()) {
            return $session->getCustomer();
        }

        return null;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        /** @var Product $product */
        $product = $this->registry->registry('current_product');
        return $product;
    }

    /**
     * @return Data
     */
    public function getCHelper()
    {
        return $this->helper;
    }

    /**
     * @return string
     */
    public function getTrackingScript()
    {
        return $this->getCHelper()->getTrackingScript();
    }

    public function isProductView()
    {
        if (in_array(
            "catalog_product_view",
            $this->getLayout()->getUpdate()->getHandles()
        )) {
            return true;
        }
        return false;
    }

    public function isAllowedWebsite()
    {
        $store = $this->storeManager->getStore();
        $websiteId = $store->getWebsiteId();
        return $this->getCHelper()->getIsWebsiteAllowed($websiteId);
    }

    public function canTrack()
    {
        if (
            $this->getCHelper()->getTrackingScript()
            && $this->isAllowedWebsite()
        ) {
            return true;
        }
        return false;
    }
}
