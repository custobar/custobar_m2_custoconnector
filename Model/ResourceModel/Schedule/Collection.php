<?php
namespace Custobar\CustoConnector\Model\ResourceModel\Schedule;


class Collection
    extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    public function __construct(
        \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Framework\DB\Adapter\AdapterInterface $connection = null,
        \Magento\Framework\Model\ResourceModel\Db\AbstractDb $resource = null
    ) {
        parent::__construct(
            $entityFactory,
            $logger,
            $fetchStrategy,
            $eventManager,
            $connection,
            $resource
        );
    }

    public function _construct()
    {
        $this->_init(
            '\Custobar\CustoConnector\Model\Schedule',
            '\Custobar\CustoConnector\Model\ResourceModel\Schedule'
        );
    }

    /**
     * Add filter by only ready for sending item
     *
     * @return \Custobar\CustoConnector\Model\ResourceModel\Schedule\Collection
     */
    public function addOnlyForSendingFilter()
    {
        $this->getSelect()
            ->where('main_table.processed_at = "0000-00-00 00:00:00"');
        return $this;
    }

    public function addOnlyErroneousFilter()
    {
        $this->addOnlyForSendingFilter();
        $this->getSelect()
            ->where('main_table.errors > 0');
        return $this;
    }
}
