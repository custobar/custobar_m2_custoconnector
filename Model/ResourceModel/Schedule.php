<?php
namespace Custobar\CustoConnector\Model\ResourceModel;

use \Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use \Magento\Framework\Model\ResourceModel\Db\Context;

class Schedule extends AbstractDb
{
    public function __construct(
        Context $context,
        $connectionName = null
    ) {
        parent::__construct(
            $context,
            $connectionName
        );
    }

    protected function _construct()
    {
        $this->_init("custoconnector_schedule", "id");
    }

    /**
     * @return \Custobar\CustoConnector\Model\ResourceModel\Schedule
     */
    public function removeProcessedSchedules()
    {
        $this->getConnection()->delete(
            $this->getMainTable(),
            ['processed_at != ?' => "0000-00-00 00:00:00"]
        );
        return $this;
    }


    public function getUnique($entity_type, $entity_id, $store_id, $processed = "0000-00-00 00:00:00")
    {
        $select = $this->_getUniqueSelect();
        $params = [
            'entity_type' => $entity_type,
            'entity_id' => $entity_id,
            'store_id' => $store_id,
            'processed_at' => $processed
        ];
        $connection = $this->getConnection();
        $result = $connection->fetchAll($select, $params);
        return $result;
    }

    /**
     * Retrieve load select for unique record
     *
     * @return \Magento\Framework\DB\Select
     */
    private function _getUniqueSelect()
    {
        $select = $this->getConnection()->select()->from(
            ['s' => $this->getMainTable()]
        )->where(
            's.entity_type = :entity_type AND s.entity_id = :entity_id AND s.store_id = :store_id AND s.processed_at = :processed_at'
        );
        return $select;
    }
}
