<?php
namespace Custobar\CustoConnector\Model\ResourceModel;


class Initial
    extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        $connectionName = null
    ) {
        parent::__construct(
            $context,
            $connectionName
        );
    }

    public function removeAll()
    {
        $this->getConnection()->delete(
            $this->getMainTable()
        );
        return $this;
    }

    protected function _construct()
    {
        $this->_init("custoconnector_initial", "id");
    }

    public function typeExists($entity_type)
    {
        $select = $this->_getTypeSelect();
        $params = [
            'entity_type' => $entity_type,
        ];
        $connection = $this->getConnection();
        $result = $connection->fetchAll($select, $params);
        return !empty($result) ? true : false;
    }

    public function deleteType($entity_type)
    {
        $connection = $this->getConnection();
        $cond = ["entity_type = ?" => $entity_type];
        $result = $connection->delete($this->getMainTable(), $cond);
        return $result;
    }

    /**
     * Retrieve load select for type records
     *
     * @return \Magento\Framework\DB\Select
     */
    private function _getTypeSelect()
    {
        $select = $this->getConnection()->select()->from(
            ['i' => $this->getMainTable()]
        )->where(
            'i.entity_type = :entity_type'
        );
        return $select;
    }
}
