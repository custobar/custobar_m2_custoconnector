<?php

namespace Custobar\CustoConnector\Model;

use Custobar\CustoConnector\Helper\Data;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Registry;

/**
 * Class \Custobar\CustoConnector\Model\Schedule
 *
 * @method int getStoreId();
 * @method Schedule setStoreId(int $value)
 * @method int getEntityId()
 * @todo should change the reference of setEntityId to someting else as there is allready a method with that name
 * @method Schedule setEntityId(int $value)
 * @method int getEntityType()
 * @method Schedule setEntityType(string $value)
 * @method Schedule setCreatedAt(string $value)
 * @method string getProcessedAt()
 * @method Schedule setProcessedAt(string $value)
 * @method Schedule setErrors(int $value)
 * @method int getErrors()
 *
 */
class Schedule extends AbstractModel
{
    const LOCK_SUFFIX = '.lock';

    /** @var Data */
    public $helper;

    /**
     * @var DirectoryList
     */
    private $directory_list;

    public function __construct(
        Context $context,
        Registry $registry,
        Data $helper,
        DirectoryList $directory_list,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection,
            $data
        );
        $this->helper = $helper;
        $this->directory_list = $directory_list;
    }

    /**
     * @return int
     */
    public function getErrorsCount()
    {
        return (int)$this->getErrors();
    }

    /**
     * @return string
     */
    public function lockFilePath()
    {
        $lock_file =
            $this->directory_list->getPath(DirectoryList::VAR_DIR)
            . "/custconnector-initial" . self::LOCK_SUFFIX;
        return $lock_file;
    }

    protected function _construct()
    {
        $this->_init('\Custobar\CustoConnector\Model\ResourceModel\Schedule');
    }

    /**
     * @return Schedule
     */
    public function cleanSchedules()
    {
        /** @var ResourceModel\Schedule $res */
        $res = $this->_getResource();
        $res->removeProcessedSchedules();
        return $this;
    }

    public function initialPopulation()
    {
        if (($pid = $this::lock()) !== false) {
            $this->helper->handleNextInitialPage();
            $this->unlock();
        }
    }

    private static $pid;

    private function isrunning()
    {
        $pids = explode(PHP_EOL, `ps -e | awk '{print $1}'`);
        if (in_array(self::$pid, $pids)) {
            return true;
        }
        return false;
    }

    public function lock()
    {
        $lock_file = $this->lockFilePath();
        if (file_exists($lock_file)) {
            //return FALSE;

            // Is running?
            self::$pid = file_get_contents($lock_file);
            if ($this->isrunning()) {
                return false;
            }
        }

        self::$pid = getmypid();
        file_put_contents($lock_file, self::$pid);
        return self::$pid;
    }

    public function unlock()
    {
        $lock_file = $this->lockFilePath();

        if (file_exists($lock_file)) {
            unlink($lock_file);
        }
        return true;
    }
}
