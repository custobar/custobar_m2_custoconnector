<?php

namespace Custobar\CustoConnector\Model;


/**
 * Class Custobar_CustoConnector_Model_Initial
 *
 * @method int getPages();
 * @method Custobar_CustoConnector_Model_Initial setPages(int $value)
 * @method int getPage();
 * @method Custobar_CustoConnector_Model_Initial setPage(int $value)
 * @method int getEntityType();
 * @method Custobar_CustoConnector_Model_Initial setEntityType(string $value)
 *
 */
class Initial extends \Magento\Framework\Model\AbstractModel
{
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection,
            $data
        );
    }

    protected function _construct()
    {
        $this->_init('\Custobar\CustoConnector\Model\ResourceModel\Initial');
    }

    /**
     * @return Initial
     */
    public function cleanInitials()
    {
        /** @var ResourceModel\Initial $res */
        $res = $this->_getResource();
        $res->removeAll();
        return $this;
    }
}
