<?php

namespace Custobar\CustoConnector\Observer;

use Custobar\CustoConnector\Helper\Data;
use Custobar\CustoConnector\Model\ScheduleFactory;
use Magento\Catalog\Helper\Image;
use Magento\Catalog\Model\Product\Media\ConfigFactory;
use Magento\Customer\Model\Customer;
use Magento\Eav\Model\Entity\Attribute\SetFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\ObjectManager\ObjectManager;
use Magento\Framework\Stdlib\DateTime\Timezone;
use Magento\Framework\UrlInterface;
use Magento\Sales\Model\Order\Address;
use Magento\Store\Model\StoreManagerInterface;
use Magento\UrlRewrite\Model\UrlRewriteFactory;
use Magento\Catalog\Model\Product\Gallery\ReadHandler as GalleryReadHandler;

class Main implements ObserverInterface
{
    /** @var \Magento\Catalog\Model\ProductFactory */
    private $productFactory;

    private $_helper;

    /**
     * @var ObjectManager
     */
    protected $objectManager;

    /**
     * @var SetFactory
     */
    protected $eavEntityAttributeSetFactory;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var ConfigFactory
     */
    protected $catalogProductMediaConfigFactory;

    /**
     * @var UrlRewriteFactory
     */
    protected $urlRewriteUrlRewriteFactory;

    /**
     * @var Image
     */
    protected $catalogImageHelper;

    /**
     * @var Timezone
     */
    private $timeZone;

    /**
     * @var \Magento\ConfigurableProduct\Model\Product\Type\Configurable\PriceFactory
     */
    private $configurablePriceFactory;

    /**
     * @var GalleryReadHandler
     */
    private $galleryReadHandler;

    /**
     * @var \Magento\Framework\EntityManager\MetadataPool
     */
    private $metadataPool;

    /**
     * Type configurable factory
     *
     * @var \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\ConfigurableFactory
     */
    protected $typeConfigurableFactory;

    /**
     * @var \Magento\Bundle\Model\ResourceModel\Selection
     */
    protected $_bundleSelection;

    /**
     * Catalog product link
     *
     * @var \Magento\GroupedProduct\Model\ResourceModel\Product\Link
     */
    protected $productLinks;


    /**
     * Main constructor.
     *
     * @param ObjectManager                                                             $objectManager
     * @param SetFactory                                                                $eavEntityAttributeSetFactory
     * @param StoreManagerInterface                                                     $storeManager
     * @param ScopeConfigInterface                                                      $scopeConfig
     * @param ConfigFactory                                                             $catalogProductMediaConfigFactory
     * @param UrlRewriteFactory                                                         $urlRewriteUrlRewriteFactory
     * @param Image                                                                     $catalogImageHelper
     * @param Data                                                                      $helper
     * @param ScheduleFactory                                                           $scheduleFactory
     * @param \Magento\ConfigurableProduct\Model\Product\Type\Configurable\PriceFactory $configurablePriceFactory
     * @param \Magento\Catalog\Model\Product\Gallery\ReadHandler                        $galleryReadHandler
     */
    public function __construct(
        ObjectManager $objectManager,
        SetFactory $eavEntityAttributeSetFactory,
        StoreManagerInterface $storeManager,
        ScopeConfigInterface $scopeConfig,
        ConfigFactory $catalogProductMediaConfigFactory,
        UrlRewriteFactory $urlRewriteUrlRewriteFactory,
        Image $catalogImageHelper,
        Data $helper,
        ScheduleFactory $scheduleFactory,
        \Magento\ConfigurableProduct\Model\Product\Type\Configurable\PriceFactory $configurablePriceFactory,
        GalleryReadHandler $galleryReadHandler,
        \Magento\Framework\EntityManager\MetadataPool $metadataPool,
        Timezone $timezone,
        \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\ConfigurableFactory $typeConfigurableFactory,
        \Magento\Bundle\Model\ResourceModel\Selection $bundleSelection,
        \Magento\GroupedProduct\Model\ResourceModel\Product\Link $catalogProductLink,
        \Magento\Catalog\Model\ProductFactory $productFactory
    ) {
        $this->objectManager = $objectManager;
        $this->eavEntityAttributeSetFactory = $eavEntityAttributeSetFactory;
        $this->storeManager = $storeManager;
        $this->scopeConfig = $scopeConfig;
        $this->catalogProductMediaConfigFactory =
            $catalogProductMediaConfigFactory;
        $this->urlRewriteUrlRewriteFactory = $urlRewriteUrlRewriteFactory;
        $this->catalogImageHelper = $catalogImageHelper;
        $this->_helper = $helper;
        $this->configurablePriceFactory = $configurablePriceFactory;
        $this->galleryReadHandler = $galleryReadHandler;
        $this->metadataPool = $metadataPool;
        $this->timeZone = $timezone;
        $this->typeConfigurableFactory = $typeConfigurableFactory;
        $this->_bundleSelection = $bundleSelection;
        $this->productLinks = $catalogProductLink;
        $this->productFactory = $productFactory;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        /** @var Data $ch */
        $ch = $this->getCHelper();
        try {
            if ($ch->canProcess()) {
                /** @var \Magento\Framework\Model\AbstractModel $ob */
                $ob = $observer->getObject();
                $modelClassName = get_class($ob);
                $trackedModelClassName = $ch->getEvenObjectTrackedModel($ob);

                if ($trackedModelClassName) {
                    if ($ch->getIsModelInternal($trackedModelClassName)) {
                        $mapped = $ch->getMappedEntity($ob);
                        $mapSet = $ch->getModelMapSet($trackedModelClassName);
                        $toClass = $mapSet["to"];

                        if (
                            !$mapped->isEmpty()
                            && $mapSet
                            && isset($toClass)
                            && $mapped->getId()
                        ) {
                            $targetMapSet =
                                $ch->getModelMapSet($toClass);

                            if ($targetMapSet) {
                                $entityFromTo = $ch->getEntityByTypeAndId(
                                    $toClass,
                                    $mapped->getId(),
                                    $ob->getStoreId()
                                );

                                if ($ch->getCanTrackObject(
                                    $entityFromTo
                                )
                                ) {
                                    $this->scheduleByStoreIdsIfPresent(
                                        $entityFromTo,
                                        $toClass
                                    );
                                } else {
                                    $ch->log(
                                        "Rejected tracking item {$mapped->getId()} of {$toClass}"
                                    );
                                }
                            }
                        }
                    } else {
                        if ($ch->getCanTrackObject($ob)) {
                            $this->scheduleByStoreIdsIfPresent(
                                $ob,
                                $modelClassName
                            );
                        } else {
                            $ch->log(
                                "Rejected tracking item {$ob->getId()} of {$modelClassName}"
                            );
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $ch->log("Model after save failed {$message}");
        }
    }

    /**
     * @param Observer $observer
     */
    public function extendCustomerData($observer)
    {
        /** @var Customer $customer */
        $customer = $observer->getEntity();

        /** @var Address $defaultAddress */
        $defaultAddress = $customer->getPrimaryBillingAddress();

        $additionalData = [];

        if ($defaultAddress) {
            $additionalData["custobar_street"] =
                implode(', ', $defaultAddress->getStreet());
            $additionalData["custobar_city"] = $defaultAddress->getCity();
            $additionalData["custobar_country_id"] =
                $defaultAddress->getCountryId();
            $additionalData["custobar_region"] =
                $defaultAddress->getRegion();
            $additionalData["custobar_postcode"] =
                $defaultAddress->getPostcode();
            $additionalData["custobar_telephone"] =
                $defaultAddress->getTelephone();
        }

        $additionalData["custobar_created_at"] =
            $this->getCreatedAtDateAsIsoISO8601($customer->getCreatedAt());


        $customer->addData($additionalData);
    }

    /**
     * @param Observer $observer
     */
    public function extendProductData($observer)
    {
        /** @var \Magento\Catalog\Model\Product $product */
        $product = $observer->getEntity();
        /** @var array $map */
        $map = $observer->getMap();

        $attributes = $product->getAttributes();

        $additionalData = [];

        $attributeSetModel = $this->eavEntityAttributeSetFactory->create();
        $attributeSetModel->load($product->getAttributeSetId());
        $attributeSetName = $attributeSetModel->getAttributeSetName();
        $additionalData["custobar_attribute_set_name"] = $attributeSetName;

        $additionalData["custobar_price"] =
            round((float)$product->getPrice() * 100);

        $additionalData["custobar_minimal_price"] =
            round((float)$product->getMinimalPrice() * 100);

        /** @var array $storeIds */
        $storeIds = $product->getStoreIds();
        $storeId = 0;


        if ($product->getStore()) {
            $storeId = $product->getStore()->getId();
        } else {
            // Lets get the default store and check if product is in it > if so use that to get the ur
            $defaultStoreId = $this->storeManager->getStore()->getId();
            if (in_array($defaultStoreId, $storeIds)) {
                $storeId = $defaultStoreId;
            } else {
                $storeId = array_shift($storeIds);
            }
        }

        /** @var \Magento\Store\Model\Store $store */
        $store = $this->storeManager->getStore($storeId);

        if ($product->isVisibleInSiteVisibility()) {
            $url = $product->getProductUrl();

            if ($url) {
                $url = $store->getUrl($url);
            } else {
                $product->getUrlInStore(
                    ["_store" => $storeId]
                );
            }

            $additionalData["custobar_product_url"] = $url;
        }

        $categories =
            $product->getCategoryCollection()
                ->setStoreId($storeId)
                ->addAttributeToSelect(
                    'name'
                );

        $categoriesNames = [];
        $categoriesIds = [];

        foreach ($categories as $category) {
            $categoriesNames[] = $category->getName();
            $categoriesIds[] = $category->getId();
        }

        $additionalData["custobar_category"] = implode($categoriesNames, ",");
        $additionalData["custobar_category_id"] = implode($categoriesIds, ",");


        $additionalData["custobar_special_price"] =
            round((float)$product->getSpecialPrice() * 100);


        $locale = $this->scopeConfig->getValue(
            \Magento\Directory\Helper\Data::XML_PATH_DEFAULT_LOCALE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );

        $lang = "fi";

        if ($locale) {
            $localeArray = explode('_', $locale);
            if (isset($localeArray[0])) {
                $lang = $localeArray[0];
            }
        }

        $additionalData["custobar_language"] = $lang;
        $additionalData["custobar_store_id"] = $product->getStoreId();

        $mediaBaseUrl = $store->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);
        $imageLink = "{$mediaBaseUrl}catalog/product{$product->getImage()}";

        $additionalData["custobar_image"] = $imageLink;

        $typeInstance = $product->getTypeInstance();

        if ($product->getTypeId() == "configurable") {
            $additionalData = $this->getConfigurableChildren(
                $typeInstance,
                $product,
                $additionalData
            );
        }

        if ($product->getTypeId() == "bundle") {
            $additionalData = $this->getBundleChildren(
                $typeInstance,
                $product,
                $storeId,
                $additionalData
            );
        }

        if ($product->getTypeId() == "grouped") {
            $additionalData = $this->getGroupedChildren(
                $typeInstance,
                $product,
                $storeId,
                $additionalData
            );
        }

        $configurableParents =
            $this->typeConfigurableFactory->create()->getParentIdsByChild(
                $product->getId()
            );

        $bundleParents =
            $this->_bundleSelection->getParentIdsByChild($product->getId());

        $groupedParents = $this->productLinks->getParentIdsByChild(
            $product->getId(),
            \Magento\GroupedProduct\Model\ResourceModel\Product\Link::LINK_TYPE_GROUPED
        );

        $parent_ids = array_unique(
            array_merge(
                $configurableParents,
                $bundleParents,
                $groupedParents
            )
        );

        // bundle item appears as a parent also for some reason. Pop at least that out
        if (($key = array_search($product->getId(), $parent_ids)) !== false) {
            unset($parent_ids[$key]);
        }

        if (count($parent_ids) > 0) {
            $parentSkus = $this->getProductSkusByID($storeId, $parent_ids);

            if (count($parentSkus) > 0) {
                $additionalData["custobar_parent_ids"] =
                    implode(",", $parentSkus);
            }
        }


        foreach ($map as $index => $item) {
            if (strpos($index, "custobar") === false
                && key_exists($index, $attributes)) {

                $productAttribute = $attributes[$index];

                $input = $productAttribute->getFrontend()->getConfigField(
                    'input'
                );

                // skip if not a dropdown element
                if (!in_array($input, ['select', 'multiselect'])) {
                    continue;
                }

                $productResource = $product->getResource();

                $attributeRawValue =
                    $productResource->getAttributeRawValue(
                        $product->getId(),
                        $index,
                        $storeId
                    );

                if ($attributeRawValue) {
                    $product->setData($index, $attributeRawValue);
                    $additionalData[$index] =
                        $productAttribute->getFrontend()->getValue(
                            $product
                        );
                }
            }
        }

        $product->addData($additionalData);
    }

    /**
     * @param Observer $observer
     */
    public function extendOrderData($observer)
    {
        /** @var \Magento\Sales\Model\Order $order */
        $order = $observer->getEntity();

        /** @var array $additionalData */
        $additionalData = [];

        $additionalData["custobar_created_at"] =
            $this->getCreatedAtDateAsIsoISO8601((string)$order->getCreatedAt());

        $additionalData["custobar_discount"] =
            round((float)$order->getDiscountAmount() * 100);

        $additionalData["custobar_grand_total"] =
            round((float)$order->getGrandTotal() * 100);

        $payment = $order->getPayment();
        if ($payment) {
            $additionalData["custobar_payment_method"] =
                $payment->getMethod();
        }

        $visibleItems = $order->getAllVisibleItems();

        $additionalData["custobar_order_items"] = [];

        /** @var \Magento\Sales\Model\Order\Item $item */
        foreach ($visibleItems as $item) {
            $orderItem = [];
            $orderItem["sale_external_id"] = $order->getIncrementId();
            $orderItem["external_id"] = $item->getId();
            $orderItem["product_id"] = $item->getSku();
            $orderItem["unit_price"] =
                round((float)$item->getPriceInclTax() * 100);
            $orderItem["quantity"] = $item->getQtyOrdered();
            $orderItem["discount"] =
                round((float)$item->getDiscountAmount() * 100);
            $orderItem["total"] =
                round(
                    (float)$item->getRowTotalInclTax() * 100
                    - (float)$item->getDiscountAmount() * 100
                );
            $additionalData["custobar_order_items"][] = $orderItem;
        }

        $additionalData["custobar_state"] = strtoupper($order->getState());
        $order->addData($additionalData);
    }

    /**
     * @param Observer $observer
     */
    public function extendSubscriberData($observer)
    {
        /** @var \Magento\Newsletter\Model\Subscriber $newsletterSubscriber */
        $newsletterSubscriber = $observer->getEntity();

        /** @var array $additionalData */
        $additionalData = [];
        //custobar_status>type;
        //custobar_change_status_at>date;

        $status = "MAIL_UNSUBSCRIBE";

        if ($newsletterSubscriber->getStatus()
            == \Magento\Newsletter\Model\Subscriber::STATUS_SUBSCRIBED
        ) {
            $status = "MAIL_SUBSCRIBE";
        }

        $additionalData["custobar_date"] =
            $this->getCHelper()->getFormattedScopeTime();
        $additionalData["custobar_status"] = $status;

        $newsletterSubscriber->addData($additionalData);
    }

    /**
     * @param Observer $observer
     */
    public function extendStoreData($observer)
    {
        /** @var \Magento\Store\Model\Store $store */
        $store = $observer->getEntity();
        $store->setData(
            "custobar_name",
            "{$store->getWebsite()->getName()}, {$store->getGroup()->getName()}, {$store->getName()}"
        );
    }


    /**
     * Get object created at date affected current active store timezone
     *
     * @param string $createdAt
     *
     * @return \\Zend_Date
     */
    public function getCreatedAtDateAsIsoISO8601($createdAt)
    {
        $time = strtotime($createdAt);
        $date = $this->timeZone->scopeDate(null, $time, true);

        return $date->format(\Zend_Date::ISO_8601);
    }

    /**
     * Obsolete
     *
     * @param $productId
     * @param $categoryId
     * @param $storeId
     *
     * @return mixed
     */
    public function rewrittenProductUrl($productId, $categoryId, $storeId)
    {
        /** @var \Magento\UrlRewrite\Model\UrlRewrite $coreUrl */
        $coreUrl = $this->urlRewriteUrlRewriteFactory->create();
        $idPath = sprintf('product/%d', $productId);
        if ($categoryId) {
            $idPath = sprintf('%s/%d', $idPath, $categoryId);
        }
        $coreUrl->setStoreId($storeId);
        $coreUrl->loadByIdPath($idPath);

        return $coreUrl->getRequestPath();
    }

    /**
     * @param \Magento\Framework\Model\AbstractModel $ob
     * @param string                                 $modelClassName
     */
    public function scheduleByStoreIdsIfPresent($ob, $modelClassName)
    {
        $classArray = explode('\\', $modelClassName);

        if (end($classArray) === 'Interceptor') {
            array_pop($classArray);
            $modelClassName = implode('\\', $classArray);
        }

        $storeIds = $ob->getStoreIds();

        if ($storeIds && is_array($storeIds) && count($storeIds) > 0) {
            /** @var int $storeId */
            foreach ($storeIds as $storeId) {
                $this->getCHelper()->scheduleUpdate(
                    $ob->getId(),
                    $storeId,
                    $modelClassName
                );
            }
        } else {
            $this->getCHelper()->scheduleUpdate(
                $ob->getId(),
                $ob->getStoreId(),
                $modelClassName
            );
        }
    }

    /**
     * @return \Custobar\CustoConnector\Helper\Data
     */
    public function getCHelper()
    {
        return $this->_helper;
    }

    /**
     * @param $storeId
     * @param $parent_ids
     *
     * @return array
     */
    protected function getProductSkusByID($storeId, $parent_ids)
    {
        /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $parentCollection */
        $parentCollection = $this->productFactory
            ->create()
            ->setStoreId($storeId)
            ->getCollection();
        $productAttributes = ["sku"];
        $parentCollection->setStoreId($storeId);
        $parentCollection
            ->addAttributeToSelect($productAttributes)
            ->addAttributeToFilter("entity_id", ["in" => $parent_ids]);

        $parentSkus = $parentCollection->load()->getColumnValues("sku");
        return $parentSkus;
    }

    /**
     * @param $typeInstance \Magento\Bundle\Model\Product\Type
     * @param $product
     * @param $storeId
     * @param $additionalData
     *
     * @return mixed
     */
    protected function getBundleChildren(
        $typeInstance,
        $product,
        $storeId,
        $additionalData
    ) {
        $childIds =
            $typeInstance->getChildrenIds(
                $product->getId(),
                false
            );


        if (count($childIds) > 0) {
            $additionalData["custobar_child_ids"] =
                implode(
                    ",",
                    $this->getProductSkusByID($storeId, $childIds)
                );
        }
        return $additionalData;
    }

    /**
     * @param $typeInstance \Magento\ConfigurableProduct\Model\Product\Type\Configurable
     * @param $product
     * @param $additionalData
     *
     * @return mixed
     */
    protected function getConfigurableChildren(
        $typeInstance,
        $product,
        $additionalData
    ) {
        $childProducts = $typeInstance->getUsedProducts($product);

        if ($childProducts) {
            $childSkus = [];
            foreach ($childProducts as $childProduct) {
                $childSkus[] = $childProduct->getSku();
            }

            $additionalData["custobar_child_ids"] =
                implode(",", $childSkus);
        }
        return $additionalData;
    }

    private function getRawMediaUrl($product, $code)
    {
        return (string)$this->_getImageHelper()
            ->init($product, $code);
    }

    /**
     * Return Catalog Product Image helper instance
     *
     * @return Image
     */
    protected function _getImageHelper()
    {
        return Mage::helper('catalog/image');
    }

    /**
     * @param $typeInstance \Magento\GroupedProduct\Model\Product\Type\Grouped
     * @param $product
     * @param $storeId
     * @param $additionalData
     *
     * @return mixed
     */
    private function getGroupedChildren(
        $typeInstance,
        $product,
        $storeId,
        $additionalData
    ) {

        $childProducts = $typeInstance->getAssociatedProducts($product);

        if ($childProducts) {
            $childSkus = [];
            foreach ($childProducts as $childProduct) {
                $childSkus[] = $childProduct->getSku();
            }

            $additionalData["custobar_child_ids"] =
                implode(",", $childSkus);
        }
        return $additionalData;
    }
}
