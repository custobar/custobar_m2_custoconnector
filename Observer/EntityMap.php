<?php

namespace Custobar\CustoConnector\Observer;

use Magento\Framework\Event\Observer;


class EntityMap extends Main
{
    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(Observer $observer)
    {
        $entity = $observer->getData('entity');
        $path = explode('\\', get_class($entity));
        $class = array_pop($path);
        if ($class == 'Interceptor') {
            $class = array_pop($path);
        }

        if (method_exists($this, 'extend' . $class . 'Data')) {
            $this->{'extend' . $class . 'Data'}($observer);
        }
    }
}
