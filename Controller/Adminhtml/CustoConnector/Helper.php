<?php

namespace Custobar\CustoConnector\Controller\Adminhtml\CustoConnector;

use Custobar\CustoConnector\Helper\Data;
use Magento\Framework\App\ResponseInterface;
use \Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\View\Result\PageFactory;

class Helper extends Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Custobar_CustoConnector::status';

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var JsonFactory
     */
    private $resultJsonFactory;

    /**
     * @param Context     $context
     * @param PageFactory $resultPageFactory
     * @param JsonFactory $resultJsonFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        JsonFactory $resultJsonFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->resultJsonFactory = $resultJsonFactory;
    }

    /**
     * @return int
     */
    private function getErroneousCount()
    {
        $collection = $this->getCHelper()->getSchedulesCollection();
        $collection->addOnlyErroneousFilter();
        return $collection->getSize();
    }

    /**
     * @return int
     */
    private function getUnProcessedCount()
    {
        $collection = $this->getCHelper()->getSchedulesCollection();
        $collection->addOnlyForSendingFilter();
        return $collection->getSize();
    }

    /**
     * @return Data
     */
    protected function getCHelper()
    {
        return $this->_objectManager->get(
            Data::class
        );
    }

    /**
     * Dispatch request
     *
     * @return ResultInterface|ResponseInterface
     * @throws NotFoundException
     */
    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        if ($this->getRequest()->isAjax()) {
            $test = [
                'errors' => $this->getErroneousCount(),
                'queued' => $this->getUnProcessedCount()
            ];
            return $result->setData($test);
        }
        $page = $this->resultPageFactory->create();
        $page->setActiveMenu(
            'Custobar_CustoConnector::custobar_custoconnector_helper'
        );
        $page->getConfig()->getTitle()->prepend(__('Status'));
        $page->addHandle('custoconnector_helper');

        $block = $page->getLayout()->getBlock(
            'custobar_custoconnector_block_status'
        );
        $block->setData(
            'check_url',
            $this->getUrl('custobar/custoconnector/helper')
        );

        $splitblock = $page->getLayout()->getBlock(
            'custobar_custoconnector_block_button'
        );
        $splitblock->setData('has_split', true);
        $models = $this->getCHelper()->getConfigTrackedModels();
        $options = [];
        foreach ($models as $class => $data) {
            $options[$class] = [
                'id' => count($options) + 1,
                'label' => $class,
                'default' => empty($options) ? true : null
            ];
        }
        $splitblock->setData('options', $options);
        $splitblock->setData(
            'callback_url',
            $this->getUrl('custobar/custoconnector/population')
        );

        $splitblock->setData(
            'callback_cancel_url',
            $this->getUrl('custobar/custoconnector/cancel')
        );

        return $page;
    }
}
