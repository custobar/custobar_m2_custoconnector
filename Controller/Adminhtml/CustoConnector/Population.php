<?php

namespace Custobar\CustoConnector\Controller\Adminhtml\CustoConnector;


use Custobar\CustoConnector\Helper\Data;
use Custobar\CustoConnector\Model\Initial;
use Custobar\CustoConnector\Model\ResourceModel\Initial\Collection;
use Magento\Framework\App\Response\Http;
use \Magento\Framework\App\ResponseInterface;
use \Magento\Backend\App\Action;
use \Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NotFoundException;
use \Magento\Framework\View\Result\PageFactory;
use \Magento\Framework\Controller\Result\JsonFactory;

class Population extends Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Custobar_CustoConnector::population';

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * Population constructor.
     *
     * @param Context     $context
     * @param PageFactory $resultPageFactory
     * @param JsonFactory $resultJsonFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        JsonFactory $resultJsonFactory
    ) {

        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->resultJsonFactory = $resultJsonFactory;
    }

    /**
     * This initializes scheduling for all or single model type
     * if its processing it wont do anything
     * no need to return error as ui should prevent that case
     *
     * @todo make it possible to requeue model type
     * @todo delete/cancel queue
     *
     * @param $model_class string
     *
     * @return array
     */
    public function startAction($model_class)
    {
        $return = [
            'status' => 'Unknown',
            'processing' => '0',
            'message' => ''
        ];

        $processing = $this->isProcessing();
        if ($processing != false) {
            $return['status'] = 'Processing';
            $return['message'] = 'The job exists already. ' . $processing;
            $return['processing'] = '1';
        } else {
            try {
                $this->getCHelper()->startInitialScheduling(
                    ($model_class == 'all') ? null : $model_class
                );
                $return['status'] = 'Started';
                $return['message'] =
                    'Initials created and enqueued for ' . $model_class;
            } catch (\Exception $e) {
                $return['message'] =
                    'Error handling Initials scheduling for ' . $model_class;
                $return['error'] = [
                    'code' => $e->getCode(),
                    'message' => $e->getMessage()
                ];
            }
        }
        return $return;
    }

    /**
     * @return array
     */
    public function statusAction()
    {
        /** @var Http $response */
        $response = $this->getResponse();
        $response->setHeader('Content-Type', 'application/json');
        $status = 'Not processing';
        $message = 'No collections';

        $processing = $this->isProcessing();
        if ($processing != false) {
            $status = 'Processing';
            $message = $processing;
        }

        if (!$this->getCHelper()->canProcess()) {
            $message =  'Not configured';
        }

        return [
            'status' => $status,
            'message' => $message,
            'processing' => ($processing != false) ? '1' : '0',
        ];
    }

    /**
     * @return bool|string
     */
    protected function isProcessing()
    {
        $collection = $this->getInitialCollection();

        if ($collection->count() > 0) {
            $pages = 0;
            $processedPages = 0;

            /** @var Initial $item */
            foreach ($collection as $item) {
                $pages += (int)$item->getPages();
                $processedPages += (int)$item->getPage();
            }

            $percentage = 0;
            if ($processedPages > 0) {
                $percentage = round(($processedPages / $pages) * 100, 2);
            }

            return "Processed pages {$percentage}%";
        }
        return false;
    }

    /**
     * @return Data
     */
    protected function getCHelper()
    {
        /** @var Data $helper */
        $helper = $this->_objectManager->get(
            Data::class
        );
        return $helper;
    }

    /**
     * @return Collection
     */
    protected function getInitialCollection()
    {
        return $this->_objectManager->get(
            Collection::class
        );
    }

    /**
     * Dispatch request
     *
     * @return ResultInterface|ResponseInterface
     * @throws NotFoundException
     */
    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        if ($this->getRequest()->isAjax()) {
            $request = $this->getRequest();
            $params = $request->getParams();
            /** @noinspection PhpUnusedLocalVariableInspection */
            $return = [
                'status' => '',
                'message' => '',
                'processing' => '0',
            ];
            if (!empty($params['model'])) {
                $return = $this->startAction($params['model']);
            } else {
                $return = $this->statusAction();
            }
            return $result->setData($return);
        } else {
            return $this->resultPageFactory->create();
        }
    }
}
